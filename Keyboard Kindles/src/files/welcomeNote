WELCOME TO TAKENOTE
 
Welcome to TakeNote, the easy note manager for Kindle devices!
 
TakeNote is designed to be a simple and intuitive way to achieve one goal - taking notes.  It allows you to create a large number of notes that are always available on your Kindle device.  TakeNote has a simple user interface with no flashy graphics or extra features.  Instead, you will find productivity-enhancing shortcuts aimed at making your writing process as smooth as possible.

This note provides you with quick hints for using TakeNote.  Please see the Help screen for more details.

 
USING TAKENOTE

Main Screen

• Scroll up and down the main note list screen with the 5-way controller to select the desired note. Notes are displayed in reverse chronological order, starting from the one that was created or edited last.
• Press Menu and select 'New Note'.
• To delete a note, move the cursor over it and then press Menu, choose 'Delete Note'.  Or select the note and press the Del key.
• Press the Text Options key (Aa) to sort the note list by name or last change date/time.


Note Editor

• Press the Aa (Text option) key to change font sizes and control whether the number legend is displayed.
• Use the SYM button to select special characters (or numbers, if necessary).
• Shift + 5-way Right moves to the next word.
• Shift + 5-way Left moves to the previous word.
• Shift + 5-way Down moves to the end of the current line.
• Shift + 5-way Up moves to the beginning of the current line.
• Shift + Next Page will take you to the end of your document (same as the "Go to bottom" menu option).
• Shift + Prev Page will to to the top (same as "Go to top" menu option).
• Shift + Del deletes the word to the left of the cursor.
• Alt + S saves the current note, but remains in the editor (same as "Save" menu option).
• Alt + X saves and closes the current note (same as "Save and exit" menu option).
• Use the "Save as new note" menu option to create a copy of the current note.

The symbols dialog provides an expanded set of useful characters and symbols.  However, TakeNote provides a few handy shortcut keys to quickly enter some common symbols without using the symbols dialog:
• Shift + . (period key) enters the bullet point character (•)
• Shift + / enters a comma **
• Alt + . enters a double quote (")
• Alt + / enters a single quote (') **

** Note:  The Shift + / and Alt + / shortcuts do not apply to Kindle devices that do not have a dedicated “/” key – i.e. the latest generation 6” Kindle.  


A few more important things to know about TakeNote:

• Note titles have no limits.  You can make your titles as descriptive as desired and use any symbols available in the symbols dialog.
• There is no limit to the amount of text that can be entered in a note!  However, the "technical" limit is the amount of free memory (RAM, not storage space) your Kindle has available, so it may be possible to run out of memory while entering a very very long note.
• Notes are stored in plain text, so they can be with any text editor outside of TakeNote.  See the Help screen for instructions on how to locate the notes with your Kindle attached to your PC.

Get started with your first note!  Press the Menu key and select the New Note option.


ABOUT IZZYLABS

TakeNote is created by IzzyLabs, a small software company founded by David Alexis and his daughter, Isabel (Izzy) Alexis.  Our goal is to create software that is easy, useful, and fun.  We hope you like TakeNote!

Please visit us at http://www.izzylabs.com, and drop us a line to tell us how you like TakeNote at takenote.support@izzylabs.com.
 
Copyright © 2011, IzzyLabs, David Alexis.  All Rights Reserved.


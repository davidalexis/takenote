package com.amazon.kindle.keyboard;

import com.amazon.kindle.kindlet.KindletContext;
import com.amazon.kindle.kindlet.ui.KLabel;
import com.amazon.kindle.kindlet.ui.KPanel;
import com.amazon.kindle.kindlet.ui.KindletUIResources;
// import com.amazon.kindle.kindlet.ui.KindletUIResources.KColorName;
// import com.amazon.kindle.kindlet.ui.KindletUIResources.KFontFamilyName;
// import com.amazon.kindle.kindlet.ui.KindletUIResources.KFontStyle;
// import com.amazon.kindle.kindlet.ui.OrientationController;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public final class NumberLegend extends KPanel
{
	/**
	 * 
	 */
	private static final long		serialVersionUID	= 1L;
	public static final int			VERTICAL			= 0;
	public static final int			HORIZONTAL			= 1;
	// private static final int TOTAL_LABELS = 10;
	// private static final int HORIZONTAL_LEGEND_NUM_ROWS = 2;
	// private static final int HORIZONTAL_LEGEND_NUM_COLS = 10;
	// private static final int VERTICAL_LEGEND_NUM_ROWS = 10;
	// private static final int VERTICAL_LEGEND_NUM_COLS = 1;
	// private static final int VERTICAL_LEGEND_GRID_PANEL_NUM_ROWS = 2;
	// private static final int VERTICAL_LEGEND_GRID_PANEL_NUM_COLS = 1;
	// private static final int NUMBER_FONT = 20;
	// private static final int NUMBER_LABEL_FONT = 14;
	private static final String[]	LETTERS				= { "Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P" };
	private final KindletContext	m_context;
	private boolean					m_debugMode;
	private int						m_legendOrientation;
	private Color					m_backgroundColor;
	private Color					m_foregroundColor;
	private final GridLayout		m_gridLayout;

	public NumberLegend ( KindletContext context )
	{
		this(context, 1, false);
	}

	public NumberLegend ( KindletContext context, int legendOrientation )
	{
		this(context, legendOrientation, false);
	}

	public NumberLegend ( KindletContext context, int legendOrientation, boolean debugMode )
	{
		if (null == context)
			throw new IllegalArgumentException("context cannot be null");

		this.m_context = context;
		this.m_debugMode = debugMode;
		this.m_legendOrientation = legendOrientation;

		this.m_backgroundColor = KindletUIResources.getInstance().getColor(KindletUIResources.KColorName.BLACK);
		this.m_foregroundColor = KindletUIResources.getInstance().getColor(KindletUIResources.KColorName.WHITE);

		this.m_gridLayout = new GridLayout();
		layoutNumberLegend();
	}

	final void layoutNumberLegend()
	{
		try
		{
			if ((this.m_debugMode) || (!doesNumberExist(this.m_context)))
				setupNumberLegend();
			else
				setupBlankLegend();
		}
		catch (IllegalAccessException e)
		{
			setupBlankLegend();
		}
		catch (InvocationTargetException e)
		{
			setupBlankLegend();
		}
		catch (NoSuchMethodException e)
		{
			setupBlankLegend();
		}
	}

	private final void setupBlankLegend()
	{
		removeAll();
		setFocusable(false);
		this.m_gridLayout.setVgap(0);
	}

	public final boolean doesNumberExist(KindletContext context) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException, SecurityException, NoSuchMethodException
	{
		Object inputMethods = null;
		try
		{
			Method getInputMethod = context.getClass().getMethod("getInputMethods", null);
			inputMethods = getInputMethod.invoke(context, null);
		}
		catch (NoSuchMethodException e)
		{
			return true;
		}

		Method getKeyboardsMethod = inputMethods.getClass().getMethod("getKeyboards", null);
		Set keyboardSet = (Set) getKeyboardsMethod.invoke(inputMethods, null);

		return findNumberLabel(keyboardSet);
	}

	final boolean findNumberLabel(Set keyboardSet) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException
	{
		if (null == keyboardSet)
			return false;
		Iterator keyboardIterator = keyboardSet.iterator();

		while (keyboardIterator.hasNext())
		{
			Object keyboard = keyboardIterator.next();
			Object keyboardSequenceObj = keyboard.getClass().getMethod("getKeyboardSequences", null).invoke(keyboard, null);
			if (null != keyboardSequenceObj)
			{
				Iterator keyboardSequenceIterator = ((List) keyboardSequenceObj).iterator();

				while (keyboardSequenceIterator.hasNext())
				{
					Object keySequence = keyboardSequenceIterator.next();
					Iterator keySequenceIterator = ((List) keySequence).iterator();

					while (keySequenceIterator.hasNext())
					{
						Object keyObj = keySequenceIterator.next();

						String label = (String) keyObj.getClass().getMethod("getLabel", null).invoke(keyObj, null);
						if ((label != null) && (-1 != label.indexOf("1")))
						{
							return true;
						}

						String keyId = (String) keyObj.getClass().getMethod("getKeyId", null).invoke(keyObj, null);
						if ((keyId != null) && (-1 != keyId.indexOf("1")))
						{
							return true;
						}
					}
				}
			}
		}

		return false;
	}

	public final void doLayout()
	{
		if (0 == this.m_legendOrientation)
		{
			if ((this.m_context.getOrientationController().getOrientation() == 0) || (this.m_context.getOrientationController().getOrientation() == 1))
			{
				this.m_gridLayout.setVgap(40);
			}
			else
			{
				this.m_gridLayout.setVgap(20);
			}
		}
		super.doLayout();
	}

	public final void setLegendOrientation(int legendOrientation)
	{
		if (!EventQueue.isDispatchThread())
		{
			throw new IllegalStateException();
		}

		if ((legendOrientation != 0) && (legendOrientation != 1))
		{
			throw new IllegalArgumentException("orientation must be either VERTICAL or HORIZONTAL");
		}

		if (legendOrientation != this.m_legendOrientation)
		{
			this.m_legendOrientation = legendOrientation;
			layoutNumberLegend();
		}
	}

	public final int getLegendOrientation()
	{
		return this.m_legendOrientation;
	}

	public final void setDebugMode(boolean debugMode)
	{
		if (!EventQueue.isDispatchThread())
		{
			throw new IllegalStateException();
		}

		this.m_debugMode = debugMode;
		layoutNumberLegend();
	}

	public final boolean getDebugMode()
	{
		return this.m_debugMode;
	}

	private final void setupNumberLegend()
	{
		removeAll();

		if (this.m_legendOrientation == 1)
			setupHorizontalLegend();
		else
		{
			setupVerticalNumberLegend();
		}
		setFocusable(false);
		setForeground(this.m_foregroundColor);
		setBackground(this.m_backgroundColor);
	}

	private final void setupHorizontalLegend()
	{
		this.m_gridLayout.setRows(2);
		this.m_gridLayout.setColumns(10);
		this.m_gridLayout.setVgap(0);
		setLayout(this.m_gridLayout);

		for (int i = 0; i < 10; i++)
		{
			String number = Integer.toString((i + 1) % 10);
			add(createLabel(number, 20, this.m_foregroundColor, this.m_backgroundColor));
		}

		for (int i = 0; i < 10; i++)
		{
			String numberMapping = "ALT+" + LETTERS[i];
			add(createLabel(numberMapping, 14, this.m_foregroundColor, this.m_backgroundColor));
		}
	}

	private final void setupVerticalNumberLegend()
	{
		this.m_gridLayout.setRows(10);
		this.m_gridLayout.setColumns(1);
		setLayout(this.m_gridLayout);

		for (int i = 0; i < 10; i++)
		{
			GridLayout keyMappingGridLayout = new GridLayout(2, 1);
			KPanel keyMappingPanel = new KPanel(keyMappingGridLayout);

			String number = Integer.toString((i + 1) % 10);
			KLabel numberLabel = createLabel(number, 20, this.m_foregroundColor, this.m_backgroundColor);
			keyMappingPanel.add(numberLabel);

			String numberMapping = "ALT+" + LETTERS[i];
			KLabel numberMappingLabel = createLabel(numberMapping, 14, this.m_foregroundColor, this.m_backgroundColor);
			keyMappingPanel.add(numberMappingLabel);
			keyMappingPanel.setFocusable(false);

			keyMappingPanel.setBackground(this.m_backgroundColor);
			keyMappingPanel.setForeground(this.m_foregroundColor);
			add(keyMappingPanel);
		}
	}

	private final KLabel createLabel(String labelText, int fontSize, Color foregroundColor, Color backgroundColor)
	{
		KLabel label = new KLabel(labelText);
		label.setFont(KindletUIResources.getInstance().getFont(KindletUIResources.KFontFamilyName.SANS_SERIF, fontSize, KindletUIResources.KFontStyle.BOLD));
		label.setFocusable(false);
		label.setBackground(backgroundColor);
		label.setForeground(foregroundColor);
		label.setHorizontalAlignment(0);

		return label;
	}
}
// Copyright 2011 Amazon.com, Inc. or its affiliates. All Rights Reserved.
package com.amazon.kindle.customdialog;

import java.awt.BasicStroke;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.KeyboardFocusManager;
import java.awt.LayoutManager;
import java.awt.LayoutManager2;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import com.amazon.kindle.kindlet.ui.Focus2DTraversalPolicy;
import com.amazon.kindle.kindlet.ui.FocusTraversalEdgePolicy;
import com.amazon.kindle.kindlet.ui.KPanel;
import com.amazon.kindle.kindlet.ui.KRepaintManager;


/**
 * CustomDialog class to create a dialog box which will 
 * be overlayed and centered on the screen.
 * <p>
 * The CustomDialog works with the use of a layout manager which acts as 
 * a proxy to the original layout manager, filtering the the CustomDialog
 * from being laid out by the original layout manager. Then CustomDialog
 * will then be drawn on top of the other components.
 */
public class CustomDialog extends KPanel {
    
	private static final long	serialVersionUID	= 1L;

	/** Arc of the rounded edges of the dialog. */
    private final int ARC_LENGTH = 48;
    
    /** Width of the stroke that surrounds the dialog */
    private final int STROKE_WIDTH = 2;
        
    /** Storage for the root container. */
    private Container m_root;
       
    /** The component which will request focus when the dialog becomes visible. */
    private Component m_defaultFocus;
    
    /** The component which to give focus to after the dialog box closes. */
    private Component m_passFocus;
        
    /**
     * Constructor which takes the KindletContext. The constructor will replace the root containers layout manager with
     * a DialogLayoutManager.
     * @param root The root container of the application
     */
    public CustomDialog(final Container root) {
        m_root = root;
        initDialog();
    }
     
    /**
     * Overriding the paint method in order to paint the dialog on top of the background panel.
     * @param g the specified Graphics window.
     */
    public void paint(final Graphics g) {        
    	final Graphics2D g2 = (Graphics2D) g;
    	                        
        // Paint the inner components
        super.paint(g2);    
                
        g2.setColor(this.getForeground());        
                        
        g2.setStroke(new BasicStroke(STROKE_WIDTH));        
        g2.drawRoundRect(0, 0, this.getWidth() - 1 , this.getHeight() - 1, ARC_LENGTH, ARC_LENGTH);
    }
   
    /**
     * Sets the default focus component. This components visibility should be set to true. 
     * @param defaultFocus The component which will request focus when the dialog opens.
     */
    public void setFocusDefault(final Component defaultFocus) {
        m_defaultFocus = defaultFocus;
    }
 
    /** Shows the dialog. */
    public void showDialog() {        
        m_passFocus = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
        this.setVisible(true);
        m_root.validate();
                
        try {  
        	// Flash the dialog 
            KRepaintManager.paintImmediately(this, true);
        } catch (InvocationTargetException e) {
            // Attempt to repaint if the KRepaintManager fails
            m_root.repaint();            
        } catch (InterruptedException e) {            
            // Application is closing, give up with painting and exit
            return; 
        } 
        
        EventQueue.invokeLater(new Runnable() {
            public void run() { 
                 //m_defaultFocus.requestFocus();
            }
         });
    }
    
    /** Hides the dialog. */
    public void hideDialog() {       
        this.setVisible(false);

        try {
        	// Flash the dialog 
            KRepaintManager.paintImmediately(this, true);
        } catch (InvocationTargetException e) {
            // Attempt to repaint if the KRepaintManager fails
            m_root.repaint();
        } catch (InterruptedException e) {
            // Application is closing, give up with painting and exit
            return;
        }  
        
        EventQueue.invokeLater(new Runnable() {
            public void run() {
               m_passFocus.requestFocus();
            }
         });
    }    
    
    /** Initializes the dialog by creating a new DialogLayout and setting itself as a focus traversal root. */
    protected void initDialog() {        
    	// Create a new Dialog Layout or use the existing one. 
    	DialogLayout dialogLayout;     	 
    	if (m_root.getLayout() instanceof DialogLayout) {
            dialogLayout = (DialogLayout) m_root.getLayout();
    	} else {    	
            dialogLayout = new DialogLayout(m_root);
    	}

        this.setFocusCycleRoot(true);
        this.setVisible(false);
            	                                   
        m_root.setLayout(dialogLayout);
        m_root.add(this);

        // Set the focus policy so focus cannot leave the dialog
        Focus2DTraversalPolicy policy = (Focus2DTraversalPolicy) this.getFocusTraversalPolicy();
        policy.setEdgePolicy(FocusTraversalEdgePolicy.EDGE_POLICY_HALT);
    }
    
   /** 
    * Removes the CustomDialog from the root container. You will want call this if you do not plan on reusing the 
    * dialog. 
    */
    public void remove() {
        m_root.remove(this);
    }
   
    /**
     * Makes a request for focus on focus root of the dialog.
     */
    public void requestDefaultFocus() {
        m_defaultFocus.requestFocus();
    }
       
    /**
     * A layout manager which stands in for the original layout and filters out the CustomDialog so on top of other 
     * components.
     */
    protected class DialogLayout implements LayoutManager2 {

        /** The root layout manager which was originally set. */
        private LayoutManager m_rootLayout;
        
        /** The root component which to draw the dialog upon. */
        private Container m_root;
        
        /**
         * Constructor which sets the root container and extracts the original layout.  
         * @param root Root container of the application
         */        
        protected DialogLayout(final Container root) {
            m_root = root;
            m_rootLayout = m_root.getLayout();           
        }
              
        /** {@inheritDoc} */
        public void addLayoutComponent(final String name, final Component component) {
            if (!(component instanceof CustomDialog)) {
                m_rootLayout.addLayoutComponent(name, component);
            }
        }
        
        /** {@inheritDoc} */
        public void layoutContainer(final Container component) {                        
            if (component == m_root) {
                // Iterate and calculate placement of all CustomDialogs
            	final Component[] children = component.getComponents();
            	final ArrayList dialogs = new ArrayList();
            	for (int i = 0; i < children.length; i++) {
            	    if (children[i] instanceof CustomDialog && children[i].isVisible()) {
            	        component.remove(children[i]); 
            		    dialogs.add(children[i]);
            	    }
            	}            	            

                m_rootLayout.layoutContainer(component);                
                
                for (int i = 0; i < dialogs.size(); i++) {
                    final CustomDialog dialog = (CustomDialog) dialogs.get(i);
                    calcPlacement(dialog);
                    component.add(dialog, 0);

                    // This has to be added to overcome losing focus when orientation
                    // is changed while the dialog is open
                    EventQueue.invokeLater(new Runnable() {
                        public void run() {                                                    
                            dialog.requestDefaultFocus();                              
                        }
                    });
                }
            } else {
                m_rootLayout.layoutContainer(component);
            }
        }
        
        /** {@inheritDoc} */
        public Dimension minimumLayoutSize(final Container component) {
            if (!(component instanceof CustomDialog)) {
                return m_rootLayout.minimumLayoutSize(component);
            } else {
                return component.getMinimumSize();
            }
        }
        
        /** {@inheritDoc} */
        public Dimension preferredLayoutSize(final Container component) {
            if (!(component instanceof CustomDialog)) {
                return m_rootLayout.preferredLayoutSize(component);
            } else {
                return component.getPreferredSize();
            }
        }
        
        /** {@inheritDoc} */
        public void removeLayoutComponent(final Component component) {
            if (!(component instanceof CustomDialog)) {
                m_rootLayout.removeLayoutComponent(component);
            }
        }

        /** {@inheritDoc} */
        public void addLayoutComponent(final Component component,final Object arg1) {
            if (!(component instanceof CustomDialog)) {
                ((LayoutManager2) m_rootLayout).addLayoutComponent(component, arg1);
            }       
        }
        
        /** {@inheritDoc} */
        public float getLayoutAlignmentX(final Container container) {
            if (!(container instanceof CustomDialog)) {
                return ((LayoutManager2) m_rootLayout).getLayoutAlignmentX(container);
            } else {     
                return container.getAlignmentX();
            }
        }
        
        /** {@inheritDoc} */
        public float getLayoutAlignmentY(final Container container) {
            if (!(container instanceof CustomDialog)) {
                return ((LayoutManager2) m_rootLayout).getLayoutAlignmentY(container);
            } else {
                return container.getAlignmentY();
            }
        }
        
        /** {@inheritDoc} */
        public void invalidateLayout(final Container container) {
            ((LayoutManager2) m_rootLayout).invalidateLayout(container);
        }
        
        /** {@inheritDoc} */
        public Dimension maximumLayoutSize(final Container container) {
            if (!(container instanceof CustomDialog)) {
                return ((LayoutManager2) m_rootLayout).maximumLayoutSize(container);
            } else {
                return container.getMaximumSize();
            }
        }
        
        /**
         * Accessor for the original layout.
         * @return the original layout procured from the provided root container
         */
        public LayoutManager getOrginalLayout() {
            return m_rootLayout;
        }
        
        /** Calculates the x and y coordinates to place dialog box in the center of the screen. */
        private void calcPlacement(Component comp) {         
            final int x = ( m_root.getWidth() - comp.getWidth()) / 2;
            final int y = ( m_root.getHeight() - comp.getHeight()) / 2;
            comp.setLocation(x, y);
        }
    }  
}

package com.izzylabs.kindle;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Insets;
import com.amazon.kindle.kindlet.ui.KBoxLayout;
import com.amazon.kindle.kindlet.ui.KPanel;
import com.amazon.kindle.kindlet.ui.KLabel;
import com.amazon.kindle.kindlet.ui.KindletUIResources;
import com.amazon.kindle.kindlet.ui.KindletUIResources.KColorName;
import com.amazon.kindle.kindlet.ui.KindletUIResources.KFontStyle;

public class TitlePanel extends KPanel
{
	private static final long	serialVersionUID	= 2210152483680799293L;
	protected KLabel headerLabel;
	
	// ----------------------------------------------------------------------------------
	public TitlePanel(String title)
	{
		this.setLayout(new BorderLayout(0, 0));
		final Font headerFont =  KindletUIResources.getInstance().getFont(KindletUIResources.KFontFamilyName.SANS_SERIF, 30, KFontStyle.BOLD);
		this.setBackground(KindletUIResources.getInstance().getColor(KColorName.GRAY_02));
	
		setLayout(new KBoxLayout(this, KBoxLayout.LINE_AXIS));
		setFocusable(false);

		headerLabel = new KLabel(title)
		{
			private static final long	serialVersionUID	= 1L;
	
			public Insets getInsets()
			{
				return new Insets(15, 5, 15, 5);
			}
		};

		headerLabel.setFont(headerFont);
		add(headerLabel, BorderLayout.EAST);
	}

	// ----------------------------------------------------------------------------------
	public void paint(final Graphics g)
	{
		super.paint(g);

		int width = getWidth();
		int height = getHeight() - 2;
		g.setColor(KindletUIResources.getInstance().getColor(KColorName.DARK_GRAY));
		g.drawLine(2, height, width, height);
	}

	// ----------------------------------------------------------------------------------
	public Insets getInsets()
	{
		return new Insets(15, 15, 15, 15);
	}

	// ----------------------------------------------------------------------------------
	public void setTitle(String title)
	{
		this.headerLabel.setText(title);
	}
}

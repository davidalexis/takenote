package com.izzylabs.kindle;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import com.amazon.kindle.kindlet.event.KindleKeyCodes;
import com.amazon.kindle.kindlet.ui.*;
import com.amazon.kindle.kindlet.ui.KindletUIResources.KColorName;
import com.amazon.kindle.kindlet.util.Timer;
import com.amazon.kindle.kindlet.util.TimerTask;

public class TextArea extends KPanel
{

	private static final long	serialVersionUID	= 8757895063802523483L;
	protected Document			document			= new Document();
	private Dimension			dim;
	private boolean			isDirty;
	private boolean			hasFocus			= false;
	final 	private int		margin				= 10;
	private int				topMargin			= 50;
	private boolean			isInitialized		= false;
	private int				visibleLineCount;
	private KeyListener[]		keyListeners;
	private boolean			shiftPressed;
	private Timer 				timer;
	private final int 			maxContentSize 		=  3100;
	protected boolean			caretChanged;
	private boolean 			hasChanged;
	private boolean 			altPressed;
	
	// ----------------------------------------------------------------------------------
	public TextArea ( String documentText, String hint )
	{
		// super();
		this.setFocusable(true);
		this.setVisible(true);
		this.document = new Document();
		this.document.setLineWidth(getWidth() - (this.margin * 2) - 10);
		this.setText(documentText == null ? "" : documentText);

		timer = new Timer();
	    timer.schedule(new PaintTask(), 1000);
	    
		// TODO: this.setHint(hint);
	}

	public void dispose()
	{
		this.timer.cancel();
	}

	// ----------------------------------------------------------------------------------
	public void setText(String documentText)
	{
		if (documentText == null)
			return;
		
		try
		{
			this.document = new Document();

			// verifyComponentSize();
			this.dim = getSize();
			calculateVisibleLineCount();

			this.document.setLineWidth(dim.width - (this.margin * 2));
			this.document.charWidths = App.settings.getFontWidths();
			
			if (documentText.length() < this.maxContentSize)
			{
				this.document.setText(documentText);
			}
			else
			{
				this.document.setText(documentText.substring(0, this.maxContentSize) + 1);
			}
			
			this.document.setCurrentLine(0);
			this.document.setFirstVisibleLine(0);
			repaint();
		}
		catch (Exception ex)
		{
			App.logError("setText error:  " + ex.getMessage());
			App.logError(ex.getStackTrace().toString());
		}
	}

	// ----------------------------------------------------------------------------------
	private void calculateVisibleLineCount()
	{
		if (isInitialized == false)
		{
			if (this.dim == null)
				this.dim = getSize();
			// verifyComponentSize();

			this.topMargin = App.settings.getLineHeight() * 2;
			this.document.charWidths = App.settings.getFontWidths();
			this.document.setLineWidth(this.dim.width - (this.margin * 2));
		}

		int contentHeight = (int) (this.getHeight() - this.topMargin);
		int lineHeight = App.settings.getLineHeight() - 1;
		
		if (lineHeight <= 0 || contentHeight <= 0)
		{
			this.visibleLineCount = 1;
		}
		else
		{
			this.visibleLineCount = ((int) (this.getHeight() - this.topMargin) / App.settings.getLineHeight()) - 1;
		}
		
		this.document.setVisibleLineCount(this.visibleLineCount);
	}

	// ----------------------------------------------------------------------------------
	public String getText()
	{
		return this.document.getText();
	}

	// ----------------------------------------------------------------------------------
	public void setCaretPosition(int position)
	{
		// Instrumentation.mark("TextArea.setCaretPosition()", true);

		this.document.setCurrentPosition(position);

		// Instrumentation.mark("TextArea.setCaretPosition()", false);
	}

	// ----------------------------------------------------------------------------------
	public void gotoEnd()
	{
		int lastLineNumber = document.getLineCount() - 1;
		Document.Line lastLine = document.getLine(lastLineNumber);

		if (lastLine != null)
		{
			document.setCurrentLine(lastLineNumber);
			document.moveToEndOfLine();
		}
	}

	// ----------------------------------------------------------------------------------
	public void setFont(Font font)
	{
		super.setFont(font);
		this.topMargin = App.settings.getLineHeight() * 2;
		calculateVisibleLineCount();
		this.document.charWidths = App.settings.getFontWidths();
		this.document.reflow();
		repaint();
	}

	// ----------------------------------------------------------------------------------
	public void insertChar(char character)
	{
		this.document.insert(character);
		this.isDirty = true;
		repaint();
	}

	// ----------------------------------------------------------------------------------
	protected void processFocusEvent(FocusEvent e)
	{
		try
		{
			FocusListener[] listeners = getFocusListeners();

			int id = e.getID();

			switch (id)
			{
				case FocusEvent.FOCUS_GAINED:
					this.hasFocus = true;
					setFocusTraversalKeysEnabled(false);
					repaint();

					for (int j = 0; j < listeners.length; j++)
					{
						listeners[j].focusGained(e);
					}
					
					break;

				case FocusEvent.FOCUS_LOST:
					this.hasFocus = false;

					setFocusTraversalKeysEnabled(true);
					for (int j = 0; j < listeners.length; j++)
					{
						listeners[j].focusLost(e);
					}
					repaint();
					break;
			}
		}
		catch (Exception ex)
		{
			App.logError("Error in processFocusEvent: " + ex.getMessage());
		}
	}

	// ----------------------------------------------------------------------------------
	protected void processKeyEvent(final KeyEvent key)
	{
		try
		{
			if (this.keyListeners == null)
			{
				this.keyListeners = getKeyListeners();
			}

			switch (key.getID())
			{
				case KeyEvent.KEY_PRESSED:
					switch (key.getKeyCode())
					{
						case KindleKeyCodes.VK_FIVE_WAY_LEFT:
							if (key.isShiftDown())
							{
								this.document.moveToPreviousWord();
								this.shiftPressed = false; 
							}
							else
							{
								this.document.moveLeft();
							}
							
							this.caretChanged = true;
//							key.consume();
//							repaint();
							break;

						case KindleKeyCodes.VK_FIVE_WAY_RIGHT:
							if (key.isShiftDown())
							{
								this.document.moveToNextWord();
								this.shiftPressed = false; 
							}
							else
							{
								this.document.moveRight();
							}
							
							this.caretChanged = true;
//							key.consume();
//							repaint();
							break;

						case KindleKeyCodes.VK_FIVE_WAY_DOWN:
							if (key.isShiftDown())
							{
								this.document.moveToEndOfLine();
								this.shiftPressed = false; 
								// this.document.verifyVisibleLines();
							}
							else if (this.document.getCurrentLine() + 1 < this.document.getLineCount())
							{
								this.document.moveToNextLine();
							}
							else
							{
								transferFocusBelow();
							}

							this.caretChanged = true;
//							key.consume();
//							repaint();
							break;

						case KindleKeyCodes.VK_FIVE_WAY_UP:
							if (key.isShiftDown())
							{
								this.document.moveToBeginningOfLine();
								this.shiftPressed = false; 
							}
							else if (this.document.getCurrentLine() == 0) 
							{
								transferFocusAbove();
							}
							else
							{
								this.document.moveToPreviousLine();
							}
							
							 this.caretChanged = true;
//							key.consume();
//							repaint();
							break;

						case KindleKeyCodes.VK_LEFT_HAND_SIDE_TURN_PAGE:
						case KindleKeyCodes.VK_RIGHT_HAND_SIDE_TURN_PAGE:
							if (key.isShiftDown())
							{
								gotoEnd();
								this.shiftPressed = false; 
							}
							else
							{
								int nextPageTopLine = document.getFirstVisibleLine() + this.visibleLineCount;

								if (nextPageTopLine < document.getLineCount())
								{
									document.setCurrentLine(nextPageTopLine);
									document.setFirstVisibleLine(nextPageTopLine);
								}
								else
								{
									document.setCurrentLine(document.getLineCount() - 1);
								}
							}

							this.caretChanged = true;
//							key.consume();
//							repaint();
							break;

						case KindleKeyCodes.VK_TURN_PAGE_BACK:
							if (key.isShiftDown())
							{
								this.document.setCurrentPosition(0);
								this.shiftPressed = false; 
								// key.consume();
							}
							else
							{
								int prevPageTopLine = document.getFirstVisibleLine() - this.visibleLineCount;

								if (prevPageTopLine < 0)
									prevPageTopLine = 0;

								document.setCurrentLine(prevPageTopLine);
								document.setFirstVisibleLine(prevPageTopLine);
							}

							this.caretChanged = true;
//							key.consume();
//							repaint();
							break;

						case KeyEvent.VK_BACK_SPACE:
						case KeyEvent.VK_DELETE:
							// key.consume();
							if (this.document.getCurrentPosition() != 0)
							{
								if (key.isShiftDown() == true)
								{
									this.document.deletePreviousWord();

									for (int index = 0; index < this.keyListeners.length; index++)
									{
										this.keyListeners[index].keyTyped(key);
									}
									
									this.shiftPressed = false; 
								}
								else
								{
									this.document.deletePreviousCharacter();
								}
							}

							this.setHasChanged(true);
							this.isDirty = true;
//							repaint();
							break;

						default:
							if (key.isShiftDown())
							{
								this.shiftPressed = true;
							}
							else if (key.isAltDown())
							{
								this.altPressed = true;
							}

							break;
					}
					break;

				case KeyEvent.KEY_TYPED:
					char thisChar = key.getKeyChar();
					boolean okToInsertThisCharacter = true;
					
					switch (thisChar)
					{
						case KeyEvent.VK_BACK_SPACE:
						case KeyEvent.VK_DELETE:
							// key.consume();
							this.isDirty = true;
							this.setHasChanged(true);
							break;

						default:
							if (this.shiftPressed || key.isShiftDown())
							{
								switch (thisChar)
								{
									case '.':
										thisChar = '.';
										break;

									case '/':
										thisChar = ',';
										break;

									default:
										thisChar = Character.toUpperCase(thisChar);
										break;
								}
							}
							else if (this.altPressed || key.isAltDown())
							{
								switch (thisChar)
								{
									case '.':
										thisChar = '"';
										break;

									case '/':
										thisChar = '\'';
										break;

									default:
										// key.consume();
										okToInsertThisCharacter = false;
										break;
								}
							}

							if (okToInsertThisCharacter && isLessThanMaxSize())
							{
								this.document.insert(thisChar);
								this.isDirty = true;
							}
							
							this.setHasChanged(true);

							// key.consume();
							repaint();
							break;
					}
					
					if (this.shiftPressed)
					{
						this.shiftPressed = false;
						this.altPressed = false;
					}
					if (this.altPressed)
					{
						this.altPressed = false;
						this.shiftPressed = false;
					}
					break;

				case KeyEvent.KEY_RELEASED:
					switch (key.getKeyCode())
					{
						case KindleKeyCodes.VK_FIVE_WAY_LEFT:
						case KindleKeyCodes.VK_FIVE_WAY_RIGHT:
						case KindleKeyCodes.VK_FIVE_WAY_DOWN:
						case KindleKeyCodes.VK_FIVE_WAY_UP:
							 key.consume();
							break;
					}
					break;
			}

			if (key.isConsumed() == false)
			{
				// Broadcast event to any key listeners
				for (int index = 0; index < this.keyListeners.length; index++)
				{
					int id = key.getID();
					switch (id)
					{
						case KeyEvent.KEY_PRESSED:
							this.keyListeners[index].keyPressed(key);
							break;

						case KeyEvent.KEY_RELEASED:
							this.keyListeners[index].keyReleased(key);
							break;

						case KeyEvent.KEY_TYPED:
							this.keyListeners[index].keyTyped(key);
							break;
					}
				}
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

	// ----------------------------------------------------------------------------------
	public void setIsDirty(boolean dirty)
	{
		this.isDirty = dirty;
	}

	// ----------------------------------------------------------------------------------
	public boolean getIsDirty()
	{
		return isDirty;
	}

	// ----------------------------------------------------------------------------------
	public int getCaretPosition()
	{
		return this.document.getCurrentPosition();
	}

	// ----------------------------------------------------------------------------------
	public int getTopVisibleLine()
	{
		return this.document.getFirstVisibleLine();
	}

	// ----------------------------------------------------------------------------------
	public void setTopVisibleLine(int line)
	{
		this.document.setFirstVisibleLine(line);
		// this.document.verifyVisibleLines();
	}

	// ----------------------------------------------------------------------------------
	public boolean isLessThanMaxSize()
	{
		if (this.document.getLength() > maxContentSize)
			return false;
		else
			return true;
	}
	
	// ----------------------------------------------------------------------------------
	public int getMaxContentSize()
	{
		return maxContentSize;
	}
	
	// ----------------------------------------------------------------------------------
	public void paint(final Graphics graphics)
	{
		try
		{
			this.dim = getSize();
	
			if (isInitialized == false)
			{
				if (this.dim.width > 0)
				{
					graphics.setFont(App.settings.getFont(Settings.NoteBodyFont));
					this.topMargin = App.settings.getLineHeight() * 2;
					this.document.charWidths = App.settings.getFontWidths();
					this.document.setLineWidth(this.dim.width - (this.margin * 2));
					calculateVisibleLineCount();
					this.document.reflow();
					isInitialized = true;
				}
			}
	
			final int width = this.dim.width;
			final int height = this.dim.height;
	
			graphics.setColor(KindletUIResources.getInstance().getColor(KColorName.BLACK));
			graphics.drawRect(0, 11, width - 2, height - 15);
			final int lineHeight = App.settings.getLineHeight();
			final int firstVisibleLine = this.document.getFirstVisibleLine();
			final int currentLine = this.document.getCurrentLine();
			final int lineCount = this.document.getLineCount();

			// If the document is empty just paint the caret
			if (lineCount == 0) // || firstVisibleLine < 0)
			{
				if (this.hasFocus)
				{
					// We have no text. Paint the default caret
					drawCursor(graphics, lineHeight, this.margin, this.topMargin);
				}
			}
			else
			{
				int yposition;
				int lineNumber;
				int caretPixelPosition = 0;

				yposition = this.topMargin;

				// Paint the visible section of the document
				for (int index = 0; index < this.visibleLineCount; index++)
				{
					// lineTimerStart = System.currentTimeMillis();

					lineNumber = firstVisibleLine + index;

					if (lineNumber >= lineCount)
						break;

					Document.Line thisLine = this.document.getLine(lineNumber);

					if (thisLine == null)
						break;

					final String thisLineText = thisLine.toString();
					graphics.drawString(thisLineText, this.margin, yposition);

					if (this.hasFocus && lineNumber == currentLine)
					{
						FontMetrics fm = graphics.getFontMetrics();
						final String textToCursor = thisLineText.substring(0, this.document.getPositionInLine());
						caretPixelPosition = fm.stringWidth(textToCursor);

						drawCursor(graphics, lineHeight, this.margin + caretPixelPosition, yposition);
					}

					yposition += lineHeight;
				}
			}

			// Draw the more text indicators, if necessary
			if (firstVisibleLine > 0)
			{
				// Draw "more text above" indicator
				graphics.fillRect(0, 11, width - 1, 12);
			}

			if ((firstVisibleLine + this.visibleLineCount) < lineCount)
			{
				// Draw "more text below" indicator
				graphics.fillRect(0, height - 14, width - 1, height - 13);
			}
		}
		catch (Exception ex)
		{
			//ex.printStackTrace();
		}
	}

	// ----------------------------------------------------------------------------------
	private void drawCursor(final Graphics graphics, int lineHeight, int xposition, int yposition)
	{
		// lineHeight -= 3;
		graphics.drawLine(xposition, yposition + 4, xposition, yposition + 4 - lineHeight);
		graphics.drawLine(xposition + 1, yposition + 4, xposition + 1, yposition + 4 - lineHeight);

		graphics.drawLine(xposition - 3, yposition + 4, xposition + 4, yposition + 4);
		graphics.drawLine(xposition - 3, yposition + 4 - lineHeight, xposition + 4, yposition + 4 - lineHeight);
	}

	
	public boolean getHasChanged()
	{
		return hasChanged;
	}

	public void setHasChanged(boolean hasChanged)
	{
		this.hasChanged = hasChanged;
	}


	// ==================================================================================
	class PaintTask extends TimerTask 
	{
	    public void run() 
	    {
	    	try
	    	{
		    	if (TextArea.this.hasFocus && caretChanged == true)
		    	{
		    		repaint();
		    		caretChanged = false;
		    		setHasChanged(false);
		    	}
		    	
		    	timer.schedule(new PaintTask(), 80);
	    	}
	    	catch (Exception ex)
	    	{
	    		App.logInfo("timer error: " + ex.getMessage());
	    	}
	    }
	  }
}

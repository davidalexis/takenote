/* 
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * Copyright (c) 2012, David Alexis. All rights reserved.
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */

package com.izzylabs.kindle;

import java.awt.Container;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.StringTokenizer;

import jregex.Matcher;
import jregex.Pattern;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

final class Notebook implements IViewable
{
	private Container			view;
	public String				name;
	public String				id;
	public String				filePath;
	public LinkedHashMap		notesIndex;
	public final DateFormat	sortDateFormatter	= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private NoteIndexEntry		autosavedNote;
	public final static String	DefaultNotebokId	= "notebook";
	private String				fileName;

	// ----------------------------------------------------------------------------------
	public Container getView()
	{
		// Create the Notebook List view
		if (this.view == null)
		{
			this.view = new NotebookView(this);
		}
		
		return this.view;
	}

	// ----------------------------------------------------------------------------------
	public void setView(Container view)
	{
		this.view = view;
	}

	// ----------------------------------------------------------------------------------
	public String getViewName()
	{
		return "notebook";
	}

	// ----------------------------------------------------------------------------------
	public int getViewId()
	{
		return App.NOTEBOOKVIEW;
	}

	// ----------------------------------------------------------------------------------
	Notebook(String notebookId)
	{
		this.id = notebookId;
		this.notesIndex = new LinkedHashMap();

		// Load the notebook
		load();
	}

	// ----------------------------------------------------------------------------------
	public void addNote(NoteIndexEntry note)
	{
		this.notesIndex.put(note.id, note);
	}

	// ----------------------------------------------------------------------------------
	public void removeNote(String noteId)
	{
		if (this.notesIndex.containsKey(noteId))
		{
			this.notesIndex.remove(noteId);
			save();
		}
	}

	// ----------------------------------------------------------------------------------
	protected void deleteNote(String noteId)
	{
		// The notebook's note files should be in a sub-folder with a name
		// matching the notebook ID
		int sep = this.filePath.lastIndexOf("/");
		String notesDirName = this.filePath.substring(0, sep) + "/" + this.id;
		String noteFileName = "note" + noteId;
		File noteFile = new File(notesDirName, noteFileName);

		removeNote(noteId);

		if (noteFile.exists())
		{
			noteFile.delete();
		}
	}

	// ----------------------------------------------------------------------------------
	public ArrayList getNotes()
	{
		return getFilteredNotes(null);
	}

	// ----------------------------------------------------------------------------------
	public ArrayList getFilteredNotes(SearchDefinition searchTerm)
	{
		ArrayList sortedNotes = new ArrayList();

		try
		{
			Comparator sorter = null;
			String sortFieldName = App.settings.getSortField();
			String sortDirection = App.settings.getSortDirection();
			
			if (sortFieldName.compareTo("modified") == 0)
			{
				sorter = new byModified(sortDirection);
			}
			else if (sortFieldName.compareTo("name") == 0)
			{
				sorter = new byName(sortDirection);
			}

			if (searchTerm == null)
			{
				Iterator notes = this.notesIndex.entrySet().iterator();
				
				while (notes.hasNext())
				{
					Map.Entry entry = (Map.Entry) notes.next();
					NoteIndexEntry thisNote = (NoteIndexEntry) entry.getValue();
					sortedNotes.add(thisNote);
				}
			}
			else
			{
				sortedNotes = filterNoteList(searchTerm);
			}

			Collections.sort(sortedNotes, sorter);
		}
		catch (Exception e)
		{
			App.logError("Notebook::GetNotes error: " + e.getMessage() + e.getStackTrace().toString());
			e.printStackTrace();
		}

		return sortedNotes;
	}
	
	// ----------------------------------------------------------------------------------
	private ArrayList filterNoteList(SearchDefinition searchTerm)
	{
		ArrayList filteredNotes = new ArrayList();
		Iterator notes = this.notesIndex.entrySet().iterator();
		
		int sep = this.filePath.lastIndexOf("/");
		String notesDirName = this.filePath.substring(0, sep) + "/" + this.id;
		
		String searchString = searchTerm.term;
		
		switch (searchTerm.option)
		{
			case SearchDefinition.SearchOptions.SEARCH_ALL:
				// Break up the words and build a regex to search for all words.
				// (?=.*?the)(?=.*?fox)(?=.*?dogs).*
				StringTokenizer allTokens = new StringTokenizer(searchString, " ");
				searchString = "";
				
				while (allTokens.hasMoreTokens())
				{
					searchString += "(?=.*?" + allTokens.nextToken() + ")";
				}
				
				searchString += ".*";
				break;
				
			case SearchDefinition.SearchOptions.SEARCH_ANY:
				// Break up the words and build a regex to search for all words.
				// .*(one|two|three).*
				StringTokenizer anyTokens = new StringTokenizer(searchString, " ");
				searchString = "";
				
				while (anyTokens.hasMoreTokens())
				{
					searchString += (searchString.length() == 0 ? "" : "|") + anyTokens.nextToken();
				}
				
				searchString = ".*(" + searchString + ").*";
				break;
				
			default:
				searchString = ".*" + searchString + ".*";
				break;
		}
		
		searchString = "(?is)" + searchString;
		Pattern searchRegex = new Pattern(searchString);
		Matcher matcher = searchRegex.matcher();
		boolean isMatch = false;
		
		while (notes.hasNext())
		{
			isMatch = false;
			Map.Entry entry = (Map.Entry) notes.next();
			NoteIndexEntry thisNote = (NoteIndexEntry) entry.getValue();
			
			// Search the note for the search term
			matcher.setTarget(thisNote.name);
			if (matcher.matches())
			{
				isMatch = true;
			}
			else
			{
				// Search note body
				String noteText = FileUtilities.readFile(notesDirName + "/note" + thisNote.id);
				matcher.setTarget(noteText);
				
				if (matcher.matches())
				{
					isMatch = true;
				}
			}
			
			if (isMatch)
			{
				filteredNotes.add(thisNote);
			}
		}

		return filteredNotes;
	}

	// ----------------------------------------------------------------------------------
	public Note getNote(String noteId)
	{
		// The notebook's note files should be in a subfolder with a name
		// matching the notebook ID
		int sep = this.filePath.lastIndexOf("/");
		String notesDirName = this.filePath.substring(0, sep) + "/" + this.id;

		File notesDir = new File(notesDirName);
		Note note = new Note(this);

		if (noteId.compareTo("autosave") == 0)
		{
			noteId = this.autosavedNote.id;
		}

		note.setId(noteId);

		// Find the note in the notebook
		if (noteId != null || this.notesIndex.containsKey(noteId))
		{
			NoteIndexEntry noteInfo = noteId.startsWith("autosave") ? this.autosavedNote : (NoteIndexEntry) this.notesIndex.get(noteId);
			this.autosavedNote = null;

			if (noteInfo != null)
			{
				note.setName(noteInfo.name == null ? "" : noteInfo.name);

				// Check if the notes directory exists
				if (notesDir.exists())
				{
					// Look for a file with the name "note<noteid>"
					String noteFileName = "note" + noteId;
					String noteText = FileUtilities.readFile(notesDirName + '/' + noteFileName);
					
					if (noteText != null)
					{
						if (noteId.startsWith("autosave"))
						{
							if (noteId.compareTo("autosave") == 0)
							{
								note.setId(null);
							}
							else
							{
								note.setId(noteId.substring(9));
							}
							
							note.setIsDirty(noteInfo.isDirty);
						}
						else
						{
							note.setId(noteInfo.id);
						}

						note.setName(noteInfo.name);
						note.setText(noteText);
						note.setFirstVisibleLine(noteInfo.top);
						note.setLastCaretPosition(noteInfo.caret);
//						note.setIsDirty(noteInfo.isDirty);
						note.setCreated(noteInfo.created);
						note.setModified(noteInfo.modified);
					}

					if (noteId.startsWith("autosave"))
					{
						deleteNote (noteId);
						App.settings.setHasSavedState(false);
						save();
					}
					else
					{
						note.setFirstVisibleLine(0);
						note.setLastCaretPosition(0);
					}
				}
			}
		}

		return note;
	}
	
	// ----------------------------------------------------------------------------------
	public void load()
	{
		try
		{
			if (this.id == null)
			{
				// ID is null, so try to open the default notebook.
				this.id = DefaultNotebokId;
				this.name = "My Notebook";
				this.fileName = "notebook.json";
			}
			else if (id.equals(DefaultNotebokId) || id.equals("nb001"))
			{
				this.name = "My Notebook";
				this.fileName = "notebook.json";
			}
			else
			{
				fileName = id.toString() + ".json";
			}

			this.filePath = System.getProperty("kindlet.home") + "/notebooks/" + fileName;
			String jsonNotebook = readNotebookIndexFile(this.filePath);
			
			if (jsonNotebook != null)
			{
				// Parse JSON string
				JSONObject notebookData = parseNotebookIndexJSON(jsonNotebook);

				if (notebookData != null)
				{
					String name = notebookData.get("name").toString();
					this.id = notebookData.get("id").toString();
					this.name = name;

					// Parse notes
					JSONArray notes = (JSONArray) notebookData.get("notes");

					if (notes != null)
					{
						for (int index = 0; index < notes.size(); index++)
						{
							JSONObject thisNote = (JSONObject) notes.get(index);
							NoteIndexEntry noteEntry = new NoteIndexEntry();
							noteEntry.id = thisNote.get("id").toString();
							noteEntry.name = thisNote.get("name").toString();

							if (thisNote.containsKey("caret"))
							{
								noteEntry.caret = Integer.parseInt(thisNote.get("caret").toString());
							}

							if (thisNote.containsKey("top"))
							{
								noteEntry.top = Integer.parseInt(thisNote.get("top").toString());
							}

							if (thisNote.containsKey("dirty"))
							{
								noteEntry.isDirty = thisNote.get("dirty").toString().compareTo("true") == 0 ? true : false;
							}
							else
							{
								noteEntry.isDirty = false;
							}
							
							try
							{
								noteEntry.created = this.sortDateFormatter.parse((String) thisNote.get("created"));
								noteEntry.modified = this.sortDateFormatter.parse((String) thisNote.get("modified"));
							}
							catch (java.text.ParseException e)
							{
								App.logError("Failed to parse note date. " + e.getStackTrace());
							}

							if (noteEntry.id.startsWith("autosave"))
							{
								this.autosavedNote = noteEntry;
							}
							else
							{
								this.notesIndex.put(noteEntry.id, noteEntry);
							}
						}
					}
				}
			}
			else
			{
				if (this.id.compareTo(DefaultNotebokId) == 0 && App.settings.isFistTimeUse)
				{
					createDefaultNote();
					App.settings.isFistTimeUse = false;
					save();
				}
			}
		}
		catch (Exception e)
		{
			App.logError(e.getMessage());
			e.printStackTrace();
		}
	}

	// ----------------------------------------------------------------------------------
	public static JSONObject parseNotebookIndexJSON(String jsonNotebookIndex)
	{
		JSONParser parser = new JSONParser();
		JSONObject notebookData = null;

		try
		{
			notebookData = (JSONObject) parser.parse(jsonNotebookIndex);
		}
		catch (ParseException pe)
		{
			App.logError("Load notebook - JSON Parse error: " + pe.getMessage());
			notebookData = null;
		}
		return notebookData;
	}

	// ----------------------------------------------------------------------------------
	public static String readNotebookIndexFile(String filePath)
	{
		String jsonNotebook = null;
		File notebookFile = new File(filePath);

		if (notebookFile.exists())
		{
			FileInputStream inputStream = null;
			byte[] fileBytes;

			// Read the file data into byte array
			try
			{
				fileBytes = new byte[(int) notebookFile.length()];
				inputStream = new FileInputStream(filePath);
				inputStream.read(fileBytes);

				// Convert byte array to String
				jsonNotebook = new String(fileBytes);
				fileBytes = null;
			}
			catch (FileNotFoundException e)
			{
				e.printStackTrace();
				App.logError(e.getMessage());
			}
			catch (IOException e)
			{
				e.printStackTrace();
				App.logError(e.getMessage());
			}
			finally
			{
				try
				{
					inputStream.close();
				}
				catch (IOException e)
				{
					e.printStackTrace();
					App.logError(e.getMessage());
				}
			}
		}
		
		return jsonNotebook;
	}

	// ----------------------------------------------------------------------------------
	private void createDefaultNote()
	{
		Date now = new Date();
		String welcomeNoteText = null;

		// Read welcome note text from resource
		try
		{
			InputStream stream = getClass().getResourceAsStream("/files/welcomeNote");

			try
			{
				welcomeNoteText = FileUtilities.convertStreamToString(stream);
			}
			catch (IOException e)
			{
				e.printStackTrace();
				App.logError(e.getMessage());
			}
			finally
			{
				if (stream != null)
				{
					try
					{
						stream.close();
					}
					catch (IOException e)
					{
						e.printStackTrace();
						App.logError(e.getMessage());
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			App.logError(e.getMessage());
		}

		// Create and save note
		now = new Date();
		Note welcomeNote = new Note(this);
		welcomeNote.setCreated(now);
		welcomeNote.setModified(now);
		welcomeNote.setId("_welcome");
		welcomeNote.setName("Welcome To TakeNote");
		welcomeNote.setText(welcomeNoteText);

		welcomeNote.save();
	}

	// ----------------------------------------------------------------------------------
	public void save()
	{
		try
		{
			FileOutputStream outputStream = null;
	
			// Write out the JSON file
			JSONObject jsonNotebook = new JSONObject();
			jsonNotebook.put("id", this.id);
			jsonNotebook.put("name", this.name);
			JSONArray notes = new JSONArray();
	
			if (this.notesIndex != null && this.notesIndex.isEmpty() == false)
			{
				Iterator iter = this.notesIndex.entrySet().iterator();
	
				while (iter.hasNext())
				{
					NoteIndexEntry note = (NoteIndexEntry) ((Map.Entry) iter.next()).getValue();
					
					JSONObject jsonNote = new JSONObject();
					jsonNote.put("id", note.id);
					jsonNote.put("name", note.name);
					jsonNote.put("top", String.valueOf(note.top));
					jsonNote.put("caret", String.valueOf(note.caret));
					jsonNote.put("created", this.sortDateFormatter.format(note.created));
					jsonNote.put("modified", this.sortDateFormatter.format(note.modified));
	
					if (note.isDirty)
					{
						jsonNote.put("dirty", "true");
					}
					
					notes.add(jsonNote);					
				}
	
				jsonNotebook.put("notes", notes);
			}
	
			// Get JSON string
			String notebookJsonString = jsonNotebook.toJSONString();
	
			// Save the data
			File notebookFile = new File(this.filePath);
	
			byte[] notebookBytes = notebookJsonString.getBytes();
	
			try
			{
				outputStream = new FileOutputStream(notebookFile);
				outputStream.write(notebookBytes);
			}
			catch (FileNotFoundException e)
			{
				e.printStackTrace();
				App.logError(e.getMessage());
			}
			catch (IOException e)
			{
				e.printStackTrace();
				App.logError(e.getMessage() + e.getStackTrace());
			}
			finally
			{
				try
				{
					outputStream.close();
				}
				catch (IOException e)
				{
					e.printStackTrace();
					App.logError(e.getMessage() + e.getStackTrace());
				}
			}
		}
		catch (Exception e)
		{
			e.getStackTrace();
			App.logError(e.getMessage());
		}
	}

	// ----------------------------------------------------------------------------------
	public int getNoteCount()
	{
		return this.notesIndex.size();
	}

	// ----------------------------------------------------------------------------------
	public void updateNoteEntry(Note note)
	{
		try
		{
			NoteIndexEntry noteInfo = null;

			// Find the note in the notebook
			if (this.notesIndex.containsKey(note.getId()))
			{
				noteInfo = (NoteIndexEntry) this.notesIndex.get(note.getId());
			}

			// If we don't have a notebook entry, create one. Most likely a new
			// note.
			if (noteInfo == null)
			{
				noteInfo = new NoteIndexEntry();
				noteInfo.id = note.getId();
				noteInfo.created = note.getCreated();

				// Add to notebook
				addNote(noteInfo);
			}

			noteInfo.created = note.getCreated();
			noteInfo.modified = note.getModified();
			noteInfo.name = note.getName();
			noteInfo.top = note.getFirstVisibleLine();
			noteInfo.caret = note.getLastCaretPosition();
			
			if (note.getId().startsWith("autosave"))
			{
				noteInfo.isDirty = note.getIsDirty();
			}
			
			// Save the notebook and reload
			save();
			load();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			App.logError(e.getMessage());
		}
	}

	// ----------------------------------------------------------------------------------
	public static void delete(String notebookId)
	{
		String notebooksPath = System.getProperty("kindlet.home") + "/notebooks/";
		String indexFilePath = notebooksPath + notebookId + ".json";
		String noteFolder = notebooksPath + notebookId + "/";
		
		// Delete all note files in the notebook folder
		File noteDirectory = new File(noteFolder);
		
		if (noteDirectory.exists())
		{
			String[] noteFiles = noteDirectory.list();
			
			if (noteFiles.length > 0)
			{
				for (int fileIndex = 0; fileIndex < noteFiles.length; fileIndex++)
				{
					String fileName = noteFolder + noteFiles[fileIndex];
					File noteFile = new File(fileName);
					noteFile.delete();
					noteFile = null;
				}
			}
			
			// Delete the notebook folder
			noteDirectory.delete();
			noteDirectory = null;
		}
		
		// Delete the notebook index file
		File notebookFile = new File(indexFilePath);
		notebookFile.delete();
	}

	// ----------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------
	class byModified implements Comparator
	{
		private boolean sortAscending = false;
		
		public byModified(String direction)
		{
			super();
			
			this.sortAscending = direction.equals("asc") ? true : false;
		}
		
		public int compare(Object entry1, Object entry2)
		{
			// return ((Date)entry1).compareTo((Date)entry2) * -1;

			Date entry1Modified = ((NoteIndexEntry) entry1).modified;
			Date entry2Modified = ((NoteIndexEntry) entry2).modified;

			return entry1Modified.compareTo(entry2Modified) * (sortAscending ? 1 : -1);
		}
	}

	// ----------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------
	private class byName implements Comparator
	{
		private boolean sortAscending = false;
		
		private byName(String direction)
		{
			super();
			
			this.sortAscending = direction.equals("asc") ? true : false;
		}
		
		public int compare(Object entry1, Object entry2)
		{
			int result = ((NoteIndexEntry) entry1).name.toLowerCase().compareTo(((NoteIndexEntry) entry2).name.toLowerCase());
			
			if (this.sortAscending == false)
			{
				result *= -1;
			}
			
			return result;
		}

	}

	// ----------------------------------------------------------------------------------
	public String exportNotes()
	{
		// Verify the export folder exists
		String exportFolderPath = App.context.getHomeDirectory() + "/exported";
		File exportFolder = new File(exportFolderPath);

		if (!exportFolder.exists())
		{
			exportFolder.mkdir();
		}
		
		// Validate the file name
		DateFormat fileDateFormatter = new SimpleDateFormat("yyyy-MMM-d HH-mm-ss");
		String exportFileName = "Notebook - " + this.name + " - " + fileDateFormatter.format(new Date()) + ".htm";
		String exportFilePath = exportFolderPath + "/" + FileUtilities.sanitize(exportFileName);
			
 		File exportFile = new File(exportFilePath);
 		
 		if (exportFile.exists())
 			exportFile.delete();
 		
		DateFormat displayDateFormatter = new SimpleDateFormat("MMM d, yyyy");
		StringBuffer htmlContent = new StringBuffer();

		try
		{
			if (exportFile.exists())
				exportFile.delete();

			htmlContent.append("<html>\n");
			htmlContent.append("\t<head>\n");
			htmlContent.append("\t\t<title>");
			htmlContent.append(this.name);
			htmlContent.append("</title>\n");
			htmlContent.append("\t\t<meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\" />\n");
			htmlContent.append("\t\t<style type=\"text/css\">\n");
			htmlContent.append("\t\t\thtml, body    { margin:0; padding:0; text-align:center; } \n");
			htmlContent.append("\t\t\t#pagewidth    { width:700px; text-align:left; margin:0 auto; } \n");
			htmlContent.append("\t\t\t#header       { padding:10px; position:relative; background-color:#090909; color:white; width:100%; display:block; text-align: center; } \n");
			htmlContent.append("\t\t\t.maincol      { padding:20px; position: relative; }\n");
			htmlContent.append("\t\t\t.note         { border-top: solid 4px black;  margin-bottom: 20px; }\n");
			htmlContent.append("\t\t\t.noteTitle    { font-size: 18pt; font-weight: bold; border-top: solid 2px silver;}\n");
			htmlContent.append("\t\t\t.noteSubTitle { font-size: 9pt; border-bottom: solid 1px silver; padding-bottom: 5px; }\n");
			htmlContent.append("\t\t\t.footer       { padding:10px; color: black; clear:both; width:100%; display:block; margin-bottom: 20px; border-bottom: solid 2px silver; border-top: solid 1px silver; text-align: center; } \n");
			htmlContent.append("\t\t</style>\n");
			htmlContent.append("\t</head>\n");
			htmlContent.append("\t<body>\n");
			htmlContent.append("\t\t<div id=\"pagewidth\" >\n");
			htmlContent.append("\t\t\t<div id=\"header\"><h1>Notebook [ " + this.name + " ]</h1></div>\n");
			htmlContent.append("\t\t\t<div class=\"wrapper clearfix\">\n");
			htmlContent.append("\t\t\t\t<div class=\"maincol\">\n");

			// Extract all notes
			Iterator notes = this.notesIndex.entrySet().iterator();

			while (notes.hasNext())
			{
				Map.Entry entry = (Map.Entry) notes.next();
				NoteIndexEntry thisNote = (NoteIndexEntry) entry.getValue();
				Note note = getNote(thisNote.id);

				htmlContent.append("\t\t\t\t\t<div class=\"note\">\n");
				htmlContent.append("\t\t\t\t\t<div class=\"noteTitle\">" + thisNote.name + "</div>\n");
				htmlContent.append("\t\t\t\t\t<div class=\"noteSubTitle\">Created on "
						+ displayDateFormatter.format(thisNote.created) + ",  Last Changed on "
						+ displayDateFormatter.format(thisNote.modified) + "</div>\n");

				String content = note.getText();

				if (content != null)
				{
					StringTokenizer splitter = new StringTokenizer(content, "\n");

					while (splitter.hasMoreTokens())
					{
						htmlContent.append("\t\t\t\t<p>" + splitter.nextToken().trim() + "</p>\n");
					}
				}

				htmlContent.append("\t\t\t</div>\n");
			}

			htmlContent.append("\t\t\t\t</div>\n");
			htmlContent.append("\t\t\t</div>\n");
			htmlContent.append("\t\t\t<div class=\"footer\">Created with <a href=\"http://www.izzylabs.com/takenote\">TakeNote</a>, the easy note manager for Kindle devices!</div>\n");
			htmlContent.append("\t\t</div>\n");
			htmlContent.append("\t</body>\n");
			htmlContent.append("</html>");

			FileUtilities.writeFile(exportFilePath, htmlContent.toString());

			return exportFileName;
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			App.logError("Failed to export notes.  " + ex.getMessage());
			return null;
		}
	}

	// ----------------------------------------------------------------------------------
	public static String sanitizeName(String name) 
	{
		final String invalidChars = "[:\\\\/*?|<>]";
		
		for (int i = 0; i < invalidChars.length(); i++)
		{
			if (name.indexOf(invalidChars.charAt(i)) >= 0)
			{
				//name = name.substring(0, i + 1) + "_" + nam
				name = name.replace(invalidChars.charAt(i), '_');
			}
		}
		
	    return name;
	}


}

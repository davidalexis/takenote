/*
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * Copyright (c) 2012, David Alexis. All rights reserved.
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */

package com.izzylabs.kindle;

public final class NotebookMetadata
{
	String name;
	String id;
	int noteCount;

	public NotebookMetadata(String name, String id, int noteCount)
	{
		this.name = name;
		this.id = id;
		this.noteCount = noteCount;
	}
}

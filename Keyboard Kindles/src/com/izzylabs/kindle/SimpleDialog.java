/*
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * Copyright (c) 2012, David Alexis. All rights reserved.
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */

package com.izzylabs.kindle;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import jregex.Matcher;
import jregex.Pattern;

import com.amazon.kindle.kindlet.ui.*;
import com.amazon.kindle.kindlet.ui.KindletUIResources.KColorName;
import com.amazon.kindle.kindlet.ui.KindletUIResources.KFontStyle;

public class SimpleDialog extends RoundedDialog
{
	private static final long		serialVersionUID	= 1L;
	protected KTextField			textField;
	protected ActionListener 		onCloseHandler;
	protected String				enteredText;
	protected KTextArea 			message;
	protected int 					maxLength;
	protected boolean				mustValidate;
	protected String				validationPattern;
	protected Pattern				validator;
	protected Matcher				validationMatcher;
	protected static final Color 	borderColor = KindletUIResources.getInstance().getColor(KColorName.BLACK);
	protected static final Font 	font =  KindletUIResources.getInstance().getFont(KindletUIResources.KFontFamilyName.SANS_SERIF, 21, KFontStyle.PLAIN);
//	protected Timer 				focusTimer;
	protected KButton				okButton;
	
	public SimpleDialog()
	{
		super("");
		setSizeAndPosition(50, 55, 500, 220);
		this.setFocusable(false);
		
		// Build dialog - message, JTextField, key listener for validation		
		final KButton okButton = new KButton("OK");
		okButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				closeDialog(1);
			}
		});
		
//		final ImageIcon icon = new ImageIcon(getClass().getResource("images/close.png"));
		final KButton cancelButton = new KButton("Cancel");
		cancelButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				SimpleDialog.this.textField.setText("");
				closeDialog(0);
			}
		});
		
		this.setCloseHandler(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				SimpleDialog.this.textField.setText("");
				closeDialog(0);
			}
		});

		message = new KTextArea("");
		message.setEditable(false);
		message.setFocusable(false);
		container.add(message, BorderLayout.NORTH);
		
		// Add text entry text box
		this.textField = new KTextField()
		{
			private static final long	serialVersionUID	= 1L;

			public Insets getInsets()
			{
				return new Insets(2, 5, 2, 5);
			}
		};
		
		this.textField.setFont(font);
	
		this.textField.addKeyListener(new KeyAdapter()
		{
			public void keyTyped(KeyEvent e)
			{
				if (mustValidate)
				{
					String text = textField.getText();
					validationMatcher.setTarget(text);
					okButton.setEnabled(true);
					
					if (validationMatcher.matches() == false)
					{
						if (SimpleDialog.this.maxLength > 0 && text.length() > SimpleDialog.this.maxLength)
						{
							SimpleDialog.this.textField.setText(text.substring(0, SimpleDialog.this.maxLength));
							okButton.setEnabled(true);
						}
						else
						{
							okButton.setEnabled(false);
						}
					}	
					else
					{
						okButton.setEnabled(true);
					}
				}
			}
		});
		
		container.add(this.textField, BorderLayout.CENTER);
		
		this.addComponentListener(new ComponentAdapter()
		{			
			public void componentShown(ComponentEvent e)
			{
				validate();
				repaint();
				
				SimpleDialog.this.textField.requestFocus();
			}
		});
	}
	
	public void showDialog(String title, String message, String initialValue, int maxLength, String validationPattern, ActionListener onClose)
	{
		this.maxLength = maxLength;
		this.onCloseHandler = onClose;
		this.enteredText = null;
		this.header.setTitle(title);
		this.textField.setText(initialValue);
		this.message.setText(message);

		App.controller.modalDialogOpen = true;
		
		if (validationPattern.length() == 0 && maxLength == 0)
		{
			this.mustValidate = false;
			this.validationPattern = null;
			this.validationMatcher = null;
			this.validator = null;
		}
		else
		{
			this.mustValidate = true;
			
			if (validationPattern.length() > 0)
			{
				this.validationPattern = validationPattern;
			}
			else
			{
				this.validationPattern = ".";
			}
			
			if (maxLength > 0)
			{
				this.validationPattern = "^(" + this.validationPattern + "){0," + maxLength + "}$";
			}

			this.validator = new Pattern(this.validationPattern);
			this.validationMatcher = this.validator.matcher();
		}
		
		this.setVisible(true);
		this.repaint();
	}
	
	private void closeDialog(int eventId)
	{
		if (eventId == 1)
		{
			this.enteredText = this.textField.getText();
		}
		else
		{
			this.enteredText = null;
		}
		
		this.setVisible(false);
		this.textField.setText("");
		App.controller.modalDialogOpen = false;
		
		this.onCloseHandler.actionPerformed(new ActionEvent(this, eventId, this.enteredText));
	}

}

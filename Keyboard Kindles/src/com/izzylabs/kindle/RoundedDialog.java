/*
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * Copyright (c) 2012, David Alexis. All rights reserved.
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */

package com.izzylabs.kindle;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.amazon.kindle.kindlet.ui.KPanel;
import com.amazon.kindle.kindlet.ui.KindletUIResources;
import com.amazon.kindle.kindlet.ui.KindletUIResources.KColorName;
import com.amazon.kindle.kindlet.ui.KindletUIResources.KFontStyle;

//----------------------------------------------------------------------------------
public class RoundedDialog extends KPanel
{
	private static final long	serialVersionUID	= 1L;
	protected ActionListener 	closeActionListener;
	protected KPanel 			container;
	protected KPanel 			buttonContainer;
	protected KPanel			dialogFrame;
	protected TitlePanel 		header;
	protected Color 			borderColor = KindletUIResources.getInstance().getColor(KColorName.BLACK);
	protected final Font 		font        =  KindletUIResources.getInstance().getFont(KindletUIResources.KFontFamilyName.SANS_SERIF, 21, KFontStyle.PLAIN);
	
	// ----------------------------------------------------------------------------------
	public RoundedDialog()
	{
	}
	
	// ----------------------------------------------------------------------------------
	public RoundedDialog(String title)
	{
		setLayout(null);
		setBounds(0, 0, 600, 700);
		this.setFocusable(false);
		
		dialogFrame = new KPanel(new BorderLayout())
		{
			private static final long	serialVersionUID	= 1L;

			// ----------------------------------------------------------------------------------
			public Insets getInsets()
			{
				return new Insets(2, 2, 2, 2);
			}
			
			// ----------------------------------------------------------------------------------
			public void paint(final Graphics g) 
			{
			    int x = 2;
			    int y = 2;
			    int w = getWidth() - 4;
			    int h = getHeight() - 4;
			    int arc = 30;

			    KindletUIResources resources = KindletUIResources.getInstance();
			    
			    Graphics2D g2 = (Graphics2D) g.create();

			    g2.setColor(resources.getColor(KColorName.WHITE));
			    g2.fillRoundRect(x, y, w, h, arc, arc);

			    g2.setStroke(new BasicStroke(3f));
			    g2.setColor(resources.getColor(KColorName.BLACK));
			    g2.drawRoundRect(x, y, w, h, arc, arc); 

			    g2.dispose();
			}
		};
		
		dialogFrame.setFocusable(false);
		this.add(dialogFrame);
		
		header = new TitlePanel(title)
		{
			private static final long	serialVersionUID	= 1L;

			// ----------------------------------------------------------------------------------
			public void paint(final Graphics g) 
			{
			    int x = 0;
			    int y = 0;
			    int w = getWidth();
			    int h = getHeight();
			    int arc = 30;

			    KindletUIResources resources = KindletUIResources.getInstance();
			    Graphics2D g2 = (Graphics2D) g.create();

			    g2.setColor(resources.getColor(KColorName.GRAY_02));
			    g2.fillRoundRect(x, y, w, h - 2, arc, arc);

			    g2.setStroke(new BasicStroke(3f));
			    g2.setColor(resources.getColor(KColorName.BLACK));
			    g2.drawRoundRect(x, y, w, h - 2, arc, arc); 

			    g2.dispose();
			}
		};
		
//		header.setShouldDrawBorder(false);
		header.setFocusable(false);
		
		dialogFrame.add(header, BorderLayout.NORTH);
		
		container = new KPanel(new BorderLayout(10, 10))
		{
			private static final long	serialVersionUID	= 1L;
			public Insets getInsets()
			{
				return new Insets(10, 10, 10, 10);
			}
		};
		
		container.setFocusable(false);
		
		dialogFrame.add(container, BorderLayout.CENTER);
		
		buttonContainer = new KPanel(new FlowLayout(FlowLayout.RIGHT, 8, 5))
		{
			private static final long	serialVersionUID	= 1L;

			public Insets getInsets()
			{
				return new Insets(0, 5, 0, 5);
			}
		};

		buttonContainer.setFocusable(false);
		
		container.add(buttonContainer, BorderLayout.SOUTH);
		
		setSizeAndPosition(50, 55, 500, 315);
	}

	// ----------------------------------------------------------------------------------
	public void setCloseHandler(ActionListener closeHandler)
	{
		this.closeActionListener = closeHandler;
	}
	
	// ----------------------------------------------------------------------------------
	public void setSizeAndPosition(int top, int left, int width, int height)
	{
		dialogFrame.setSize(width, height);
		dialogFrame.setBounds(top, left, width, height);
	}

	// ----------------------------------------------------------------------------------
	protected void closeDialog()
	{
		this.closeActionListener.actionPerformed(new ActionEvent(this, 1, null));
	}
}
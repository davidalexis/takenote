/*
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * Copyright (c) 2012, David Alexis. All rights reserved.
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */

package com.izzylabs.kindle;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import com.amazon.kindle.keyboard.NumberLegend;
import com.amazon.kindle.kindlet.event.KindleKeyCodes;
import com.amazon.kindle.kindlet.ui.*;
import com.amazon.kindle.kindlet.ui.KindletUIResources.KFontStyle;
import com.amazon.kindle.kindlet.ui.border.KLineBorder;

final public class NoteEditor implements IViewable
{
	protected NoteEditorView 	view;
	protected Note 				note;
	
	// ----------------------------------------------------------------------------------
	public Container getView()
	{
		return this.view;
	}
	
	// ----------------------------------------------------------------------------------
	public void setView(Container view)
	{
		this.view = (NoteEditorView)view;
	}
	
	// ----------------------------------------------------------------------------------
	public String getViewName()
	{
		return "editor";
	}
	
	// ----------------------------------------------------------------------------------
	public int getViewId()
	{
		return App.EDITORVIEW;
	}

	// ----------------------------------------------------------------------------------
	public NoteEditor(String noteId)
	{
		try
		{
			this.view = new NoteEditorView();
			this.view.isLoading = true;
			
			// Get note data and load it into the view
			if (noteId != null)
			{
				this.note = App.controller.getCurrentNotebook().getNote(noteId);
				
				String noteText = note.getText();
				this.view.titleField.setText(note.getName());
				this.view.contentField.setText(noteText == null ? "" : noteText);
				
				if (noteId != null && noteId.compareTo("autosave") == 0)
				{
					this.view.setIsDirty(note.getIsDirty());
					this.view.contentField.setCaretPosition(note.getLastCaretPosition());
				}
				else
				{
					this.view.setIsDirty(false);
					this.view.titleField.setCaretPosition(0);
					this.view.contentField.setCaretPosition(0);
				}
			}
			else
			{
				this.note = new Note(App.controller.getCurrentNotebook());
				this.view.titleField.setText("");
				this.view.contentField.setText("");
				this.view.setIsDirty(false);
				this.view.titleField.setCaretPosition(0);
				this.view.contentField.setCaretPosition(0);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		this.view.isLoading = false;
			
	}
	
	// ----------------------------------------------------------------------------------
	public void saveState()
	{
		if (this.note == null || this.note.getId() == null)
		{
			this.note = new Note(App.controller.getCurrentNotebook());
			this.note.setId("autosave");
		}
		else
		{
			this.note.setId("autosave-" + this.note.getId());
		}

		this.note.setName(this.view.titleField.getText());
		this.note.setText(this.view.contentField.getText());
		this.note.setIsDirty(this.view.isDirty);
		this.note.setLastCaretPosition(this.view.contentField.getCaretPosition());
		this.note.save();
		
		App.settings.setHasSavedState(true);
	}
		
	// ----------------------------------------------------------------------------------
	public class NoteEditorView extends KPanel
	{
		public boolean	isLoading;

		private static final long	serialVersionUID	= 1L;
		
		public KTextField			titleField;
		public TextArea				contentField;
		private KPanel				footerArea;
		private KMenu				menu;
		private KButton				saveButton;
//		private KButton				cancelButton;
		private boolean				saveEnabled;
		private KMenuItem			saveMenuItem;
		private KMenuItem			saveAndCloseMenuItem;
		private KMenuItem			deleteMenuItem;
		private KMenuItem			gotoTopMenuItem;
		private KMenuItem			gotoEndMenuItem;
		private boolean				isDirty;
		protected int				currentFontSize;
		private KTextOptionPane		textOptionPane;
		private KMenuItem			cancelMenuItem;
		private SymbolDialog		symbolDialog;
		private KMenuItem			saveAsNewMenuItem;
		private FooterPanel			footerPanel;
		private KTextOptionFontMenu	fontMenu;
		private KTextOptionListMenu	numberLegendMenu;
		private int					titleCaretPosition;
		private KLabel				memoryInfoLabel;

//		private KButton	undoButton;
//		private KButton	redoButton;
	    
		// ------------------------------------------------------------------------------
	    public NoteEditorView()
		{
			createUI();
			createMenu();
		}
		
	 // ----------------------------------------------------------------------------------
 		private void createUI()
 		{
 			try
 			{
 				Dimension screenSize = App.context.getRootContainer().getSize();

 				setLayout(new BorderLayout(0, 0));
 				setFocusable(false);

 				final TitlePanel title = new TitlePanel("Editor");
 				this.add(title, BorderLayout.NORTH);

 				this.symbolDialog = new SymbolDialog(new ActionListener()
 				{
 					public void actionPerformed(ActionEvent e)
 					{
 						String symbol = e.getActionCommand();

 						switch (e.getID())
 						{
 							case 600:
 								// Accept a symbol
 								contentField.insertChar(symbol.toCharArray()[0]);
 								checkDirty(symbol.charAt(0));
 								break;

 							case 601:
 								String content = titleField.getText();
 								int textLength = content.length();
 								titleField.setText(content.substring(0, titleCaretPosition) + symbol + ((titleCaretPosition) < textLength ? content.substring(titleCaretPosition) : ""));
 								titleCaretPosition = titleCaretPosition + 1;
 								break;

 							case 610:
 								// Close the dialog
 								contentField.requestFocus();
 								setMenu();
 								break;

 							case 611:
 								// Close the dialog
 								titleField.requestFocus();
 								setMenu();
 								break;
 						}

 						repaint();
 					}
 				});

 				add(this.symbolDialog);

 				int symDialogWidth = this.symbolDialog.getPreferredSize().width;
 				int symDialogHeight = this.symbolDialog.getPreferredSize().height;
 				this.symbolDialog.setBounds((screenSize.width - symDialogWidth) / 2, screenSize.height / 2, symDialogWidth, symDialogHeight);
 				this.symbolDialog.setVisible(false);

 				this.saveEnabled = false;

 				titleField = new KTextField("", "Add a title (or name) for your note.")
 				{
 					private static final long	serialVersionUID	= 1L;

 					protected void processFocusEvent(FocusEvent e)
 					{
 						super.processFocusEvent(e);

 						try
 						{
 							if (NoteEditorView.this.isShowing())
 							{
 								if (e.getID() == FocusEvent.FOCUS_LOST)
 								{
 									if (NoteEditorView.this.isShowing() && e.getOppositeComponent() == null)
 									{
 										requestFocus();
 									}
 									else
 									{
 										titleCaretPosition = titleField.getCaretPosition();
 									}
 								}
 								else
 								{
 									gotoEndMenuItem.setEnabled(false);
 									gotoTopMenuItem.setEnabled(false);
 									int textLength = titleField.getText().length();

 									processKeyEvent(new KeyEvent(titleField, KeyEvent.KEY_PRESSED, System.currentTimeMillis(), 0, KindleKeyCodes.VK_FIVE_WAY_RIGHT, KeyEvent.CHAR_UNDEFINED));

 									if (titleCaretPosition < 0)
 										titleCaretPosition = 0;

 									if (titleCaretPosition >= textLength)
 										titleCaretPosition = textLength == 0 ? 0 : textLength;

 									titleField.setCaretPosition(titleCaretPosition);
 								}

 								validateNote();
 							}
 						}
 						catch (Exception ex)
 						{
 							App.logError(ex.getMessage());
 							ex.printStackTrace();
 						}
 					}
 				
// 					public void paint(final Graphics g)
// 					{
// 						super.paint(g);
// 						int width = getWidth();
// 						int height = getHeight() - 2;
// 						g.setColor(KindletUIResources.getInstance().getColor(KColorName.BLACK));
// 						g.drawRect(1, 1, width - 2, height);
// 					}

 					public Insets getInsets()
 					{
 						return new Insets(5, 5, 5, 5);
 					}
 				};
 				titleField.setBorder(new KLineBorder(2));
 				contentField = new TextArea("", "Add the content of your note here.");
 				footerArea = new KPanel(new BorderLayout());
 				titleField.setFont(App.settings.getFont(Settings.NoteTitleFont));
 				contentField.setFont(App.settings.getFont(Settings.NoteBodyFont));
 				
 				KPanel buttonBar = new KPanel(new BorderLayout());
 				KPanel buttonPanel = new KPanel();
 				saveButton = new KButton("Save Note");
 				saveButton.setEnabled(false);
// 				cancelButton = new KButton("Cancel");
 				buttonPanel.add(saveButton);
// 				buttonPanel.add(cancelButton);
 				buttonBar.add(buttonPanel, BorderLayout.EAST);
 				buttonBar.setFocusable(false);
 				buttonPanel.setFocusable(false);

 				this.memoryInfoLabel = new KLabel("");
 				Font memoryLabelFont = KindletUIResources.getInstance().getFont(KindletUIResources.KFontFamilyName.SANS_SERIF, 16, KFontStyle.PLAIN, true);
 				this.memoryInfoLabel.setFont(memoryLabelFont);
 				this.memoryInfoLabel.setVisible(false);
 				buttonBar.add(this.memoryInfoLabel, BorderLayout.WEST);

 				footerArea.setFocusable(false);
 				this.footerPanel = new FooterPanel();
 				footerArea.add(this.footerPanel, BorderLayout.SOUTH);
 				footerArea.add(buttonBar, BorderLayout.NORTH);
 				footerPanel.setFocusable(false);

 				KPanel editorContainer = new KPanel()
 				{
 					private static final long	serialVersionUID	= 1L;
 					private final Insets		insets				= new Insets(5, 10, 5, 10);

 					{
 						setLayout(new BorderLayout(5, 5));
 					}

 					public Insets getInsets()
 					{
 						return insets;
 					}
 				};

 				editorContainer.setFocusable(false);
 				add(editorContainer);

 				editorContainer.add(titleField, BorderLayout.NORTH);
 				editorContainer.add(contentField, BorderLayout.CENTER);
 				editorContainer.add(footerArea, BorderLayout.SOUTH);

 				contentField.addFocusListener(new FocusAdapter()
 				{
 					public synchronized void focusGained(FocusEvent e)
 					{
 						try
 						{
 							setGotoMenuActive(true);
 							titleField.setCaretPosition(0);
 						}
 						catch (Exception ex)
 						{
 							App.logError(ex.getMessage());
 							ex.printStackTrace();
 						}
 					}
 				});

 				saveButton.addFocusListener(new FocusListener()
 				{

 					public void focusLost(FocusEvent e)
 					{
 						try
 						{
 							if (e.getOppositeComponent() == null)
 							{
 								contentField.requestFocus();
 							}
 						}
 						catch (Exception ex)
 						{
 							App.logError(ex.getMessage());
 							ex.printStackTrace();
 						}
 					}

 					public void focusGained(FocusEvent e)
 					{
 						setGotoMenuActive(false);
 					}
 				});

// 				cancelButton.addFocusListener(new FocusAdapter()
// 				{
// 					public void focusGained(FocusEvent arg0)
// 					{
// 						setGotoMenuActive(false);
// 					}
// 				});

 				textOptionPane = new KTextOptionPane();

 				// Add the font size menu to the text option panel
 				textOptionPane.addFontSizeMenu(createFontMenu());

 				if (App.isBigScreen() == true || this.footerPanel.legend.doesNumberExist(App.context) == false)
 				{
 					textOptionPane.addListMenu(createNumberLegendToggleMenu());
 				}

 				createMenu();

 				titleField.addKeyListener(new KeyHandler());
 				contentField.addKeyListener(new KeyHandler());

 				// Adds an action listener for the button to save the note
 				saveButton.addActionListener(new ActionListener()
 				{
 					public void actionPerformed(ActionEvent e)
 					{
						NoteEditorView.this.updateAndSaveNote();
						NoteEditorView.this.closeView();
 					}
 				});

// 				// Add an ActionListener to the Cancel button to close the editor
// 				cancelButton.addActionListener(new ActionListener()
// 				{
// 					public void actionPerformed(ActionEvent e)
// 					{
// 						NoteEditorView.this.closeView();
// 					}
//
// 				});

 				// set name field as the default field when this card is displayed
 				addComponentListener(new ComponentAdapter()
 				{
 					public void componentShown(ComponentEvent e)
 					{
 						try
 						{
 							verifyFocus();
 						}
 						catch (Exception ex)
 						{
 							App.logError("componentShown error: " + ex.getMessage());
 							ex.printStackTrace();
 						}
 					}
 					
 					public void componentResized(ComponentEvent e)
 					{
 						try
 						{
 							verifyFocus();
 						}
 						catch (Exception ex)
 						{
 							App.logError("componentShown error: " + ex.getMessage());
 							ex.printStackTrace();
 						}
 					}
 				});
 			}
 			catch (Exception ex)
 			{
 				App.logError("CreateUI error: " + ex.getMessage());
 				ex.printStackTrace();
 			}
 		}
 		
 		// ----------------------------------------------------------------------------------
 		private void createMenu()
 		{
 			try
 			{
 				if (this.menu == null)
 				{
 					this.menu = new KMenu();

 					// Create Save menu item
 					this.saveMenuItem = new KMenuItem("Save and Continue Editing");
 					saveMenuItem.addActionListener(new ActionListener()
 					{
 						public void actionPerformed(ActionEvent e)
 						{
 							// Save the note then exit
 							NoteEditorView.this.updateAndSaveNote();
 						}
 					});
 					this.menu.add(saveMenuItem);

 					this.saveAndCloseMenuItem = new KMenuItem("Save and Close");
 					saveAndCloseMenuItem.addActionListener(new ActionListener()
 					{
 						public void actionPerformed(ActionEvent e)
 						{
 							// Save the note then exit
 							NoteEditorView.this.updateAndSaveNote();
 							closeView();
 						}
 					});
 					this.menu.add(saveAndCloseMenuItem);
 					
 					// Create cancel menu item
 					cancelMenuItem = new KMenuItem("Close Without Saving");
 					cancelMenuItem.addActionListener(new ActionListener()
 					{
 						public void actionPerformed(ActionEvent e)
 						{
 							closeView();
 						}
 					});
 					this.menu.add(cancelMenuItem);
 					
 					// Create Goto Top and Goto End menu items
 					this.gotoTopMenuItem = new KMenuItem("Go to Top");
 					this.menu.add(this.gotoTopMenuItem);
 					this.gotoTopMenuItem.addActionListener(new ActionListener()
 					{
 						public void actionPerformed(ActionEvent e)
 						{
 							contentField.setCaretPosition(0);
 						}
 					});

 					this.gotoEndMenuItem = new KMenuItem("Go to Bottom");
 					this.menu.add(this.gotoEndMenuItem);
 					this.gotoEndMenuItem.addActionListener(new ActionListener()
 					{
 						public void actionPerformed(ActionEvent e)
 						{
 							contentField.gotoEnd();
 						}
 					});
 					
 					// Create a new note based on this one
 					saveAsNewMenuItem = new KMenuItem("Save As New Note");
 					saveAsNewMenuItem.addActionListener(new ActionListener()
 					{
 						public void actionPerformed(ActionEvent e)
 						{
 							NoteEditorView.this.saveNoteAsNew();
 						}
 					});
 					this.menu.add(saveAsNewMenuItem);

 					// Create Delete menu item
 					deleteMenuItem = new KMenuItem("Delete This Note");
 					deleteMenuItem.addActionListener(new ActionListener()
 					{
 						public void actionPerformed(ActionEvent e)
 						{
 							KOptionPane.showConfirmDialog(NoteEditorView.this, "This note will be permanently deleted.  Would you like to continue?", "Delete This Note", new KOptionPane.ConfirmDialogListener()
 							{
 								public void onClose(final int option)
 								{
 									if (option == KOptionPane.OK_OPTION)
 									{
 										NoteEditor.this.note.delete();
 										closeView();
 									}
 								}
 							});
 						}
 					});
 					this.menu.add(deleteMenuItem);
 					
 					// Create Help menu item
 					final KMenuItem helpNewMenuItem = new KMenuItem("TakeNote Help");
 					helpNewMenuItem.addActionListener(new ActionListener()
 					{
 						public void actionPerformed(ActionEvent e)
 						{
 							App.controller.getEventHandler().actionPerformed(new ActionEvent(NoteEditorView.this, App.HELPVIEW, null));
 						}
 					});
 					this.menu.add(helpNewMenuItem);	
 				}
 			}
 			catch (Exception ex)
 			{
 				ex.printStackTrace();
 			}
 		}

 		// ----------------------------------------------------------------------------------
 		private void setGotoMenuActive(boolean isActive)
 		{
 			gotoEndMenuItem.setEnabled(isActive);
 			gotoTopMenuItem.setEnabled(isActive);
 			repaint();
 		}
 		
 		// ----------------------------------------------------------------------------------
 		public void setCaretPosition(String name, int caretPosition)
 		{
 			if (name.compareTo("Textbox") == 0)
 			{
 				this.titleField.setCaretPosition(caretPosition);
 			}
 			else
 			{
 				this.contentField.setCaretPosition(caretPosition);
 			}
 		}

 		// ----------------------------------------------------------------------------------
 		public int getCaretPosition(String name)
 		{
 			if (name.compareTo("titleField") == 0)
 			{
 				return this.titleField.getCaretPosition();
 			}
 			else
 			{
 				return this.contentField.getCaretPosition();
 			}
 		}

//	    // ----------------------------------------------------------------------------------
//	    private boolean undoContentEdit()
//	    {
////	    	if (this.undoStack.size() > 0)
////	    	{
//////	    		UndoableEdit edit = (UndoableEdit)this.undoStack.pop();
//////	    		edit.undo();
//////	    		this.redoStack.push(edit);
//////	    		this.redoButton.setEnabled(true);
//////	    		
//////	    		if (this.undoStack.size() == 0)
//////	    		{
//////	    			this.undoButton.setEnabled(false);
//////	    		}
////	    		
////	    		return true;
////	    	}
////	    	else
////	    	{
//	    		return false;
////	    	}
//	    }
	    
//	    // ----------------------------------------------------------------------------------
//	    private boolean redoContentEdit()
//	    {
//	    	if (this.redoStack.size() > 0)
//	    	{
//	    		UndoableEdit edit = (UndoableEdit)this.redoStack.pop();
//	    		edit.redo();
//	    		this.undoStack.push(edit);
//	    		this.undoButton.setEnabled(true);
//	    		
//	    		if (this.redoStack.size() == 0)
//	    		{
//	    			this.redoButton.setEnabled(false);
//	    		}
//	    		
//	    		return true;
//	    	}
//	    	else
//	    	{
//	    		return false;
//	    	}
//	    }
	    
		// ----------------------------------------------------------------------------------
		private void setMenu()
		{
			try
			{
				if (Thread.interrupted() == false)
				{
					// Set the menu for the sample application
					App.context.setMenu(this.menu);

					// Set the text option panel for the sample application
					App.context.setTextOptionPane(this.textOptionPane);
				}
			}
			catch (Exception ex)
			{
				//ex.printStackTrace();
			}
		}

		// ----------------------------------------------------------------------------------
		// Create the font size menu
		// ----------------------------------------------------------------------------------
		private KTextOptionFontMenu createFontMenu()
		{
			// Create a font size menu to control font size of the sample text area
			fontMenu = new KTextOptionFontMenu();
			int size = App.settings.getFontSize();
			fontMenu.setSelected(size);

			try
			{
				fontMenu.addItemListener(new ItemListener()
				{
					public void itemStateChanged(final ItemEvent e)
					{
						// The incoming event is actually an KTextOptionEvent, which
						// has
						// the ability
						// to distinguish between a visit and a select for the item.
						// @see KTextOptionEvent#isVisit
						// KTextOptionEvent textOptionEvent = (KTextOptionEvent) e;

						if (((KTextOptionEvent) e).isVisit())
						{
							// If the current event is a visit, show a preview of
							// the
							// font size we are changing to
							int thisSize = fontMenu.getSelectedFontSize();
							setFontSize(thisSize);
						}
						else
						{
							// If the current event is not a visit, either commit or
							// revert the font size change
							if (e.getStateChange() == ItemEvent.SELECTED)
							{
								// Commit the change and update the currentFontSize
								// if
								// the event is a select
								currentFontSize = fontMenu.getSelectedFontSize();

								// Save font size
								App.settings.setFontSize(currentFontSize);
							}
							else
							{
								// Revert to the original currentFontSize if the
								// even is
								// not a select
								setFontSize(currentFontSize);
							}
						}
					}
				});

				return fontMenu;
			}
			catch (Exception ex)
			{
			}

			return null;
		}

		// ----------------------------------------------------------------------------------
		private void setFontSize(final int size)
		{
			App.settings.setFontSize(size);
			this.contentField.setFont(App.settings.getFont(Settings.NoteBodyFont));
			this.titleField.setFont(App.settings.getFont(Settings.NoteTitleFont));
			this.repaint();
		}

		// ----------------------------------------------------------------------------------
		private KTextOptionListMenu createNumberLegendToggleMenu()
		{
			try
			{
				// Create the list menu with the label "Title Labels"
				numberLegendMenu = new KTextOptionListMenu("Number Legend");
				numberLegendMenu.add(new KTextOptionMenuItem("auto"));
				numberLegendMenu.add(new KTextOptionMenuItem("on"));
				numberLegendMenu.add(new KTextOptionMenuItem("off"));

				String legendSetting = App.settings.getNumberLegendToggle();
				int selectedIndex = 0;

				if (legendSetting.compareTo("auto") == 0)
					selectedIndex = 0;
				else if (legendSetting.compareTo("on") == 0)
					selectedIndex = 1;
				else if (legendSetting.compareTo("off") == 0)
					selectedIndex = 2;

				numberLegendMenu.setSelectedIndex(selectedIndex);

				numberLegendMenu.addItemListener(new ItemListener()
				{
					public void itemStateChanged(final ItemEvent e)
					{
						// The incoming event is actually an KTextOptionEvent, which
						// has
						// the ability
						// to distinguish between a visit and a select for the item.
						// @see KTextOptionEvent#isVisit
						// final KTextOptionEvent textOptionEvent =
						// (KTextOptionEvent)e;

						if (!((KTextOptionEvent) e).isVisit() && e.getStateChange() == ItemEvent.SELECTED)
						{
							App.settings.setNumberLegendToggle(numberLegendMenu.getSelected().getValue().toString());
							NoteEditorView.this.footerPanel.setNumberLegendVisibility(App.settings.getNumberLegendToggle());
						}

						if (((KTextOptionEvent) e).isVisit())
						{
							// If the current event is a visit, show a preview of
							// the
							// font size we are changing to
							NoteEditorView.this.footerPanel.setNumberLegendVisibility(numberLegendMenu.getSelected().getValue().toString());
						}
						else
						{
							// If the current event is not a visit, either commit or
							// revert the font size change
							if (e.getStateChange() == ItemEvent.SELECTED)
							{
								// Commit the change and update the currentFontSize
								// if
								// the event is a select
								App.settings.setNumberLegendToggle(numberLegendMenu.getSelected().getValue().toString());
							}

							NoteEditorView.this.footerPanel.setNumberLegendVisibility(App.settings.getNumberLegendToggle());
						}

					}
				});

				return numberLegendMenu;
			}
			catch (Exception ex)
			{
				return null;
			}
		}

		// ----------------------------------------------------------------------------------
		private void validateNote()
		{
			try
			{
				boolean shouldEnable;
				boolean hasTitle = titleField.getText().trim().length() != 0;
				if (hasTitle == false || this.isDirty == false)
					shouldEnable = false;
				else
					shouldEnable = true;

				if (shouldEnable != this.saveEnabled)
				{
					this.saveEnabled = shouldEnable;
				}

				this.saveButton.setEnabled(this.saveEnabled);
				this.saveMenuItem.setEnabled(hasTitle && this.saveEnabled);
				this.saveAndCloseMenuItem.setEnabled(hasTitle && this.saveEnabled);
				this.saveAsNewMenuItem.setEnabled(hasTitle);
				boolean deleteEnabled = !(NoteEditor.this.note == null || NoteEditor.this.note.getId() == null);
				this.deleteMenuItem.setEnabled(deleteEnabled);
				repaint();
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
			}
		}



//		// ------------------------------------------------------------------------------
//		private Component createButtonBar()
//		{
//			ButtonBar bar = new ButtonBar("");
//
//			// Add undo button
//			this.undoButton = bar.addButton("undo", "images/undo.png", -1, "undo", new ActionListener()
//			{
//				public void actionPerformed(ActionEvent e)
//				{
//					if (!undoContentEdit())
//					{
//						NoteEditorView.this.undoButton.setEnabled(false);
//					}
//				}
//			});
//			
//			// Add redo button
//			this.redoButton = bar.addButton("undo", "images/redo.png", -1, "redo", new ActionListener()
//			{
//				public void actionPerformed(ActionEvent e)
//				{
//					if (!redoContentEdit())
//					{
//						NoteEditorView.this.redoButton.setEnabled(false);
//					}
//				}
//			});

//		}

		// ----------------------------------------------------------------------------------
		private void setSaveState(boolean saveEnabled)
		{
//			this.saveButton.setEnabled(saveEnabled);
			this.saveMenuItem.setEnabled(saveEnabled);
		}

		
		// ----------------------------------------------------------------------------------
		public void setIsDirty(boolean isDirty)
		{
			App.logInfo("set iDirty");
			
			this.isDirty = isDirty;
			NoteEditor.this.note.setIsDirty(isDirty);
			setSaveState(isDirty);
		}

		// ----------------------------------------------------------------------------------
		public boolean getIsDirty()
		{
			App.logInfo("get isDirty: " + this.isDirty);
			
			return isDirty;
		}
		
		// ----------------------------------------------------------------------------------
		protected void updateAndSaveNote()
		{
			if (NoteEditor.this.note == null)
			{
				NoteEditor.this.note = new Note(App.controller.getCurrentNotebook());
			}
			
			NoteEditor.this.note.setName(NoteEditorView.this.titleField.getText());
			NoteEditor.this.note.setText(NoteEditorView.this.contentField.getText());
			NoteEditor.this.note.save();
		}
		
		// ----------------------------------------------------------------------------------
		private void verifyFocus()
		{
			if (NoteEditorView.this.isShowing())
			{
				if (titleField.getText().length() == 0)
				{
					if (titleField.hasFocus() == false)
					{
						titleField.requestFocus();
						setGotoMenuActive(false);
					}
				}
				else
				{
					if (contentField.hasFocus() == false)
					{
						contentField.requestFocus();
						setGotoMenuActive(true);
					}
				}
			}
			
			setMenu();
		}

		// ----------------------------------------------------------------------------------
		protected void saveNoteAsNew()
		{
			NoteEditor.this.note.setName(NoteEditorView.this.titleField.getText());
			NoteEditor.this.note.setText(NoteEditorView.this.contentField.getText());
			
			try
			{
				App.controller.modalDialogOpen = true;

				// Get new name for the note
				KOptionPane.showInputDialog(NoteEditorView.this,  "What title would you like for the new Note?", NoteEditor.this.note.getName(), new KOptionPane.InputDialogListener()
				{
					public void onClose(String noteTitle)
					{
						if (noteTitle != null && noteTitle.trim().length() != 0)
						{
							noteTitle = noteTitle.trim();
									
							if (noteTitle.length() != 0 && NoteEditor.this.note != null && noteTitle.toLowerCase().compareTo(NoteEditor.this.note.getName().toLowerCase()) != 0)
							{
								NoteEditor.this.note = new Note(App.controller.getCurrentNotebook());
								NoteEditor.this.note.setName(noteTitle);
								NoteEditor.this.note.setText(NoteEditorView.this.contentField.getText());
								NoteEditor.this.note.save();

								closeView();
							}
						}
						else
						{
							App.controller.modalDialogOpen = false;
							contentField.requestFocus();
						}
					}
				});
			}
			catch (Exception ex)
			{
				App.logError("Copy Note error: " + ex.getMessage());
				App.controller.modalDialogOpen = false;
				contentField.requestFocus();
			}
		}
		
		// ----------------------------------------------------------------------------------
		private void checkDirty(char addedCharacter)
		{
			switch (addedCharacter)
			{
				case KeyEvent.VK_BACK_SPACE:
				case KeyEvent.VK_DELETE:
				case KeyEvent.VK_SPACE:
					setIsDirty(true);
					break;

				default:
					if (addedCharacter >= 32 || this.contentField.getIsDirty() == true)
					{
						setIsDirty(true);
					}
					break;
			}
		}
		
		// ----------------------------------------------------------------------------------
		private void closeView()
		{
			if (App.controller.modalDialogOpen)
			{
				App.controller.modalDialogOpen = false;
			}
			
			NoteEditor.this.note = null;
			App.controller.getEventHandler().actionPerformed(new ActionEvent(NoteEditorView.this, -1, "noteeditor"));
		}

		// ----------------------------------------------------------------------------------
		class FooterPanel extends KPanel
		{
			private static final long	serialVersionUID	= -4911638306174881473L;
			private NumberLegend		legend;

			public FooterPanel ()
			{
				setLayout(new BorderLayout());
				
				try
				{
					legend = new NumberLegend(App.context, NumberLegend.HORIZONTAL);

					if (App.isBigScreen() == true || (legend.doesNumberExist(App.context) == false))
					{
						add(legend, BorderLayout.NORTH);
						legend.validate();
						legend.doLayout();
						setNumberLegendVisibility(App.settings.getNumberLegendToggle());
						setFocusable(false);
					}
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}

			// ----------------------------------------------------------------------------------
			public Insets getInsets()
			{
				return new Insets(10, 0, 0, 0);
			}

			// ----------------------------------------------------------------------------------
			public void setNumberLegendVisibility(String visibility)
			{
				if (legend != null)
				{
					if (visibility.compareTo("auto") == 0)
					{
						this.legend.setDebugMode(false);
						this.legend.setVisible(true);
					}
					else if (visibility.compareTo("on") == 0)
					{
						this.legend.setDebugMode(true);
						this.legend.setVisible(true);
					}
					else
					{
						this.legend.setDebugMode(false);
						this.legend.setVisible(false);
					}
		
					legend.validate();
					legend.doLayout();
					repaint();
				}
			}
		}

		// ----------------------------------------------------------------------------------
		private final class KeyHandler implements KeyListener
		{
			public void keyReleased(KeyEvent e)
			{
				try
				{
					if (e.isConsumed() == false && e.getSource().getClass() == titleField.getClass())
					{
						validateNote();
					}
				}
				catch (Exception ex)
				{
					ex.printStackTrace();
				}
			}

			public void keyTyped(KeyEvent e)
			{
				try
				{
					if (e.isConsumed() == false)
					{
						if (!e.isAltDown())
						{
							checkDirty(e.getKeyChar());
							validateNote();
						}
					}
				}
				catch (Exception ex)
				{
					ex.printStackTrace();
				}
			}

			public void keyPressed(KeyEvent e)
			{
				try
				{
					if (e.isConsumed() == false)
					{
						if (e.getKeyCode() == KindleKeyCodes.VK_SYMBOL)
						{
							e.consume();
							symbolDialog.showDialog((KComponent) e.getSource());
						}
						else if (e.isAltDown())
						{
							switch (e.getKeyChar())
							{
								case 'x':
									if (isDirty)
									{
										NoteEditorView.this.updateAndSaveNote();
									}
									NoteEditorView.this.closeView();
									break;

								case 's':
									if (isDirty)
									{
										NoteEditorView.this.updateAndSaveNote();
									}
									break;

								case 'm':
									// Show/hide memory info
									if (memoryInfoLabel.isVisible())
									{
										// turn off the label
										memoryInfoLabel.setVisible(false);
									}
									else
									{
										// populate the label with memory info and
										// display
										memoryInfoLabel.setVisible(true);
										Runtime runtime = Runtime.getRuntime();
										memoryInfoLabel.setText("[free: " + (runtime.freeMemory() / 1024) + "] [total: " + (runtime.totalMemory() / 1024) + "]");
										repaint();
									}
									break;
							}
							e.consume();
						}
					}
				}
				catch (Exception ex)
				{
					ex.printStackTrace();
				}
			}
		}
	}
}

/*
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * Copyright (c) 2012, David Alexis. All rights reserved.
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */

package com.izzylabs.kindle;

import java.util.Date;

// ----------------------------------------------------------------------------------
final class NoteIndexEntry
{
	public String	id;
	public String	name;
	public int		top;
	public int		caret;
	public String	fileName;
	public boolean	isDirty;
	public Date		created;
	public Date		modified;
}
/*
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * Copyright (c) 2012, David Alexis. All rights reserved.
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */

package com.izzylabs.kindle;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Insets;

import com.amazon.kindle.kindlet.ui.*;
import com.amazon.kindle.kindlet.ui.KindletUIResources.KColorName;

//----------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------
final class NotebookFooterPanel extends KPanel
{
	private static final long	serialVersionUID	= -4911638306174881473L;
	private KLabel				notebookNameLabel;
	private KLabel				countLabel;

	// ------------------------------------------------------------------------------
	public NotebookFooterPanel()
	{
		try
		{
			this.setLayout(new KBoxLayout(this, KBoxLayout.X_AXIS));
			this.setFocusable(false);
			final Font footerFont = App.settings.getFont(Settings.BasicFont);
			this.notebookNameLabel = new KLabel();
			this.notebookNameLabel.setFont(footerFont);
			this.countLabel = new KLabel();
			this.countLabel.setFont(footerFont);
			this.setBackground(KindletUIResources.getInstance().getColor(KColorName.GRAY_02));
			this.add(this.notebookNameLabel);
			this.add(KBox.createHorizontalGlue());
			this.add(this.countLabel);
		}
		catch (Exception ex)
		{
			App.logError(ex.getMessage());
		}
	}

	// -----------------------------------------------------------------------------
	public void updateStatus(int pageNumber, int pageCount, int itemCount)
	{
		final String pageStatus = "page " + pageNumber + " of " + pageCount;
		notebookNameLabel.setText(pageStatus);

		if (itemCount == 1)
			countLabel.setText("1 note");
		else
			countLabel.setText(itemCount + " notes");

		validate();
		repaint();
	}

	// ----------------------------------------------------------------------------------
	public Insets getInsets()
	{
		return new Insets(10, 10, 5, 10);
	}

	// ----------------------------------------------------------------------------------
	public void paint(final Graphics g)
	{
		super.paint(g);

		// Draw a border around the panel
		int width = getWidth();
		g.setColor(KindletUIResources.getInstance().getColor(KColorName.GRAY_05));
		g.drawLine(0, 0, width, 0);
	}

}
/* 
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * Copyright (c) 2011, David Alexis. All rights reserved.
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */

package com.izzylabs.kindle;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import com.amazon.kindle.kindlet.ui.KBoxLayout;
import com.amazon.kindle.kindlet.ui.KMenu;
import com.amazon.kindle.kindlet.ui.KMenuItem;
import com.amazon.kindle.kindlet.ui.KOptionPane;
import com.amazon.kindle.kindlet.ui.KPagedContainer;
import com.amazon.kindle.kindlet.ui.KPages;
import com.amazon.kindle.kindlet.ui.KPanel;
import com.amazon.kindle.kindlet.ui.KTextOptionEvent;
import com.amazon.kindle.kindlet.ui.KTextOptionListMenu;
import com.amazon.kindle.kindlet.ui.KTextOptionMenuItem;
import com.amazon.kindle.kindlet.ui.KTextOptionPane;
import com.amazon.kindle.kindlet.ui.pages.PageProviders;

public class NotebookView extends KPanel
{
	private static final long	serialVersionUID		= 3142167970907737627L;

	private KPages				pages;
	public  Notebook			myNotebook = null;
	private NotebookFooterPanel	footerPanel;
	private KMenu				menu;
	private DateFormat			displayDateFormatter	= new SimpleDateFormat("MMM d, yyyy");
	private boolean				trackPageChanges		= false;
	private NoteListItem		currentItem;
	private int					pageCount				= 0;
	private int					currentPage				= 0;
	private int					currentPageStart		= 0;
	private String				noteId;
	private boolean				restorePositionOnLoad	= false;
	private int					itemCount;
	private KMenuItem			deleteMenuItem;
	private KMenuItem			exportMenuItem;
	private KTextOptionPane		textOptionPane;
	private SearchDefinition	currentSearchTerm;
	private ArrayList			currentFilteredNoteList;
	
	// ----------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------
	public NotebookView(Notebook notebook)
	{
		super(new BorderLayout(0, 0));

		this.myNotebook = notebook;
		trackPageChanges = false;
		setFocusable(false);
		
		add(new TitlePanel(notebook.name), BorderLayout.NORTH);

		// the components are added to the container but not positioned until
		// the container is resized
		this.pages = new KPages(PageProviders.createKBoxLayoutProvider(KBoxLayout.Y_AXIS))
		{
			private static final long	serialVersionUID	= 1L;

			public Insets getInsets()
			{
				return new Insets(10, 10, 10, 10);
			}
		};
		this.pages.setFirstPageAlignment(BorderLayout.NORTH);
//		this.pages.setFocusable(false);
		this.pages.setPageKeyPolicy(KPagedContainer.PAGE_KEYS_LOCAL);

		add(this.pages, BorderLayout.CENTER);

		this.footerPanel = new NotebookFooterPanel();

		add(footerPanel, BorderLayout.SOUTH);
		validate();

		addComponentListener(new ComponentListener()
		{

			public void componentShown(ComponentEvent arg0)
			{
				displayNotes(true);
				setMenu();
				pages.setPageKeyPolicy(KPagedContainer.PAGE_KEYS_LOCAL);
				validate();
				calculatePages();
				
				if (itemCount > 0)
				{
					selectCurrentNote();
				}
				else
				{
					pages.requestFocus();
				}
				
//				try
//				{
//					KRepaintManager.paintImmediately(NotebookView.this, true);
//				}
//				catch (InterruptedException e)
//				{
//				}
//				catch (InvocationTargetException e)
//				{
//				}
			}

			public void componentResized(ComponentEvent arg0)
			{
				displayNotes(true);
//				pages.setPageKeyPolicy(KPagedContainer.PAGE_KEYS_LOCAL);
				validate();
				calculatePages();
				footerPanel.updateStatus(currentPage, pageCount, itemCount);
				
				if (itemCount > 0)
				{
					selectCurrentNote();
				}
				else
				{
					pages.requestFocus();
				}
				
				repaint();
			}

			public void componentMoved(ComponentEvent arg0)
			{
			}

			public void componentHidden(ComponentEvent arg0)
			{
				pages.setPageKeyPolicy(KPagedContainer.PAGE_KEYS_DISABLED);
			}
		});

		pages.addPropertyChangeListener(new PropertyChangeListener()
		{

			public void propertyChange(PropertyChangeEvent evt)
			{
				if (evt.getPropertyName().compareTo("pageEndLocation") == 0 && trackPageChanges == true)
				{
					EventQueue.invokeLater(new Runnable()
					{
						public void run()
						{
							calculatePages();
							footerPanel.updateStatus(currentPage, pageCount, itemCount);

							int firstLocation = pages.getPageStartLocation();
							currentPageStart = firstLocation;

							if (!(currentPageStart != 0 && currentPageStart < pages.getPageModel().getLastLocation()))
							{
								currentPageStart = pages.getPageStartLocation();
							}

							if (NotebookView.this.itemCount <= NotebookView.this.currentPageStart)
								NotebookView.this.currentPageStart = NotebookView.this.itemCount - 1;
							
							if (currentPageStart >= 0)
							{
								NoteListItem topNote = ((NoteListItem) (pages.getPageModel().getElementAt(currentPageStart)));
								currentItem = topNote;
								pages.setLocation(currentPageStart);
								topNote.select();
							}
						}
					});
				}
			}
		});
		
		textOptionPane = createSortTypeMenu();
		createMenu();
		setMenu();
	}

	// ----------------------------------------------------------------------------------
	private KTextOptionPane createSortTypeMenu()
	{
		// Create the list menu with the label "Title Labels"
		final KTextOptionListMenu sortTypeMenu = new KTextOptionListMenu("Sort Notes By:");
		sortTypeMenu.add(new KTextOptionMenuItem("Last Modified"));
		sortTypeMenu.add(new KTextOptionMenuItem("Note Title"));
		
		final KTextOptionListMenu sortDirectionMenu = new KTextOptionListMenu("Sort Direction:");
		sortDirectionMenu.add(new KTextOptionMenuItem("Ascending"));
		sortDirectionMenu.add(new KTextOptionMenuItem("Descending"));
		
		String sortType = App.settings.getSortField();
		sortTypeMenu.setSelectedIndex(sortType.equals("modified") ? 0 : 1);

		String sortDirection = App.settings.getSortDirection();
		sortDirectionMenu.setSelectedIndex(sortDirection.equals("asc") ? 0 : 1);
		
		sortTypeMenu.addItemListener(new ItemListener()
		{
			public void itemStateChanged(final ItemEvent e)
			{
				final KTextOptionEvent textOptionEvent = (KTextOptionEvent) e;
				
				if (!textOptionEvent.isVisit() && e.getStateChange() == ItemEvent.SELECTED)
				{
					if (sortTypeMenu.getSelectedIndex() == 0)
						App.settings.setSortField("modified");
					else
						App.settings.setSortField("name");
					
					displayNotes(true);					
				}
			}
		});

		sortDirectionMenu.addItemListener(new ItemListener()
		{
			public void itemStateChanged(final ItemEvent e)
			{
				final KTextOptionEvent textOptionEvent = (KTextOptionEvent) e;

				if (!textOptionEvent.isVisit() && e.getStateChange() == ItemEvent.SELECTED)
				{
					if (sortDirectionMenu.getSelectedIndex() == 0)
						App.settings.setSortDirection("asc");
					else
						App.settings.setSortDirection("desc");
					
					displayNotes(true);
				}
			}
		});
		
		final KTextOptionPane optionPane = new KTextOptionPane();
		optionPane.addListMenu(sortTypeMenu);
		optionPane.addListMenu(sortDirectionMenu);
		
		return optionPane;
	}
	
	// ----------------------------------------------------------------------------------
	private void selectCurrentNote()
	{		
		App.logInfo("itemCount: " + itemCount + "  CurrentITem: " + currentItem);

		//if (currentItem == null && itemCount > 0)
		if (itemCount > 0)
		{
			currentItem = ((NoteListItem) (pages.getPageModel().getElementAt(currentPageStart)));
		}

		pages.setLocation(currentPageStart);
		
		if (currentItem != null)
			currentItem.select();
		
		App.logInfo("---itemCount: " + itemCount + "  CurrentITem: " + currentItem);

	}

	// ----------------------------------------------------------------------------------
	public Insets getInsets()
	{
		return new Insets(0, 0, 0, 0);
	}

	// ----------------------------------------------------------------------------------
	public void loadNotebook(String id, boolean keepCurrentPosition)
	{
		if (this.myNotebook == null)
		{
			this.myNotebook = new Notebook(id);
			restorePositionOnLoad = false;
		}
		else
		{
			restorePositionOnLoad = keepCurrentPosition;
		}

		App.settings.pageSize = 0;
		displayNotes(restorePositionOnLoad);		
		App.settings.pageSize = 0;
	}

//	// ----------------------------------------------------------------------------------
//	public void displayNotes(boolean restorePosition)
//	{
//		App.logInfo("displayNotes");
//		
//		this.pages.removeAllItems();
//		this.currentItem = null;
//		trackPageChanges = false;
//		this.currentItem = null;
//
//		try
//		{
//			ArrayList notes = this.myNotebook.getNotes();
//
//			for (int index = 0; index < notes.size(); index++)
//			{
//				NoteIndexEntry thisNote = (NoteIndexEntry) notes.get(index);
//
//				NoteListItem note = new NoteListItem(thisNote.id, 
//													 thisNote.name, "created on " + displayDateFormatter.format(thisNote.created), 
//													 "changed on " + displayDateFormatter.format(thisNote.modified));
//
//				note.addFocusListener(new NoteItemFocusHandler());
//
//				pages.addItem(note);
//			}
//
//			itemCount = pages.getPageModel().getLastLocation();
//			
//			if (itemCount == Integer.MAX_VALUE || itemCount < 0)
//				itemCount = 0;
//			else
//				itemCount += 1;
//			
//			App.logInfo("Item Count: " + itemCount);
//
//			// Display the footer with page data
//			this.validate();
//			trackPageChanges = true;
//			calculatePages();
//			this.footerPanel.updateStatus(currentPage, pageCount, itemCount);
//
//			if (restorePosition == false || this.currentPageStart < 0)
//			{
//				this.currentPageStart = 0;
//			}
//
//			if (this.currentPageStart != 0 && this.currentPageStart < this.pages.getPageModel().getLastLocation())
//			{
//				this.pages.setLocation(this.currentPageStart);
//			}
//			else
//				this.pages.setLocation(this.pages.getPageStartLocation());
//
//			App.logInfo("CurrentPageStart: " + currentPageStart);
//			
//			repaint();
//		}
//		catch (Error e)
//		{
//			e.printStackTrace();
//		}
//		finally
//		{
//			trackPageChanges = true;
//		}
//		
//		if (currentItem == null)
//		{
//			deleteMenuItem.setEnabled(false);
//		}
//		else
//		{
//			deleteMenuItem.setEnabled(true);
//		}
//	}

	// ------------------------------------------------------------------------------
	private void displayNotes(boolean restorePosition)
	{
		try
		{
//			pages.setPagingPolicy(KPagedContainer.PAGING_POLICY_LOCAL);

			if (this.currentFilteredNoteList != null)
			{
				this.currentFilteredNoteList = this.myNotebook.getFilteredNotes(this.currentSearchTerm);
				renderNotes(restorePosition, this.currentFilteredNoteList);
			}
			else
			{
				ArrayList notes = this.myNotebook.getNotes();
				renderNotes(restorePosition, notes);
			}
		}
		catch (Error e)
		{
			App.logError(e.getMessage());
		}
		finally
		{
			trackPageChanges = true;
		}
	}
	
	// ----------------------------------------------------------------------------------
	private void calculatePages()
	{
		try
		{
			// Gather known information:  item count, location range of current page		
			int lastLocation = pages.getPageModel().getLastLocation();
			itemCount = lastLocation + 1;
			int pageEndLocation = pages.getPageEndLocation();
			int pageStartLocation = pages.getPageStartLocation();
			
			if (pageEndLocation == Integer.MAX_VALUE)
				pageEndLocation = lastLocation;
			else
				pageEndLocation -= 1;
			
			if (pageStartLocation == Integer.MIN_VALUE)
				pageStartLocation = 0;
	
			// Calculate page size from given information
			int pageSize = pageEndLocation - pageStartLocation + 1;
			
			// Compare with saved page location and use whichever is larger
			if (pageSize > App.settings.pageSize)
			{
				App.settings.pageSize = pageSize;
			}
			else
			{
				pageSize = App.settings.pageSize;
			}
			
			// Based on this data, figure out how many pages there are and what page we're on
			if (App.settings.pageSize == 0 || itemCount == 0)
				pageCount = 1;
			else
				pageCount = ((int)(itemCount / App.settings.pageSize)) + ((itemCount % pageSize) > 0 ? 1 : 0);
			
			if (lastLocation <= 0)
				currentPage = 1;
			else
				currentPage = (int)((pageStartLocation + 1) / pageSize) + 1;
		}
		catch (Exception ex)
		{
			//ex.printStackTrace();
		}
	}

	// ----------------------------------------------------------------------------------
	private void createMenu()
	{
		try
		{
			this.menu = new KMenu();
	
			menu.add(createMenuItem("New Note", "200", new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					App.controller.getEventHandler().actionPerformed(new ActionEvent(NotebookView.this, App.EDITORVIEW, null));
				}
			}));
	
			this.deleteMenuItem = new KMenuItem("Delete selected note");
			this.menu.add(this.deleteMenuItem);
			this.deleteMenuItem.setEnabled(true);
			this.deleteMenuItem.addActionListener(new NoteDeleteHandler());
	
			menu.addSeparator();
			
			menu.add(createMenuItem("Search ...", "", new ActionListener()
			 {
				 public void actionPerformed(ActionEvent e)
				 {
					 SearchDialog.show (
								App.context.getRootContainer(), 
								new ActionListener()
								{
									public void actionPerformed(ActionEvent e)
									{
										App.controller.modalDialogOpen = false;
										
										EventQueue.invokeLater(new Runnable() 
										{
								            public void run() 
								            { 
								            	selectCurrentNote();
								            }
								         });
										NotebookView.this.currentSearchTerm = new SearchDefinition(e.getActionCommand(), e.getID());
										NotebookView.this.doSearch(NotebookView.this.currentSearchTerm);
									}
								}, 
								new ActionListener()
								{
									public void actionPerformed(ActionEvent e)
									{
										App.controller.modalDialogOpen = false;

										EventQueue.invokeLater(new Runnable() 
										{
								            public void run() 
								            { 
								            	selectCurrentNote();
								            }
								         });
//										NotebookView.this.searchButton.setEnabled(true);
//										NotebookView.this.setSearchControlsMode(true);
									}
								});
				 }
			 }));
			
			menu.add(createMenuItem("Switch to Another Notebook", "", new ActionListener()
					 {
						 public void actionPerformed(ActionEvent e)
						 {
							 App.controller.getEventHandler().actionPerformed(new ActionEvent(NotebookView.this, App.NOTEBOOKLISTVIEW, null));
						 }
					 }));
			
			// Create SaveAs menu item
			this.exportMenuItem = new KMenuItem("Export Notebook to HTML");
			this.menu.add(this.exportMenuItem);
			this.exportMenuItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					NoteExportHandler exportHandler = new NoteExportHandler();
					exportHandler.export();
				}
			});
			
			menu.add(createMenuItem("Help", "240", new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					App.controller.getEventHandler().actionPerformed(new ActionEvent(NotebookView.this, App.HELPVIEW, null));
				}
			}));
		}
		catch (Exception ex)
		{}
	}
	
	// ----------------------------------------------------------------------------------
	private KMenuItem createMenuItem(String text, String command, ActionListener handler)
	{
		KMenuItem menuItem = new KMenuItem(text);

		// Set the action command for the menu item
		menuItem.setActionCommand(command);
		menuItem.addActionListener(handler);

		return menuItem;
	}

	// ----------------------------------------------------------------------------------
	protected void setMenu()
	{
		try
		{
			if (Thread.interrupted() == false && App.context != null && this.menu != null)
			{
				App.context.setMenu(this.menu);
				App.context.setTextOptionPane(this.textOptionPane);
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	// ----------------------------------------------------------------------------------
	private class NoteExportHandler // implements InputDialogListener
	{
		public void export()
		{
			String fileName = "My TakeNote Notebook.htm";
		
	 		try
			{
	 			// Verify the export folder exists
	 			String exportFolderPath = App.context.getHomeDirectory() + "/exported";
	 			File exportFolder = new File(exportFolderPath);

	 			if (!exportFolder.exists())
	 			{
	 				exportFolder.mkdir();
	 			}
	 			
	 			// Validate the file name
	 			String validatedName = fileName.trim(); //.replaceAll("[\\/:*?\"<>|]","-");
	 			String exportFilePath = exportFolderPath + "/" + validatedName + (validatedName.endsWith(".htm") ? "" : ".htm");
	 			
		 		File exportFile = new File(exportFilePath);
		 		
		 		if (exportFile.exists())
		 			exportFile.delete();

	 			String message = null;
	 			
	 			String exportFileName = NotebookView.this.myNotebook.exportNotes();
	 			if (exportFileName != null)
	 			{
	 				message = "The notes have been exported to " +  exportFileName + ". " +
	 						  "Please refer to the TakeNote Help content for instructions on " +
	 						  "how to retrieve this file.";
	 			}
	 			else
	 			{
	 				message = "Failed to export your notebook.";
	 			}
	 			
		 		KOptionPane.showMessageDialog(NotebookView.this, message, "Notebook Export", 
                    new KOptionPane.MessageDialogListener() 
	 				{
                        public void onClose() 
                        {
                        	NotebookView.this.selectCurrentNote();
                        }
                    });
			}
			catch (IllegalStateException e)
			{
//				ApplicationContext.logger.error("NoteExport error: " + e.getMessage());
	 		}
			catch (IllegalArgumentException e)
			{
//				ApplicationContext.logger.error("NoteExport error: " + e.getMessage());
			}
			
			pages.requestFocus();
		}
	 }

	// ----------------------------------------------------------------------------------
	private final class NoteDeleteHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			try
			{
				if (currentItem != null)
				{
					currentPageStart = pages.getPageStartLocation();
		
					NotebookView.this.noteId = currentItem.noteId;
		
					KOptionPane.showConfirmDialog(NotebookView.this,
							"This note will be permanently deleted.  Would you like to continue?", "Delete This Note",
							new KOptionPane.ConfirmDialogListener()
							{
								public void onClose(final int option)
								{
									if (option == KOptionPane.OK_OPTION)
									{
										currentItem = null;
										myNotebook.deleteNote(noteId);
										loadNotebook(null, true);
									}
									
									selectCurrentNote();
								}
							});
				}
			}
			finally
			{
				selectCurrentNote();
			}
		}
	}

	// ----------------------------------------------------------------------------------
	private final class NoteItemFocusHandler implements FocusListener
	{
		public void focusGained(FocusEvent e)
		{
			NotebookView.this.currentItem = (NoteListItem) e.getSource();
			deleteMenuItem.setEnabled(true);
			currentPageStart = pages.getPageStartLocation();
		}

		public void focusLost(FocusEvent e)
		{
			if (NotebookView.this.currentItem == null)
			{
				deleteMenuItem.setEnabled(false);
				currentPageStart = 0;
			}
		}
	}
	
	// ----------------------------------------------------------------------------------
		private void doSearch(SearchDefinition searchTerm)
		{

			if (searchTerm != null && searchTerm.term.length() != 0)
			{
				currentFilteredNoteList = this.myNotebook.getFilteredNotes(searchTerm);
				renderNotes(false, currentFilteredNoteList);
			}
		}
		
		// ------------------------------------------------------------------------------
		private void renderNotes(boolean restorePosition, ArrayList notes)
		{
			int savedCurrentPageStart = this.currentPageStart;
			this.pages.removeAllItems();
			this.pages.invalidate();

			trackPageChanges = false;

			for (int index = 0; index < notes.size(); index++)
			{
				NoteIndexEntry thisNote = (NoteIndexEntry) notes.get(index);
				NoteListItem note = new NoteListItem(thisNote.id, thisNote.name, "created on " + displayDateFormatter.format(thisNote.created), "changed on " + displayDateFormatter.format(thisNote.modified));

				if (restorePosition == true && this.noteId != null && note.noteId.equals(this.noteId))
				{
					this.pages.setLocation(savedCurrentPageStart);
				}

				this.pages.addItem(note);
			}

			trackPageChanges = true;
			itemCount = this.pages.getPageModel().getLastLocation();

			if (itemCount == Integer.MAX_VALUE)
				itemCount = 0;
			else
				itemCount += 1;

			// Display the footer with page data
			this.validate();
			calculatePages();
			footerPanel.updateStatus(currentPage, pageCount, itemCount);

			if (restorePosition == false)
			{
				// this.currentItem = null;
				this.currentPageStart = 0;
			}
			else
			{
				this.currentPageStart = savedCurrentPageStart;
			}

			if (this.currentPageStart != 0 && this.currentPageStart < this.pages.getPageModel().getLastLocation())
			{
				this.pages.setLocation(this.currentPageStart);
			}
			else
				this.pages.setLocation(this.pages.getPageStartLocation());

			this.pages.repaint();
		}

}
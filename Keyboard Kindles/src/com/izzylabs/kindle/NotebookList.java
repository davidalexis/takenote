/*
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * Copyright (c) 2012, David Alexis. All rights reserved.
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */

package com.izzylabs.kindle;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FilenameFilter;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.amazon.kindle.kindlet.event.KindleKeyCodes;
import com.amazon.kindle.kindlet.ui.*;
import com.amazon.kindle.kindlet.ui.KindletUIResources.KColorName;
import com.amazon.kindle.kindlet.ui.pages.PageProviders;

public final class NotebookList implements IViewable
{
	private Container view;
	
	// ----------------------------------------------------------------------------------
	public Container getView()
	{
		return this.view;
	}
	
	// ----------------------------------------------------------------------------------
	public void setView(Container view)
	{
		this.view = view;
	}
	
	// ----------------------------------------------------------------------------------
	public String getViewName()
	{
		return "notebooklist";
	}
	
	// ----------------------------------------------------------------------------------
	public int getViewId()
	{
		return App.NOTEBOOKLISTVIEW;
	}

	// ----------------------------------------------------------------------------------
	public NotebookList()
	{
		this.view = new NotebookListView();
	}
	
	// ----------------------------------------------------------------------------------
	public class NotebookListView extends KPanel
	{
		private static final long	serialVersionUID	= 1L;
		
		private 	KPages			pages;
		private 	FooterPanel		footerPanel;
		private 	KMenu			menu;
		protected 	int				currentPageStart;
		protected 	int				currentPage;
		protected 	int				pageCount;
		protected 	int				itemCount;
		protected 	boolean			trackPageChanges;
		private 	int				pageSize;

		protected NotebookListItem	currentItem;
		protected KMenuItem		deleteMenuItem;
		
		// ------------------------------------------------------------------------------
		public NotebookListView()
		{
			this.setLayout(new BorderLayout(0, 0));
			this.setFocusable(false);			
			final TitlePanel bar = new TitlePanel("Notebooks");
			this.add(bar, BorderLayout.NORTH);
			
			this.pages = new KPages(PageProviders.createKBoxLayoutProvider(KBoxLayout.Y_AXIS))
			{
				private static final long	serialVersionUID	= 1L;

				public Insets getInsets()
				{
					return new Insets(10, 10, 10, 10);
				}
			};
			this.pages.setFirstPageAlignment(BorderLayout.NORTH);
//			this.pages.setFocusable(false);
			this.pages.setPageKeyPolicy(KPagedContainer.PAGE_KEYS_LOCAL);
			this.add(this.pages, BorderLayout.CENTER);
			this.pageSize = 0;
			this.footerPanel = new FooterPanel();
			this.add(footerPanel, BorderLayout.SOUTH);

			this.addComponentListener();
			this.addPropertyListener();
			
			this.createMenu();

		}
		
		// ------------------------------------------------------------------------------
		private void addComponentListener()
		{
			addComponentListener(new ComponentListener()
			{
				public void componentShown(ComponentEvent arg0)
				{
					displayItems();
					setMenu();
					pages.setPageKeyPolicy(KPagedContainer.PAGE_KEYS_LOCAL);
//					validate();
					calculatePages();
					
					if (itemCount > 0)
					{
						selectCurrentItem();
					}
					else
					{
						pages.requestFocus();
					}
					
					try
					{
						KRepaintManager.paintImmediately(NotebookListView.this, true);
					}
					catch (InterruptedException e)
					{
					}
					catch (InvocationTargetException e)
					{
					}
				}

				public void componentResized(ComponentEvent arg0)
				{
//					displayItems();
					validate();
//					calculatePages();
//					
//					if (itemCount > 0)
//					{
//						selectCurrentItem();
//					}
//					else
//					{
//						pages.requestFocus();
//					}
					
					repaint();
				}

				public void componentMoved(ComponentEvent arg0)
				{}

				public void componentHidden(ComponentEvent arg0)
				{
					pages.setPageKeyPolicy(KPagedContainer.PAGE_KEYS_LOCAL);
					setMenu();
				}
			});
		}
		
		// ------------------------------------------------------------------------------
		private void addPropertyListener()
		{
			pages.addPropertyChangeListener(new PropertyChangeListener()
			{
				public void propertyChange(PropertyChangeEvent evt)
				{
					if (evt.getPropertyName().compareTo("pageEndLocation") == 0 && trackPageChanges == true)
					{
						try
						{
							calculatePages();
							footerPanel.updateStatus(currentPage, pageCount, itemCount);

							int firstLocation = pages.getPageStartLocation();
							currentPageStart = firstLocation;

							if (!(currentPageStart != 0 && currentPageStart < pages.getPageModel().getLastLocation()))
							{
								currentPageStart = pages.getPageStartLocation();
							}

							if (itemCount <= currentPageStart)
								currentPageStart = itemCount - 1;
							
							if (currentPageStart >= 0)
							{
								trackPageChanges = false;
								NotebookListItem topNotebook = ((NotebookListItem) (pages.getPageModel().getElementAt(currentPageStart)));
								currentItem = topNotebook;
								pages.setLocation(currentPageStart);
								topNotebook.select();
								trackPageChanges = true;
							}
						}
						catch (Exception ex)
						{
							App.logError("Property page change error: " + ex.getMessage());
						}
					}
				}
			});
		}
		
		// ----------------------------------------------------------------------------------
		private void calculatePages()
		{
			try
			{
				if (pages.getHeight() <= 0)
					return;

				// Gather known information: item count, location range of
				// current page
				int lastLocation = pages.getPageModel().getLastLocation();
				itemCount = lastLocation + 1;
				int pageEndLocation = pages.getPageEndLocation();
				int pageStartLocation = pages.getPageStartLocation();

				if (pageEndLocation == Integer.MAX_VALUE)
					pageEndLocation = lastLocation;
				else
					pageEndLocation -= 1;

				if (pageStartLocation == Integer.MIN_VALUE)
					pageStartLocation = 0;

				// Calculate page size from given information
				int pageSize = pageEndLocation - pageStartLocation + 1;

				// Compare with saved page location and use whichever is larger
				if (pageSize > this.pageSize)
				{
					this.pageSize = pageSize;
				}
				else
				{
					pageSize = this.pageSize;
				}

				// Based on this data, figure out how many pages there are and
				// what page we're on
				if (this.pageSize == 0 || itemCount == 0)
					pageCount = 1;
				else
					pageCount = ((int) (itemCount / this.pageSize)) + ((itemCount % pageSize) > 0 ? 1 : 0);

				if (lastLocation <= 0)
					currentPage = 1;
				else
					currentPage = (int) ((pageStartLocation + 1) / pageSize) + 1;
			}
			catch (Exception ex)
			{
				App.logError("Error! " + ex.getMessage() + ex.getStackTrace());
			}
		}
		
		// ------------------------------------------------------------------------------
		public void displayItems()
		{

			try
			{
				pages.setPageKeyPolicy(KPagedContainer.PAGE_KEYS_LOCAL);

				ArrayList notebooks = getNotebookList();
				this.pages.removeAllItems();
				trackPageChanges = false;

				for (int index = 0; index < notebooks.size(); index++)
				{
					NotebookMetadata thisNotebook = (NotebookMetadata) notebooks.get(index);				
					NotebookListItem note = new NotebookListItem(thisNotebook.id, thisNotebook.name, thisNotebook.noteCount);
					
					note.addFocusListener(new FocusListener()
					{
						public void focusGained(FocusEvent e)
						{
							NotebookListView.this.currentItem = (NotebookListItem) e.getSource();
							currentPageStart = pages.getPageStartLocation();
						}

						public void focusLost(FocusEvent e)
						{
							if (NotebookListView.this.currentItem == null)
							{
								deleteMenuItem.setEnabled(false);
								currentPageStart = 0;
							}
						}
					});
					pages.addItem(note);
				}

				itemCount = pages.getPageModel().getLastLocation();

				if (itemCount == Integer.MAX_VALUE)
					itemCount = 0;
				else
					itemCount += 1;

				// Display the footer with page data
				this.validate();
				trackPageChanges = true;
				calculatePages();
				footerPanel.updateStatus(currentPage, pageCount, itemCount);
				
				this.currentPageStart = 0;

				if (this.currentPageStart != 0 && this.currentPageStart < this.pages.getPageModel().getLastLocation())
				{
//					if (this.currentPageStart < 0)
//						this.currentPageStart = 0;
					
					this.pages.setLocation(this.currentPageStart);
				}
					this.pages.setLocation(this.pages.getPageStartLocation());
				
				repaint();
			}
			catch (Error e)
			{
				App.logError(e.getMessage() + e.getStackTrace());
			}
			finally
			{
				trackPageChanges = true;
			}
		}
		
		// ------------------------------------------------------------------------------
		private ArrayList getNotebookList()
		{
			ArrayList notebooks = new ArrayList();
			String notebooksPath = System.getProperty("kindlet.home") + "/notebooks/";
			
			// Get list of JSON notebook indexes
			File notebooksDirectory = new File(notebooksPath);
			final FilenameFilter fileFilter = new FilenameFilter() 
			{
				public boolean accept(File dir, String name) 
				{
				  	return name.endsWith(".json");
			  	}
			};
			    
			String[] notebookIndexFiles = notebooksDirectory.list(fileFilter);
			
			// Read each index file to get notebook name, id and note count
			for (int fileIndex = 0; fileIndex < notebookIndexFiles.length; fileIndex++)
			{
				String fileName = notebooksPath + "/" + notebookIndexFiles[fileIndex];
				String jsonContent = Notebook.readNotebookIndexFile(fileName);
				
				if (jsonContent != null)
				{
					JSONObject notebookData = Notebook.parseNotebookIndexJSON(jsonContent);
					JSONArray notes = (JSONArray) notebookData.get("notes");
					String name = notebookData.get("name").toString();
					String id = notebookData.get("id").toString();
					int noteCount = notes == null ? 0 : notes.size();
					
					NotebookMetadata notebookEntryData = new NotebookMetadata(name, id, noteCount);
					notebooks.add(notebookEntryData);
				}
			}

			Collections.sort(notebooks, new byName());
			return notebooks;
		}

		// ----------------------------------------------------------------------------------
		private void createMenu()
		{
			try
			{
				this.menu = new KMenu();
				
				this.deleteMenuItem = new KMenuItem("Delete this notebook");
				this.menu.add(this.deleteMenuItem);
				this.deleteMenuItem.setEnabled(true);
				this.deleteMenuItem.addActionListener(new NoteDeleteHandler());
				
				final KMenuItem newNotebookMenuItem = new KMenuItem("Create new notebook");
				newNotebookMenuItem.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						createNotebook();				
					}
				});
				this.menu.add(newNotebookMenuItem);
				
				final KMenuItem renameNotebookMenuItem = new KMenuItem("Rename selected notebook");
				renameNotebookMenuItem.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{		
						renameNotebook();
					}
				});
				this.menu.add(renameNotebookMenuItem);
				
				
				final KMenuItem helpMenuItem = new KMenuItem("TakeNote Help");
				helpMenuItem.setActionCommand("");
				helpMenuItem.addActionListener(new ActionListener()
				{
					 public void actionPerformed(ActionEvent e)
					 {
						 App.controller.getEventHandler().actionPerformed(new ActionEvent(NotebookListView.this, App.HELPVIEW, null));
					 }
				});
					
				menu.add(helpMenuItem);

			}
			catch (Exception ex)
			{
				App.logError(ex.getMessage() + ex.getStackTrace());
			}
		}
		
		// ----------------------------------------------------------------------------------
		protected void setMenu()
		{
			try
			{
				if (Thread.interrupted() == false && App.context != null && this.menu != null)
				{
					App.context.setMenu(this.menu);
				}
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
			}
		}
		
		// ----------------------------------------------------------------------------------
		private void doRename(final String notebookId, final String oldNotebookName, final String newNotebookName)
		{
			try
			{
				if (newNotebookName != null && newNotebookName.equals(oldNotebookName) == false)
				{		
					//TODO:  Validate that the new name does not already exist.
					Notebook currentNotebook = App.controller.getCurrentNotebook();
					
					if (currentNotebook.id.equals(notebookId))
					{
						App.controller.setCurrentNotebook(null);
					}
					
					String notebooksPath = System.getProperty("kindlet.home") + "/notebooks/";
					String indexFilePath = notebooksPath + notebookId + ".json";
					String noteFolder = notebooksPath + notebookId + "/";
					String newNotebookId = FileUtilities.sanitize(newNotebookName).toLowerCase();
					String newIndexFilePath = notebooksPath + newNotebookId + ".json";
					
					Notebook notebook = new Notebook(notebookId);
					notebook.filePath = newIndexFilePath;
					notebook.id = newNotebookId;
					notebook.name = newNotebookName;
					notebook.save();
					notebook = null;
					
					File noteDirectory = new File(noteFolder);
					
					// Rename the notes folder
					if (noteDirectory.exists())
					{
						noteDirectory.renameTo(new File(notebooksPath + newNotebookId));
						noteDirectory = null;
					}
					
					// Rename the notebook index file
					File newNotebookFile = new File(newIndexFilePath);
					File notebookFile = new File(indexFilePath);
					
					if (newNotebookFile.exists())
					{
						// The new index file was already created.  Just delete the old one.
						notebookFile.delete();
					}
					else
					{
						// Rename the old file to the new name
						notebookFile.renameTo(new File(newIndexFilePath));
					}
	
					// Reload
					NotebookListView.this.displayItems();
				}
			}
			finally
			{
				App.controller.modalDialogOpen = false;
			}
		}
		
		// ----------------------------------------------------------------------------------
		private void selectCurrentItem()
		{
			if (currentItem == null)
			{
				currentItem = ((NotebookListItem) (pages.getPageModel().getElementAt(currentPageStart)));
			}

			pages.setLocation(currentPageStart);
			currentItem.select();
		}
		
		// ----------------------------------------------------------------------------------
		protected void createNotebook()
		{
			// Get new name for the note
			KOptionPane.showInputDialog(NotebookListView.this,  "What is the name for this Notebook?\n ", "Notebook", new KOptionPane.InputDialogListener()
			{
				public void onClose(String notebookTitle)
				{
					try
					{
						if (notebookTitle != null && notebookTitle.trim().length() != 0)
						{
							notebookTitle = notebookTitle.trim();
						
							if (doesNotebookExist(notebookTitle))
							{
						 		KOptionPane.showMessageDialog(NotebookListView.this, "A Notebook with that name already exists. Try another name.", "Create Notebook",
				                    new KOptionPane.MessageDialogListener() 
					 				{
				                        public void onClose() 
				                        {
//				                        	App.controller.modalDialogOpen = false;
//				    						selectCurrentItem();
				                        }
				                    });								
							}
							else
							{
								notebookTitle = notebookTitle.trim();
								
								if (notebookTitle.length() != 0)
								{
									currentItem = null;
									String newNotebookId = FileUtilities.sanitize(notebookTitle).toLowerCase();
									Notebook newNotebook = new Notebook(newNotebookId);
									newNotebook.name = notebookTitle;
									newNotebook.save();
									newNotebook = null;
									displayItems();
								}
								
//								App.controller.modalDialogOpen = false;
//	    						selectCurrentItem();
							}					
						}
//						else
//						{
//							// Please enter a valid notebook name
////					 		KOptionPane.showMessageDialog(NotebookListView.this, "Please enter a valid Notebook name.", "Create Notebook", null);
//					 		App.controller.modalDialogOpen = false;
//							selectCurrentItem();
//						}
					}
					finally
					{
						App.controller.modalDialogOpen = false;
						selectCurrentItem();
					}
				}
			});
		}

		// ----------------------------------------------------------------------------------
		protected void renameNotebook()
		{
			// Get new name for the note
			KOptionPane.showInputDialog(NotebookListView.this,  "What is the new name\nfor this Notebook?", "Notebook", new KOptionPane.InputDialogListener()
			{
				public void onClose(String notebookTitle)
				{
					try
					{
						if (notebookTitle != null && notebookTitle.trim().length() != 0)
						{
							notebookTitle = notebookTitle.trim();
						
							if (doesNotebookExist(notebookTitle))
							{
						 		KOptionPane.showMessageDialog(NotebookListView.this, "A Notebook with that name already exists. Try another name.", "Create Notebook",
				                    new KOptionPane.MessageDialogListener() 
					 				{
				                        public void onClose() 
				                        {
				                        }
				                    });								
							}
							else
							{
								notebookTitle = notebookTitle.trim();
								
								if (notebookTitle.length() != 0)
								{
									doRename(currentItem.notebookId, currentItem.notebookName, notebookTitle);
									
									currentItem = null;
									displayItems();
								}
								
//										App.controller.modalDialogOpen = false;
//			    						selectCurrentItem();
							}					
						}
//								else
//								{
//									// Please enter a valid notebook name
////							 		KOptionPane.showMessageDialog(NotebookListView.this, "Please enter a valid Notebook name.", "Create Notebook", null);
//							 		App.controller.modalDialogOpen = false;
//									selectCurrentItem();
//								}
					}
					finally
					{
						App.controller.modalDialogOpen = false;
						selectCurrentItem();
					}
				}
			});
		}
		
		// ----------------------------------------------------------------------------------
		protected boolean doesNotebookExist(String noteTitle)
		{
			Iterator list = getNotebookList().iterator();
			String compareTitle = noteTitle.toUpperCase();
			
			while (list.hasNext())
			{
				NotebookMetadata notebook = (NotebookMetadata) list.next();

				if (notebook.name.toUpperCase().compareTo(compareTitle) == 0)
				{
					return true;
				}
			}
					
			return false;
		}

		// ----------------------------------------------------------------------------------
		class NotebookListItem extends KPanel implements FocusListener, KeyListener
		{
			private static final long	serialVersionUID	= 1L;
			private String 			notebookId;
			protected String 		notebookName;
			protected int    		noteCount;
			private KPanel			displayPanel;
			private	boolean			hasFocus			= false;

			
			public NotebookListItem(String id, String name, int noteCount)
			{
				this.notebookId = id;
				this.notebookName = name;
				this.noteCount = noteCount;
				
				setLayout(new BorderLayout(10, 10));
				this.setFocusable(false);
				
				displayPanel = new KPanel(new BorderLayout(5, 5));
				
				this.add(displayPanel, BorderLayout.NORTH);
				
				final KLabel nameLabel = new KLabel(name)
				{
					private static final long	serialVersionUID	= 1L;

					public Insets getInsets()
					{
						return new Insets(5, 30, 5, 20);
					}
				};
				nameLabel.setFont(App.settings.getFont(Settings.NotebookItemTitleFont));
				final String countMessage = Integer.toString(noteCount) + (noteCount == 1 ? " note" : " notes");
				final KLabel noteCountLabel = new KLabel(countMessage)
				{
					private static final long	serialVersionUID	= 1L;

					public Insets getInsets()
					{
						return new Insets(5, 60, 10, 0);
					}
				};
				noteCountLabel.setFont(App.settings.getFont(Settings.BasicFont));

				displayPanel.setFocusable(true);
				displayPanel.add(nameLabel, BorderLayout.NORTH);
				displayPanel.add(noteCountLabel, BorderLayout.CENTER);
				displayPanel.addFocusListener(this);
				displayPanel.addKeyListener(this);
			}
			
//			// ----------------------------------------------------------------------------------
//			public synchronized void addFocusListener(FocusListener subscriber)
//			{
//				this.focusListeners = AWTEventMulticaster.add(this.focusListeners, subscriber);
//			}
			
			public void select()
			{
				this.displayPanel.requestFocus();
			}

			// ----------------------------------------------------------------------------------
			public Insets getInsets()
			{
				return new Insets(10, 20, 10, 20);
			}
			
			// ----------------------------------------------------------------------------------
			public void paint(final Graphics g)
			{
				super.paint(g);

				g.setColor(KindletUIResources.getInstance().getColor(KColorName.BLACK));

				// Draw a border at the bottom
				int width = getWidth();
				int height = getHeight();

				Graphics2D g2d = (Graphics2D) g;
				g2d.setStroke(new BasicStroke(1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
				g2d.drawLine(0, height - 2, width, height - 2);
				
				if (this.hasFocus)
				{
					g2d.setStroke(new BasicStroke(4, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
					g2d.drawRect(0, 0, width - 2, height - 2);
				}
				else
				{
					g2d.setStroke(new BasicStroke(1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
					g2d.drawLine(0, height - 2, width, height - 2);
				}
			}
			
			// ----------------------------------------------------------------------------------
			public void focusGained(FocusEvent evt)
			{
				this.hasFocus = true;
//				this.repaint();				
				NotebookListView.this.currentItem = NotebookListItem.this;
//				setBackgrounds(KindletUIResources.getInstance().getColor(KColorName.GRAY_01));
				repaint();
			}

			// ----------------------------------------------------------------------------------
			public void focusLost(FocusEvent arg0)
			{
				this.hasFocus = false;
				this.repaint();
//				setBackgrounds(KindletUIResources.getInstance().getColor(KColorName.WHITE));
				repaint();
			}

			// ----------------------------------------------------------------------------------
			public void keyPressed(KeyEvent e)
			{
				int key = e.getKeyCode();

				switch (key)
				{
					case KindleKeyCodes.VK_FIVE_WAY_SELECT:
					case KeyEvent.VK_ENTER:
						if (App.controller.modalDialogOpen == false)
						{
							String notebookId = NotebookListItem.this.notebookId;
							App.controller.setCurrentNotebook(null);
							App.controller.getEventHandler().actionPerformed(new ActionEvent(NotebookListItem.this, App.NOTEBOOKVIEW, notebookId));
						}
						break;

					case KeyEvent.VK_BACK_SPACE:
					case KeyEvent.VK_DELETE:
					case KindleKeyCodes.VK_FIVE_WAY_LEFT:
//						this.listeners.actionPerformed(new ActionEvent(this, 400, this.noteId));
						break;

//					case KindleKeyCodes.VK_LEFT_HAND_SIDE_TURN_PAGE:
//					case KindleKeyCodes.VK_RIGHT_HAND_SIDE_TURN_PAGE:
//					case KindleKeyCodes.VK_TURN_PAGE_BACK:
//						if (this.keyListeners != null)
//						{
//							this.keyListeners.keyPressed(e);
//						}
//						break;
				}			}

			public void keyReleased(KeyEvent e)
			{
				// TODO Auto-generated method stub
				
			}

			public void keyTyped(KeyEvent e)
			{
				// TODO Auto-generated method stub
				
			}
			
			// ----------------------------------------------------------------------------------
			public void setBackgrounds(Color backgroundColor)
			{
				this.setBackground(backgroundColor);
//				this.nameLabel.setBackground(backgroundColor);
//				this.modifiedLabel.setBackground(backgroundColor);
//				this.createdLabel.setBackground(backgroundColor);
				this.displayPanel.setBackground(backgroundColor);
			}
		}
	
		// ----------------------------------------------------------------------------------
		class NoteDeleteHandler implements ActionListener
		{
			public void actionPerformed(ActionEvent e)
			{
				String message = "The \"" + NotebookListView.this.currentItem.notebookName 
						+ "\" notebook and all its notes will be permanently deleted.  Are you sure you want to do this?";
				
				KOptionPane.showConfirmDialog(NotebookListView.this,
						message, "Delete This Notebook",
						new KOptionPane.ConfirmDialogListener()
						{
							public void onClose(final int option)
							{
								if (option == KOptionPane.OK_OPTION)
								{
									String id = NotebookListView.this.currentItem.notebookId;
									currentItem = null;
									Notebook.delete(id);
									displayItems();
								}
								
								NotebookListView.this.selectCurrentItem();
							}
						});
			}
		}
	}
	
	// ----------------------------------------------------------------------------------
	class byName implements Comparator
	{		
		public byName()
		{
			super();
		}
		
		public int compare(Object entry1, Object entry2)
		{
			int result = ((NotebookMetadata)entry1).name.toLowerCase().compareTo(((NotebookMetadata) entry2).name.toLowerCase());
			
			return result;
		}

	}
		
	//----------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------
	class FooterPanel extends KPanel
	{
		private static final long	serialVersionUID	= -4911638306174881473L;
		private KLabel				notebookNameLabel;
		private KLabel				countLabel;

		// ------------------------------------------------------------------------------
		public FooterPanel()
		{
			try
			{
				this.setLayout(new KBoxLayout(this, KBoxLayout.X_AXIS));
				this.setFocusable(false);
				Font footerFont = App.settings.getFont(Settings.BasicFont);
				this.notebookNameLabel = new KLabel();
				this.notebookNameLabel.setFont(footerFont);
				this.countLabel = new KLabel();
				this.countLabel.setFont(footerFont);
				this.setBackground(KindletUIResources.getInstance().getColor(KColorName.GRAY_02));
				this.add(this.notebookNameLabel);
				this.add(KBox.createHorizontalGlue());
				this.add(this.countLabel);
			}
			catch (Exception ex)
			{
				App.logError(ex.getMessage());
			}
		}

		// -----------------------------------------------------------------------------
		public void updateStatus(int pageNumber, int pageCount, int itemCount)
		{
			String pageStatus = "page " + pageNumber + " of " + pageCount;
			notebookNameLabel.setText(pageStatus);

			if (itemCount == 1)
				countLabel.setText("1 notebook");
			else
				countLabel.setText(itemCount + " notebooks");

			validate();
			repaint();
		}

		// ----------------------------------------------------------------------------------
		public Insets getInsets()
		{
			return new Insets(10, 10, 5, 10);
		}

		// ----------------------------------------------------------------------------------
		public void paint(final Graphics g)
		{
			super.paint(g);

			// Draw a border around the panel
			int width = getWidth();
			g.setColor(KindletUIResources.getInstance().getColor(KColorName.GRAY_05));
			g.drawLine(0, 0, width, 0);
		}

	}
}

/*
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * Copyright (c) 2012, David Alexis. All rights reserved.
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */

package com.izzylabs.kindle;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.amazon.kindle.kindlet.event.KindleKeyCodes;
import com.amazon.kindle.kindlet.ui.*;
import com.amazon.kindle.kindlet.ui.KindletUIResources.KFontStyle;
import com.amazon.kindle.kindlet.ui.KindletUIResources.KColorName;

public final class Help implements IViewable
{
	public HelpView view;
	
	public Help()
	{
		this.view = new HelpView();
	}
	
	public Container getView()
	{
		return this.view;
	}

	public int getViewId()
	{
		return App.HELPVIEW;
	}

	public String getViewName()
	{
		return "help";
	}

	public void setView(Container view)
	{
		this.view = (HelpView)view;
	}

	// ----------------------------------------------------------------------------------
	public class HelpView extends KPanel
	{
		private static final long	serialVersionUID	= 1L;
		
		private Font				textFont;
		private Font				textFontBoldLarge;
		private Font				textFontBold;
		private KPages				pages;
		private FooterPanel 		footer;
		private int 				currentPageNumber;
		private int					pageCount;
		private int 				lineHeight;
		private int 				margin;
		
		// ----------------------------------------------------------------------------------
		public HelpView()
		{	
			setLayout(new BorderLayout(0, 0));
			setFocusable(false);
			
			final TitlePanel title = new TitlePanel("TakeNote Help");
			this.add(title, BorderLayout.NORTH);
			
			this.margin = App.isBigScreen() ? 45 : 20;
			
			addKeyListener(new KeyListener()
			{

				public void keyTyped(KeyEvent arg0)
				{
				}

				public void keyReleased(KeyEvent arg0)
				{
				}

				public void keyPressed(KeyEvent e)
				{
					if (e.isActionKey())
					{
						// If the key pressed is the back key, don't consume it so
						// that
						// we can exit the sample application
						if (e.getKeyCode() == KindleKeyCodes.VK_BACK)
						{
							e.consume();
							App.controller.getEventHandler().actionPerformed(new ActionEvent(HelpView.this, -1, "close"));
						}
					}
				}
			});
			
			this.pages = new KPages() //PageProviders.createFullPageProvider())
			{
				private static final long serialVersionUID = 1L;

				public Insets getInsets()
				{
					return new Insets(margin, margin, margin, margin);
				}
			};

			this.pages.setFirstPageAlignment(BorderLayout.NORTH);
			this.pages.setLastPageAlignment(BorderLayout.NORTH);
			this.pages.setFocusable(true);
			this.pages.setPageKeyPolicy(KPagedContainer.PAGE_KEYS_LOCAL);
			
			this.currentPageNumber = 1;

			add(this.pages, BorderLayout.CENTER);
			
			lineHeight = (int)(App.settings.getLineHeight());
			footer = new FooterPanel();
			add(footer, BorderLayout.SOUTH);
			
			int fontSmall, fontMedium, fontLarge;
			
			if (App.isBigScreen())
			{
				fontSmall = 23;
				fontMedium = 25;
				fontLarge = 28;
			}
			else
			{
				fontSmall = 21;
				fontMedium = 22;
				fontLarge = 25;
			}
			
			final KindletUIResources resources = KindletUIResources.getInstance();
			textFont = resources.getFont(KindletUIResources.KFontFamilyName.SANS_SERIF, fontSmall, KFontStyle.PLAIN);
			textFontBold = resources.getFont(KindletUIResources.KFontFamilyName.SANS_SERIF, fontMedium, KFontStyle.BOLD);
			textFontBoldLarge = resources.getFont(KindletUIResources.KFontFamilyName.SANS_SERIF, fontLarge, KFontStyle.BOLD);
			
			final List contentPages = new ArrayList();
			RenderHelpPagesFromResouce(contentPages);
			
			final Iterator pageIterator = contentPages.iterator();
			
			while (pageIterator.hasNext())
			{
				pages.addItem(pageIterator.next());
			}
			
			pages.setFocusTraversalKeysEnabled(false);		
			pages.validate();
			pages.setLocation(pages.getPageStartLocation());
			
			HelpView.this.footer.updateStatus();

			pages.validate();
			this.pageCount = pages.getPageModel().getLastLocation() + 1;
			footer.updateStatus();

			addComponentListener(new ComponentListener()
			{
				public void componentShown(ComponentEvent e)
				{
					pages.setPageKeyPolicy(KPagedContainer.PAGE_KEYS_GLOBAL);
					pages.validate();
					pages.requestFocus();
					pages.setLocation(pages.getPageStartLocation());
					repaint();
					App.context.setMenu(null);
					App.context.setTextOptionPane(null);
				}

				public void componentResized(ComponentEvent e)
				{
					pages.validate();
					pages.requestFocus();
					pages.setLocation(pages.getPageStartLocation());
					repaint();
				}

				public void componentMoved(ComponentEvent e)
				{
				}

				public void componentHidden(ComponentEvent e)
				{
					pages.setPageKeyPolicy(KPagedContainer.PAGE_KEYS_DISABLED);
				}
			});
			
			pages.addPropertyChangeListener(new PropertyChangeListener()
			{

				public void propertyChange(PropertyChangeEvent evt)
				{				
					if (evt.getPropertyName().compareTo("pageEndLocation") == 0)
					{
						EventQueue.invokeLater(new Runnable()
						{
							public void run()
							{
								pages.validate();
								pages.relayoutPage();
															
								currentPageNumber = pages.getPageStartLocation() + 1;
								HelpView.this.footer.updateStatus();
							}
						});
					}
				}
			});
		}

		// ----------------------------------------------------------------------------------
		public KPanel createGridBagParagraph()
		{
			KPanel paragraph = new KPanel(new GridBagLayout())
			{
				private static final long	serialVersionUID	= 1L;

				public Insets getInsets()
			 	{
			 		return new Insets(0, 20, 0, 10);
			 	}
			};
			return paragraph;
		}

		// ----------------------------------------------------------------------------------
		private KBox createPage()
		{
			KBox page;
			page = KBox.createVerticalBox();
			return page;
		}
		
		// ----------------------------------------------------------------------------------
		private KComponent createHeading1(String text, final int spacingAfter)
		{
			final KLabelMultiline headingLabel = new KLabelMultiline2(text)
			{
				private static final long	serialVersionUID	= 1L;

				public Insets getInsets()
				{
					return new Insets(10, 20, lineHeight, 20);
				}
			};
			
			headingLabel.setEditable(false);
			headingLabel.setFocusable(false);
			headingLabel.setFont(this.textFontBoldLarge);
			
			return headingLabel;
		}
		
		// ----------------------------------------------------------------------------------
		private KComponent createHeading2(String text, final int spacingAfter)
		{
			final KLabelMultiline headingLabel = new KLabelMultiline2(text)
			{
				private static final long	serialVersionUID	= 1L;

				public Insets getInsets()
				{
					return new Insets(5, 20,  lineHeight + spacingAfter, 20);
				}
			};
			
			headingLabel.setEditable(false);
			headingLabel.setFocusable(false);
			headingLabel.setFont(this.textFontBold);
			
			return headingLabel;		
		}
		
		// ----------------------------------------------------------------------------------
		private KComponent createParagraph(final String text)
		{
			return createParagraph(text, false, 10);
		}
						
		// ----------------------------------------------------------------------------------
		private KComponent createParagraph(final String text, final boolean bold, final int spacingAfter)
		{			
			KLabelMultiline paragraph = new KLabelMultiline2(text);
			
			if (bold)
			{
				paragraph.setFont(textFontBold);
			}
			else
			{
				paragraph.setFont(textFont);
			}

			paragraph.validate();
			
			return paragraph;
		}
		
		// ----------------------------------------------------------------------------------
		private void RenderHelpPagesFromResouce(List contentPages)
		{
			String helpText =  null;
			
			// Read help text from resource
			try
			{
				InputStream stream = getClass().getResourceAsStream("/files/helpText");

				try
				{
					helpText = FileUtilities.convertStreamToString(stream);
				}
				catch (IOException e)
				{
					e.printStackTrace();
					App.logError(e.getMessage());
				}
				finally
				{
					if (stream != null)
					{
						try
						{
							stream.close();
						}
						catch (IOException e)
						{
							e.printStackTrace();
							App.logError(e.getMessage());
						}
					}
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
				App.logError(e.getMessage());
			}
			
			// Now let's Render the text into a set of page objects
			KBox page = createPage();

			LineParser parser = new LineParser(helpText);
			String thisLine;
			
			while ((thisLine = parser.getNextLineOfText()) != null)
			{
				if (thisLine.equals("----"))
				{
					// New page
					page.add(KBox.createVerticalGlue());
					contentPages.add(page);
					page = createPage();	
				}
				else if (thisLine.startsWith("** "))
				{
					// Heading 1
					page.add(createHeading1(thisLine.substring(3), 10));
				}
				else if (thisLine.startsWith("*** "))
				{
					// Heading 2
					page.add(createHeading2(thisLine.substring(4), 10));
				}
				else if (thisLine.startsWith("**** "))
				{
					// Bold
					page.add(createParagraph(thisLine.substring(5), true, 0));
					
				}
				else if (thisLine.equals("~~~"))
				{
					// Half line spacing
					page.add(KBox.createVerticalStrut(lineHeight / 2));
				}
				else if (thisLine.equals(""))
				{
					page.add(KBox.createVerticalStrut(lineHeight));
				}
				else if (thisLine.equals("??IsSmallScreen"))
				{
					// Early page break for small screens.  If false, skip to end brace "}"
					if (App.isBigScreen())
					{
						// Skip to end brace
						while ((thisLine = parser.getNextLineOfText()) != null)
						{
							if (thisLine.equals("}"))
								break;
						}
					}
				}
				else if (thisLine.equals("{") || thisLine.equals("}"))
				{
					// Skip these lines.  Do nothing.
				}
				else
				{
					// Must be content.  Add it.
					page.add(createParagraph(thisLine));
				}
			}
			
			contentPages.add(page);
		}
		
		// ----------------------------------------------------------------------------------
		private class FooterPanel extends KPanel
		{
		 	private static final long	serialVersionUID	= -4911638306174881473L;
		 	private KLabel				morePagesLabel;

		 	public FooterPanel()
		 	{
		 		try
		 		{
		 			setLayout(new BorderLayout());
		 			setFocusable(false);
		 			this.setBackground(KindletUIResources.getInstance().getColor(KColorName.GRAY_02));
		 			
		 			Font footerFont = App.settings.getFont(Settings.BasicFont);
		 			this.morePagesLabel = new KLabel();
		 			this.morePagesLabel.setFont(footerFont);
		 			KLabel buildLabel = new KLabel("TakeNote Version " + App.getAppVersion());
		 			buildLabel.setFont(footerFont);
		 			
		 			add(buildLabel, BorderLayout.EAST);
		 			add(this.morePagesLabel, BorderLayout.WEST);
		 		}
		 		catch (Exception ex)
		 		{
		 			App.logError(ex.getMessage());
		 		}
		 	}

		 	public void updateStatus()
		 	{
		 		morePagesLabel.setText("page " + HelpView.this.currentPageNumber + " of " + HelpView.this.pageCount);
		 		validate();
		 		pages.requestFocus();
		 		repaint();
		 	}

		 	// ----------------------------------------------------------------------------------
			public Insets getInsets()
			{
				return new Insets(10, 10, 5, 10);
			}

		 	public void paint(Graphics g)
		 	{
		 		super.paint(g);

		 		// Draw a border around the panel
		 		int width = getWidth();
		 		g.setColor(KindletUIResources.getInstance().getColor(KColorName.GRAY_05));
		 		g.drawLine(0, 5, width, 5);
		 	}

		}
	
		// ----------------------------------------------------------------------------------
		public class KLabelMultiline2 extends KLabelMultiline 
		{
			private static final long serialVersionUID = 1L;
			
			public KLabelMultiline2(String text) 
		     {
		         super(text);
		     }
		     
		     protected Dimension getAncestorMaxInteriorSize() 
		     {	         
		         return getAncestorMaxInteriorSize(this.getParent());
		     }	
		     
		     private Dimension getAncestorMaxInteriorSize(Container parent) 
		     {
		         if (parent == null) 
		         {                                  
	                return App.context.getRootContainer().getSize();   
	            } 
	            
	            Dimension maxSize = parent.getSize(); 
	             
	            if( ! parent.isValid()) 
	            { 
	                if(parent.getParent() == null || parent.getParent().getLayout() != null) 
	                {   
	                    maxSize = getAncestorMaxInteriorSize(parent.getParent());   
	                }   
	            }
	            
	            Insets parentInsets = parent.getInsets();
	            
	            if(null != parentInsets) 
	            {  
	                maxSize.setSize(Math.max(maxSize.width  - (parentInsets.left + parentInsets.right), 0),  
	                        Math.max(maxSize.height - (parentInsets.top  + parentInsets.bottom), 0));  
	            }
	            
	            return maxSize;
		     }	     
		 }
	
		// ----------------------------------------------------------------------------------
		class LineParser
		{
			private String content;
			private int endPosition;
			private int currentPosition = 0;
			
			// ----------------------------------------------------------------------------------
			public LineParser(String text)
			{
				this.content = text;
				this.endPosition = this.content.length();
			}
			
			// ----------------------------------------------------------------------------------
			public String getNextLineOfText()
			{
				String line = null;
				
				if (this.currentPosition >= this.endPosition)
				{
					return null;
				}
				
				// Scan text to break at paragraphs
				for (int position = this.currentPosition; position <= endPosition; position++)
				{
					char thisChar = position == endPosition ? ' ' : this.content.charAt(position);

					if (thisChar == '\r' || thisChar == '\n' || position >= endPosition)
					{
						line = this.content.substring(this.currentPosition, position);
						this.currentPosition = position + 1;
						
						return line.trim();
					}
				}
							
				return null;
			}
		}
	}
}

/* 
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * Copyright (c) 2010, David Alexis. All rights reserved.
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */

package com.izzylabs.kindle;

import java.awt.*;
import java.awt.event.*;

import com.amazon.kindle.kindlet.event.KindleKeyCodes;
import com.amazon.kindle.kindlet.ui.*;
import com.amazon.kindle.kindlet.ui.KindletUIResources.KColorName;

public class NoteListItem extends KPanel implements FocusListener, KeyListener
{

	private static final long	serialVersionUID	= 1L;
	private KLabel				nameLabel;
	private KLabel				createdLabel;
	private KLabel				modifiedLabel;
	public  String				noteId;
	private KPanel				displayPanel;
//	private ActionListener		listeners			= null;
	private FocusListener		focusListeners		= null;
	private KeyListener			keyListeners		= null;
	private	boolean				hasFocus			= false;

	// ----------------------------------------------------------------------------------
	public NoteListItem(String id, String name, String created, String modified)
	{
		this.noteId = id;
		setLayout(new BorderLayout(10, 10));
		setFocusable(false);

		displayPanel = new KPanel(new BorderLayout(5, 5));

		nameLabel = new KLabel(name);
		nameLabel.setFont(App.settings.getFont(Settings.NotebookItemTitleFont));
		createdLabel = new KLabel(created);
		modifiedLabel = new KLabel(modified);
		
		createdLabel.setFont(App.settings.getFont(Settings.BasicFont));
		modifiedLabel.setFont(App.settings.getFont(Settings.BasicFont));
		
		displayPanel.addFocusListener(this);
		displayPanel.addKeyListener(this);

		displayPanel.add(nameLabel, BorderLayout.NORTH);
		displayPanel.add(createdLabel, BorderLayout.WEST);
		displayPanel.add(modifiedLabel, BorderLayout.EAST);
		this.add(displayPanel, BorderLayout.NORTH);

		validate();
	}

	// ----------------------------------------------------------------------------------
	public Insets getInsets()
	{
//		int margin = ApplicationContext.viewManager.getScreenHeight() <= 760 ? 10 : 30;
//		return new Insets(10, margin, 10, margin);
		return new Insets(10, 10, 10, 10);
	}

	// ----------------------------------------------------------------------------------
	public void select()
	{
		this.displayPanel.requestFocus();
	}

	// ----------------------------------------------------------------------------------
	public void paint(final Graphics g)
	{
		super.paint(g);

		g.setColor(KindletUIResources.getInstance().getColor(KColorName.BLACK));

		// Draw a border at the bottom
		int width = getWidth();
		int height = getHeight();

		Graphics2D g2d = (Graphics2D) g;
		g2d.setStroke(new BasicStroke(1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
		g2d.drawLine(0, height - 2, width, height - 2);
		
		if (this.hasFocus)
		{
			g2d.setStroke(new BasicStroke(4, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
			g2d.drawRect(0, 0, width - 4, height - 4);
		}
	}

	// ----------------------------------------------------------------------------------
	public void setText(String name, String created, String modified)
	{
		this.nameLabel.setText(name);
		this.createdLabel.setText(created);
		this.modifiedLabel.setText(modified);
		repaint();
	}

	// ----------------------------------------------------------------------------------
	public synchronized void addFocusListener(FocusListener subscriber)
	{
		this.focusListeners = AWTEventMulticaster.add(this.focusListeners, subscriber);
	}

	// ----------------------------------------------------------------------------------
	public synchronized void addKeyListener(KeyListener subscriber)
	{
		App.logInfo("add key listener");
		
		this.keyListeners = AWTEventMulticaster.add(this.keyListeners, subscriber);
	}

	// ----------------------------------------------------------------------------------
	public void triggerDelete()
	{
//		this.listeners.actionPerformed(new ActionEvent(this, 400, this.noteId));
	}

	// ----------------------------------------------------------------------------------
	public void focusGained(FocusEvent arg0)
	{
		this.hasFocus = true;

		if (this.focusListeners != null)
			this.focusListeners.focusGained(new FocusEvent(this, 0));
		
//		setBackgrounds(KindletUIResources.getInstance().getColor(KColorName.BLACK));
		repaint();
	}

	// ----------------------------------------------------------------------------------
	public void focusLost(FocusEvent arg0)
	{
		this.hasFocus = false;

		if (this.focusListeners != null)
			this.focusListeners.focusLost(new FocusEvent(this, 0));
		
//		setBackgrounds(KindletUIResources.getInstance().getColor(KColorName.WHITE));
		repaint();
	}

	// ----------------------------------------------------------------------------------
	public void keyPressed(KeyEvent e)
	{
		int key = e.getKeyCode();

		App.logInfo("keypressed");
		
		switch (key)
		{
			case KindleKeyCodes.VK_FIVE_WAY_SELECT:
			case KeyEvent.VK_ENTER:
				// Raise EditNote event. Pass note ID in event object.
				if (App.controller.modalDialogOpen == false)
				{
					String noteId = NoteListItem.this.noteId;
					App.controller.getEventHandler().actionPerformed(new ActionEvent(NoteListItem.this, App.EDITORVIEW, noteId));
				}
				
//				this.listeners.actionPerformed(new ActionEvent(this, 100, this.noteId));
				break;

			case KeyEvent.VK_BACK_SPACE:
			case KeyEvent.VK_DELETE:
			case KindleKeyCodes.VK_FIVE_WAY_LEFT:
//				this.listeners.actionPerformed(new ActionEvent(this, 400, this.noteId));
				break;

//			case KindleKeyCodes.VK_LEFT_HAND_SIDE_TURN_PAGE:
//			case KindleKeyCodes.VK_RIGHT_HAND_SIDE_TURN_PAGE:
//			case KindleKeyCodes.VK_TURN_PAGE_BACK:
//				if (this.keyListeners != null)
//				{
//					this.keyListeners.keyPressed(e);
//				}
//				break;
		}
	}

	// ----------------------------------------------------------------------------------
	public void keyReleased(KeyEvent arg0)
	{
		App.logInfo("key released");
	}

	// ----------------------------------------------------------------------------------
	public void keyTyped(KeyEvent arg0)
	{
		App.logInfo("keyTyped");
	}
	
	// ----------------------------------------------------------------------------------
	public void setBackgrounds(Color backgroundColor)
	{
		this.setBackground(backgroundColor);
		this.nameLabel.setBackground(backgroundColor);
		this.modifiedLabel.setBackground(backgroundColor);
		this.createdLabel.setBackground(backgroundColor);
		this.displayPanel.setBackground(backgroundColor);
	}
}

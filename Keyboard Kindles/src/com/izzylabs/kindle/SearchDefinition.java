/*
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * Copyright (c) 2012, David Alexis. All rights reserved.
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */

package com.izzylabs.kindle;

public final class SearchDefinition
{
	public String 	term;
	public int 		option;
	
	public SearchDefinition(String term, int option)
	{
		this.term = term;
		this.option = option;
	}

	public static class SearchOptions
	{
		public static final int SEARCH_EXACT = 0;
		public static final int SEARCH_ALL   = 1;
		public static final int SEARCH_ANY   = 2;
	}
}

package com.izzylabs.kindle;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import com.amazon.kindle.kindlet.event.KindleKeyCodes;
import com.amazon.kindle.kindlet.ui.KComponent;
import com.amazon.kindle.kindlet.ui.KPanel;
import com.amazon.kindle.kindlet.ui.KindletUIResources;

public class SymbolDialog extends KPanel
{
	private static final long	serialVersionUID	= -3344637728205525388L;
	
	// ----------------------------------------------------------------------------------
	private final String[][] symbols = 
	{
		{"<", ">", "\"", "'", "?", "¿", "{", "}", "|", "©", "®", "™"},
		{":", ";", ",", ".", "!", "¡", "/", "\\", "[", "]", "«", "»"},
		{"@", "&", "#", "%", "^", "*", "(", ")", "-", "+", "÷", "="},
		{"$", "£", "€", "¢", "§", "¶", "•", "¯", "¼", "½", "¾", "°"},
		{"1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "~", "²"}
	};

	private String instructionCaption = "(press 'SYM' key to close symbols)";
	private Point currentSymbolLocation;
	private ActionListener notificationTarget;
	private KComponent targetComponent;
	final int dialogWidth = 400;
	final int dialogHeight = 250;
	
	// ----------------------------------------------------------------------------------
	public SymbolDialog(ActionListener actionListener)
	{		
		setLayout(null);
        setSize(dialogWidth, dialogHeight);
        setFocusTraversalKeysEnabled(false);
        currentSymbolLocation = new Point(2,1);
		addKeyListener(new KeyHandler());
		this.notificationTarget = actionListener;
	}
	
	// ----------------------------------------------------------------------------------
	public void showDialog(KComponent target)
	{
		try
		{
			this.targetComponent = target;
			this.setVisible(true);
			this.requestFocus();
			
			App.controller.modalDialogOpen = true;
			App.context.setMenu(null);
			App.context.setTextOptionPane(null);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	// ----------------------------------------------------------------------------------
	public void paint(Graphics g)
	{
		super.paint(g);

		Graphics2D g2d = (Graphics2D) g;
		
		g2d.setStroke(new BasicStroke(4, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
		g2d.drawRoundRect(0, 0, dialogWidth - 2, dialogHeight - 2, 30, 30);
		
		g2d.setFont(App.settings.getFont(Settings.MediumBoldFont));

        FontMetrics fontMetrics = g.getFontMetrics();
        int fontHeight = fontMetrics.getHeight();
        int fontWidth = fontMetrics.stringWidth("W");
        Color foreColor = App.context.getUIResources().getBackgroundColor(KindletUIResources.KColorName.BLACK);
        Color backColor = App.context.getUIResources().getBackgroundColor(KindletUIResources.KColorName.WHITE);
        
		for (int yindex = 0; yindex < 5; yindex++)
		{
		
			for (int xindex = 0; xindex < 12; xindex++)
			{
				int y = (yindex + 1) * 40;
				int x = (xindex + 1) * 30;
			
				// Paint it!

				if (xindex == currentSymbolLocation.x && yindex == currentSymbolLocation.y)
				{					
					// Render current selected character
					g2d.setColor(foreColor);
					g2d.fillOval(x - fontWidth, y - fontHeight - 10, 35, 35);
					g2d.setColor(backColor);
					g2d.drawString(symbols[yindex][xindex], x - (fontMetrics.stringWidth(symbols[yindex][xindex]) / 2) - 4, y - (fontHeight / 2));
					g2d.setColor(foreColor);
				}
				else
				{
					g2d.drawString(symbols[yindex][xindex], x - (fontMetrics.stringWidth(symbols[yindex][xindex]) / 2) - 4, y - (fontHeight / 2));
				}
				

			}
		}
		
		g2d.setFont(App.settings.getFont(Settings.BasicFont));
        int stringWidth = fontMetrics.stringWidth(instructionCaption);
        int stringStartX = (dialogWidth - stringWidth ) / 2 + 60;
        g.drawString(instructionCaption, stringStartX, 230); 

	}
	
	// ----------------------------------------------------------------------------------
	class KeyHandler implements KeyListener
	{

		public void keyPressed(KeyEvent e)
		{
			e.consume();

			switch (e.getKeyCode())
			{
				case KindleKeyCodes.VK_SYMBOL:
				case KindleKeyCodes.VK_BACK:
					e.consume();
					// The Symbol key was pressed. Close the dialog.
					setVisible(false);
					App.controller.modalDialogOpen = false;
					
					if (notificationTarget != null)
					{
						int commandId = (targetComponent.getClass() == TextArea.class) ? 610 : 611;
						notificationTarget.actionPerformed(new ActionEvent(e.getSource(), commandId, "closedialog"));
					}
					
					break;
					
				case KindleKeyCodes.VK_FIVE_WAY_DOWN:
					changeSelection(0, 1);
					break;
					
				case KindleKeyCodes.VK_FIVE_WAY_UP:
					changeSelection(0, -1);
					break;
					
				case KindleKeyCodes.VK_FIVE_WAY_LEFT:
					changeSelection(-1, 0);
					break;
					
				case KindleKeyCodes.VK_FIVE_WAY_RIGHT:
					changeSelection(1, 0);
					break;
					
				case KindleKeyCodes.VK_FIVE_WAY_SELECT:
					if (notificationTarget != null)
					{
						int commandId = (targetComponent.getClass() == TextArea.class) ? 600 : 601;
						notificationTarget.actionPerformed(new ActionEvent(this, commandId, symbols[currentSymbolLocation.y][currentSymbolLocation.x] ));
					}
					repaint();
					break;
					
				default:
					if (e.getKeyChar() == KeyEvent.VK_ENTER)
					{
						if (notificationTarget != null)
						{
							int commandId = (targetComponent.getClass() == TextArea.class) ? 600 : 601;
							notificationTarget.actionPerformed(new ActionEvent(this, commandId, symbols[currentSymbolLocation.y][currentSymbolLocation.x] ));
						}
					}
					else
					{
						e.setKeyCode(KeyEvent.VK_UNDEFINED);
						e.setKeyCode(0);
					}
					break;
			}		
		}

		public void keyReleased(KeyEvent e)
		{
			e.consume();
		}

		public void keyTyped(KeyEvent e)
		{
			e.consume();
		}
		
	}
	
	// ----------------------------------------------------------------------------------
	private void changeSelection(int xChange, int yChange)
	{
		int newX = this.currentSymbolLocation.x + xChange;
		int newY = this.currentSymbolLocation.y + yChange;
		
		if (newX < 12 && newY < 5 && newX >= 0 && newY >= 0)
		{
			this.currentSymbolLocation.x = newX;
			this.currentSymbolLocation.y = newY;
			repaint();
		}
	}
}
/*
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * Copyright (c) 2012, David Alexis. All rights reserved.
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */

package com.izzylabs.kindle;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Container;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.*;
import java.io.Serializable;
import java.util.*;

import com.amazon.kindle.kindlet.event.KindleKeyCodes;
import com.amazon.kindle.kindlet.ui.KOptionPane;
import com.amazon.kindle.kindlet.ui.KPanel;
import com.amazon.kindle.kindlet.util.Timer;
import com.amazon.kindle.kindlet.util.TimerTask;

public final class Controller implements Serializable
{
	private static final long	serialVersionUID	= -5587509040724238782L;
	private KPanel				uiContainer;
	private CardLayout			cardPanelLayout;
	private KPanel				cardPanel;
	private Stack				viewStack;
	private Notebook			currentNotebook;
	public boolean				modalDialogOpen;
	private Timer 				memoryCheckTimer;
	private KeyboardFocusManager	keyboardFocusManager;
	protected long				lastTimeBackButtonWasHit;
	protected int				currentView;
	private static final String	defaultViewName = "notebook";
	
	// ----------------------------------------------------------------------------------
	public Controller()
	{
		// Startup code goes here
		// Set up the UI
		setupKeyboardHook();
		this.uiContainer = new KPanel();
		final Container root = App.context.getRootContainer();
		root.setLayout(new BorderLayout(0, 0));
		root.add(this.uiContainer, BorderLayout.CENTER);
//		root.setFocusable(false);
		
		this.uiContainer.setLayout(new BorderLayout(0, 0));
		this.uiContainer.setFocusable(false);
		this.viewStack = new Stack();
		
		
		// Create the CardLayout panel containing the UI screen cards
		this.cardPanelLayout = new CardLayout();
		this.cardPanel = new KPanel(this.cardPanelLayout);
		this.cardPanel.setFocusable(false);
		this.uiContainer.add(this.cardPanel);

		App.controller = this;
		String lastNotebook = App.settings.getCurrentNotebookId();
		this.setCurrentNotebook(new Notebook(lastNotebook));
		this.cardPanel.add(this.currentNotebook.getView(), "notebook");

		if (App.settings.getHasSavedState() == true)
		{
			this.RequestViewById(App.EDITORVIEW, "autosave");
			App.settings.setHasSavedState(false);
		}
		else
		{
			this.RequestViewById(App.NOTEBOOKVIEW, null);
		}
		
		startMemoryChecker();
	}

	// ----------------------------------------------------------------------------------
	private void RequestViewById(int viewId, String viewData)
	{
		String viewName = defaultViewName;
		
		if (viewId == App.PREVIOUSVIEW)
		{
			if (this.viewStack.empty())
			{
				// No views on the stack.  Go to the Notebook view
				viewId = App.NOTEBOOKVIEW;
				viewName = "notebook";
			}
			else
			{
				// Pop the stack and remove that item from the cardPanel
				IViewable currentInterface = (IViewable)this.viewStack.pop();
				this.cardPanel.remove(currentInterface.getView());
				
				// Get the view that needs to be displayed
				if (this.viewStack.empty())
				{
					viewId = App.NOTEBOOKVIEW;
					viewName = "notebook";
				}
				else
				{
					IViewable interfaceToShow = (IViewable)this.viewStack.peek(); 
					viewId = interfaceToShow.getViewId();
					viewName = interfaceToShow.getViewName();
				}				
				
				if (viewName == "notebook" && this.currentNotebook == null)
				{
					this.setCurrentNotebook(new Notebook(Notebook.DefaultNotebokId));
					this.viewStack.pop();
					this.cardPanel.remove(currentInterface.getView());
					buildViewableObject(this.currentNotebook);
				}
			}
		}
		else
		{
			switch (viewId)
			{
				case App.NOTEBOOKVIEW:
					if (this.currentNotebook == null)
					{
						String currentNotebookId = viewData;
						
						if (currentNotebookId == null || currentNotebookId == "")
						{
							currentNotebookId = "notebook";
						}
						
						this.setCurrentNotebook(new Notebook(currentNotebookId));
					}
					
					viewName = this.currentNotebook.getViewName();
					buildViewableObject(this.currentNotebook);
					break;
					
				case App.NOTEBOOKLISTVIEW:
					IViewable notebooks = new NotebookList();
					viewName = notebooks.getViewName();
					buildViewableObject(notebooks);
					break;
				
				case App.EDITORVIEW:
					// The id of the note we want to edit is in the viewData variable.
					IViewable editor = new NoteEditor(viewData);
					viewName = editor.getViewName();
					buildViewableObject(editor);
					break;
					
				case App.HELPVIEW:
					IViewable help = new Help();
					viewName = help.getViewName();
					buildViewableObject(help);
					break;
			}
		}
		
		this.currentView = viewId;
		this.cardPanelLayout.show(this.cardPanel, viewName);
	}
	
	// ----------------------------------------------------------------------------------
	public void shutdown()
	{
		stopMemoryChecker();
		memoryCheckTimer = null;
		
		// Save editor state
		IViewable currentInterface = (IViewable)this.viewStack.peek(); 
		
		if (currentInterface.getViewId() == App.EDITORVIEW)
		{
			((NoteEditor)currentInterface).saveState();
			App.settings.save();
		}
		
		// Clean up view/controller objects
//		this.dashboard.setView(null);
//		this.dashboard = null;
		this.currentNotebook.setView(null);
		this.currentNotebook = null;
		
		//TODO:  Clean up any other views/controllers
		
		// Clean up the rest
		this.cardPanel = null;
		this.cardPanelLayout = null;
		this.uiContainer = null;
	}
	
	// ----------------------------------------------------------------------------------
	public ActionListener getEventHandler()
	{
		return new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				App.logInfo("controller actionPerformed");
				
				int actionId = e.getID();
				String actionData = e.getActionCommand();
				RequestViewById(actionId, actionData);
			}
		};
	}

	// ----------------------------------------------------------------------------------
	private void buildViewableObject(IViewable viewableObject)
	{
		this.viewStack.push(viewableObject);
		this.cardPanel.add(viewableObject.getView(), viewableObject.getViewName());
	}

	// ----------------------------------------------------------------------------------
	public Notebook getCurrentNotebook() 
	{
		return currentNotebook;
	}

	// ----------------------------------------------------------------------------------
	public void setCurrentNotebook(Notebook currentNotebook) 
	{
		this.currentNotebook = currentNotebook;
		
		if (currentNotebook != null)
		{
			App.settings.setCurrentNotebookId(currentNotebook.id);
		}
	}

	// ----------------------------------------------------------------------------------
	public void startMemoryChecker()
	{
		memoryCheckTimer = new Timer();
		memoryCheckTimer.schedule(new MemoryCheck(), 5000);
	}
	
	// ----------------------------------------------------------------------------------
	public void stopMemoryChecker()
	{
		memoryCheckTimer.cancel();
	}
	
	// ----------------------------------------------------------------------------------
	public void setupKeyboardHook()
	{
		try
		{
			keyboardFocusManager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
			keyboardFocusManager.addKeyEventDispatcher(new KeyEventDispatcher() 
			{
	            public boolean dispatchKeyEvent(final KeyEvent key) 
	            {
	            	try
	            	{
		        		if (!key.isConsumed())
		            	{
			        		int keyCode = key.getKeyCode();
				        
		            		// Handle the Back key
			                if (keyCode == KindleKeyCodes.VK_BACK) 
			                {
				        		if (App.controller.modalDialogOpen)
				        		{
				        			key.consume();
				        			return false;
				        		}
	
				        		int id = key.getID();

				        		// Only handle the key on the KeyReleased event.  The process does not seem to work
			                	// properly if handled in KeyPressed.
			                	if (id == KeyEvent.KEY_RELEASED)
			                	{           
			                		if (lastTimeBackButtonWasHit != -1)
		                			{
				                		// Normal Back key processing...
					                    // Ask view manager to handle the Back request.
				                		key.consume();
//				                		Controller.this.RequestViewById(-1, null);
				                		handleBackKey();
				                    	return true;
		                			}
			                	}
			                	else
			                	{
			                		// This is the KeyPressed event.  If we're not in the Symbol dialog or in the Notebook view, eat the event
			                		// otherwise it interferes with our tracking of the Symbol dialog.
			                		if ( Controller.this.currentView == App.NOTEBOOKVIEW)
		                			{
		                				long now = System.currentTimeMillis();
		                				
		                				if (lastTimeBackButtonWasHit == 0 || (now - lastTimeBackButtonWasHit) > 200)
		                				{
					                		key.consume();				                			
		                					lastTimeBackButtonWasHit = now;
		                					
		                					// Instrumentation.mark("Controller.dispatchKeyEvent()", false);
		                					return true;
		                				}
		                				else
		                				{
		                					lastTimeBackButtonWasHit = -1;
		                					return false;
		                				}
		                			}
			                		else
			                		{
			                			key.consume();
			                			return true;
			                		}
			                	}
			                }
		            	}
	            	}
	            	catch (Exception ex)
	            	{
	            		ex.printStackTrace();
	            	}

	                return false;
	            }
	
	        });
	
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	// ----------------------------------------------------------------------------------
	public boolean handleBackKey()
	{
		boolean consumeKey = true;

		try
		{
//			if (modalDialogOpen)
//			{
//				KOptionPane.dismissDialog(editorPanel);
//			}
//			else
			if (!modalDialogOpen)
			{
				// Handle the Back key appropriately depending on the current
				// view.
				// First let's figure out where we are.
				switch (this.currentView)
				{
					case App.HELPVIEW:
						// Go back to previous view
						Controller.this.RequestViewById(-1, null);
						System.gc(); 
						System.gc();
						break;

					case App.EDITORVIEW:
						// Check if the editor is dirty. If so, ask the user if
						// they are
						// sure they want to cancel.
						try
						{
							NoteEditor editorInterface = (NoteEditor)this.viewStack.peek(); 
							if (editorInterface.view.getIsDirty())
							{
								KOptionPane.showConfirmDialog(editorInterface.view,
										"Are you sure you want to lose the changes you made to your note?",
										"Confirm: Exit without saving?", new KOptionPane.ConfirmDialogListener()
										{
											public void onClose(final int option)
											{
												if (option == KOptionPane.OK_OPTION)
												{
													Controller.this.RequestViewById(-1, null);
												}
											}
										});
							}
							else
							{
								// Go back to the notebook view
								Controller.this.RequestViewById(-1, null);
							}
						}
						catch (Exception e) {}
						
						System.gc(); 
						System.gc();

						break;

					default:
//						setView(ApplicationContext.NOTEBOOK);			
						Controller.this.RequestViewById(-1, null);
						System.gc(); 
						System.gc();

						consumeKey = false;
						break;
				}
			}
		}
		catch (Exception ex)
		{
			App.logError(ex.getMessage());
			ex.printStackTrace();
		}

		return consumeKey;
	}

	// ----------------------------------------------------------------------------------
	class MemoryCheck extends TimerTask 
	{
	    public void run() 
	    {
	    	if (!Thread.interrupted())
			{
	    		if (Runtime.getRuntime().freeMemory() <= 1024 * 1024 * 2)
				{
					System.gc();
					System.gc();
				}
				
				memoryCheckTimer.schedule(new MemoryCheck(), 10000);
			}
	    }
	}
	

}

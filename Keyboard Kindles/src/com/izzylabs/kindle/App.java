/*
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * Copyright (c) 2012, David Alexis. All rights reserved.
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */

package com.izzylabs.kindle;

import java.awt.EventQueue;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

import org.apache.log4j.*;

import com.amazon.kindle.kindlet.*;

public class App extends AbstractKindlet
{
	public static KindletContext		context;
	public static boolean				isStartingUp;
	public static boolean				isFirstRun;
	public static Settings				settings;
	public static Controller			controller;
	
	
	static final int					NOVIEW				= 0;
	static final int					DASHBOARDVIEW		= 1;
	static final int					NOTEBOOKLISTVIEW	= 2;
	static final int					NOTEBOOKVIEW		= 3;
	static final int					EDITORVIEW			= 4;
	static final int					SETTINGSVIEW		= 5;
	static final int					HELPVIEW			= 6;
	static final int					SEARCHVIEW			= 7;
	static final int					PREVIOUSVIEW		= -1;
	
	private static boolean 			stopped				= false;
	private static String				appVersion			= null;
	private static Logger				logger;
	
	// ----------------------------------------------------------------------------------
	public void create(final KindletContext context)
	{
		try
		{
			App.isStartingUp = true;
			App.context = context;
			initializeLogger(context);					
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

	// ----------------------------------------------------------------------------------
	public void start()
	{
		synchronized (this)
		{
			App.setStopped(false);
			super.start();
			
			try
			{
				if (isStartingUp)
				{
					EventQueue.invokeLater( new Runnable() 
					{
					    public void run() 
					    { 
					    	App.this.doStartup();
					    }
					});				
				}
				else
				{
					App.controller.startMemoryChecker();
				}
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
			}
		}
	}

	// ----------------------------------------------------------------------------------
	public void stop()
	{
		try
		{
			App.setStopped(true);
			
			synchronized (this)
			{
				App.settings.save();
				App.controller.stopMemoryChecker();
			}
		}
		catch (Exception e)
		{
		}
	}

	// ----------------------------------------------------------------------------------
	public void destroy()
	{
		try
		{
			// Detect whether we're in the editor.
			// If so, save state so that it can be retrieved on next load
//			ApplicationContext.saveState();
			
			App.settings.save();	
			App.controller.shutdown();	
			App.context.getRootContainer().setLayout(null);
			
			App.settings = null;
			App.context = null;
			App.logger = null;
		}
		catch (Exception ex)
		{
		}
		
		System.gc();
	}
	
	// ----------------------------------------------------------------------------------
	// set the global stop flag
	public static void setStopped(boolean value)
	{
		App.stopped = value;
	}
	
	// ----------------------------------------------------------------------------------
	// check the global stop flag (especially long running threads call that periodically)
	public static boolean isStopped()
	{
		return App.stopped;
	}
	

	// ----------------------------------------------------------------------------------
	private void initializeLogger(final KindletContext context)
	{
		App.logger = Logger.getLogger(App.class);
		//App.settings.getLoggingLevel()
		App.logger.setLevel(Level.DEBUG);

		if (App.logger.getLevel() != Level.OFF)
		{
			try
			{
				PatternLayout layout = new PatternLayout("%p - %m%n");
				FileAppender appender = new FileAppender(layout, context.getHomeDirectory() + "/appEvents.log");
				BasicConfigurator.configure(appender);
			}
			catch (java.io.IOException e)
			{
				e.printStackTrace();
			}
		}
		
		App.logInfo("Log initialized");
	}

	// ----------------------------------------------------------------------------------
	void doStartup() 
	{
		try
		{
			if (App.isStopped()) 
			{
				return; 
			}
			
			App.settings = new Settings();
		
			if (isStartingUp == true)
			{	
				App.settings.VerifyHomeFolder();
				App.controller = new Controller();
				App.isStartingUp = false;
			}					
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	// ----------------------------------------------------------------------------------
	public static boolean isBigScreen()
	{
		if (context.getRootContainer().getWidth() == 600)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	// ----------------------------------------------------------------------------------
	public static void logInfo(String message)
	{
		// TODO:  add timestamp
		try
		{
			logger.info(message);
		}
		catch (Exception e)
		{}
	}
	
	// ----------------------------------------------------------------------------------
	public static void logError(String message)
	{
		try
		{
			logger.error(message);
		}
		catch (Exception e)
		{}
	}

	// ----------------------------------------------------------------------------------
	public static String getAppVersion() 
	{
	    Enumeration resEnum;
	    
	    if (appVersion == null)
	    {
		    try 
		    {
		        resEnum = Thread.currentThread().getContextClassLoader().getResources(JarFile.MANIFEST_NAME);
		        
		        while (resEnum.hasMoreElements()) 
		        {
		            try
		            {
		                URL url = (URL)resEnum.nextElement();
		                InputStream is = url.openStream();
		                
		                if (is != null) 
		                {
		                    Manifest manifest = new Manifest(is);
		                    Attributes mainAttribs = manifest.getMainAttributes();
		                    String version = mainAttribs.getValue("Implementation-Version");
		                    
		                    if(version != null) 
		                    {
		                        appVersion = version;
		                    }
		                }
		            }
		            catch (Exception e) 
		            {
		                // Silently ignore wrong manifests on classpath?
		            }
		        }
		    } catch (IOException e1) 
		    {
		        // Silently ignore wrong manifests on classpath?
		    }
	    }
	    
	    return appVersion;
	}
	
}
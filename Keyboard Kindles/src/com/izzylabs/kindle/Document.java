/* 
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * Copyright (c) 2011, David Alexis. All rights reserved.
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */

package com.izzylabs.kindle;

import java.util.ArrayList;

public class Document
{
	public String		text;
	private int			caretPosition;
	private Line		currentLine;
	private int			currentLineNumber	= 0;
	private int			positionInLine		= 0;
	public ArrayList	paragraphs;
	public ArrayList	allLines;
	private int			firstVisibleLine	= 0;
	private int			visibleLineCount;
	private int			linePixelwidth;
	private String		breakChars			= " .,+:-;\n";

	public int[]		charWidths;
	private int			lastKnownLineNumber	= -1;
	private int			lineCount;

	/*
	 * ------------------------------------------------------------------------
	 * ------------------------------------------------------------------------
	 */
	public int getFirstVisibleLine()
	{
		return firstVisibleLine;
	}

	/*
	 * ------------------------------------------------------------------------
	 * ------------------------------------------------------------------------
	 */
	public void setFirstVisibleLine(int firstVisibleLine)
	{
		if (firstVisibleLine < 0)
		{
			firstVisibleLine = 0;
		}
		else if (firstVisibleLine >= this.lineCount)
		{
			if (this.lineCount == 0)
			{
				firstVisibleLine = 0;
			}
			else
			{
				firstVisibleLine = this.lineCount - 1;
			}
		}

		this.firstVisibleLine = firstVisibleLine;

		verifyVisibleLines();
	}

	/*
	 * ------------------------------------------------------------------------
	 * ------------------------------------------------------------------------
	 */
	public int getVisibleLineCount()
	{
		return visibleLineCount;
	}

	/*
	 * ------------------------------------------------------------------------
	 * ------------------------------------------------------------------------
	 */
	public void setVisibleLineCount(int visibleLineCount)
	{
		this.visibleLineCount = visibleLineCount;
		verifyVisibleLines();
	}

	/*
	 * ------------------------------------------------------------------------
	 * ------------------------------------------------------------------------
	 */
	public void setLineWidth(int value)
	{
		this.linePixelwidth = value;
	}

	// ------------------------------------------------------------------------
	// -- Document change Section
	// ------------------------------------------------------------------------
	public Document ()
	{
		this.text = "";
		this.allLines = new ArrayList();
		this.caretPosition = 0;
		this.positionInLine = 0;
		this.currentLineNumber = 0;
	}

	/*
	 * ------------------------------------------------------------------------
	 * ------------------------------------------------------------------------
	 */
	public void setText(String documentText)
	{
		this.text = documentText == null ? "" : documentText;
		this.paragraphs = new ArrayList();
		this.allLines = new ArrayList();
		reflow();
		this.caretPosition = 0;
		this.positionInLine = 0;
		this.currentLineNumber = 0;
		this.currentLine = getLine(0);
	}

	/*
	 * ------------------------------------------------------------------------
	 * Insert the text at the current position and update the caret
	 * position based on the length of the string.
	 * ------------------------------------------------------------------------
	 */
	public void insert(char value)
	{
		// Insert the text and update the current position
		this.text = this.text.substring(0, this.caretPosition) + value + this.text.substring(caretPosition);

		// Update the current position, if necessary
		this.caretPosition++;
		this.positionInLine++;
		reflow();
		verifyVisibleLines();
	}

	/*
	 * ------------------------------------------------------------------------
	 * Delete the character just before the current caret position.
	 * ------------------------------------------------------------------------
	 */
	public void deletePreviousCharacter()
	{
		if (this.caretPosition > 0)
		{
			this.text = this.text.substring(0, this.caretPosition - 1) + this.text.substring(this.caretPosition);
			this.caretPosition--;
			reflow();
			verifyVisibleLines();
		}
	}

	/*
	 * ------------------------------------------------------------------------
	 * Delete from the current position back to the next word-break
	 * character.
	 * ------------------------------------------------------------------------
	 */
	public void deletePreviousWord()
	{
		try
		{
			int selectionEnd = this.caretPosition;
			int selectionStart = selectionEnd - 1;
			
			while (this.breakChars.indexOf(this.text.charAt(selectionStart)) >= 0)
			{
				selectionStart--;
			}
			while (selectionStart > 0 && this.breakChars.indexOf(this.text.charAt(selectionStart - 1)) == -1)
			{
				selectionStart--;
			}
			
			// We got our word selected. Now delete it
			setCurrentPosition(selectionStart);
			this.text = this.text.substring(0, selectionStart) + this.text.substring(selectionEnd);
			reflow();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

	// ------------------------------------------------------------------------
	// -- Navigation Section
	// ------------------------------------------------------------------------

	// ------------------------------------------------------------------------
	// ------------------------------------------------------------------------
	public void moveToNextLine()
	{
		if (this.currentLineNumber <= this.lineCount)
		{
			setCurrentLine(this.currentLineNumber + 1);
		}

		verifyVisibleLines();
	}

	/*
	 * ------------------------------------------------------------------------ 
	 * Attempts to move to the next line, then returns the line number
	 * after the attempt (could be the same as the original).
	 * ------------------------------------------------------------------------
	 */
	public void moveToPreviousLine()
	{
		if (this.currentLineNumber > 0)
		{
			setCurrentLine(this.currentLineNumber - 1);
		}

		verifyVisibleLines();
	}

	// ------------------------------------------------------------------------
	// ------------------------------------------------------------------------
	public void moveToPreviousWord()
	{
		if (this.caretPosition > 0)
		{
			// Go backwards till we find the next break char
			this.caretPosition--;

			while (this.caretPosition > 0 && this.breakChars.indexOf(this.text.charAt(this.caretPosition)) >= 0)
				this.caretPosition--;

			while (this.caretPosition > 0 && this.breakChars.indexOf(this.text.charAt(this.caretPosition)) == -1)
			{
				this.caretPosition--;
			}

			if (this.breakChars.indexOf(this.text.charAt(this.caretPosition)) >= 0 && this.caretPosition < this.text.length())
				this.caretPosition++;

			setCurrentPosition(this.caretPosition);
		}
	}

	// ------------------------------------------------------------------------
	// ------------------------------------------------------------------------
	public void moveToNextWord()
	{
		int textLength = this.text.length();

		if (this.caretPosition < textLength)
		{
			// Go backwards till we find the next break char
			while (this.caretPosition < textLength && this.breakChars.indexOf(this.text.charAt(this.caretPosition)) == -1)
			{
				this.caretPosition++;
			}

			if (this.caretPosition < textLength && this.breakChars.indexOf(this.text.charAt(this.caretPosition)) >= 0)
				this.caretPosition++;

			setCurrentPosition(this.caretPosition);
		}
	}

	// ------------------------------------------------------------------------
	// ------------------------------------------------------------------------
	public void moveToBeginningOfLine()
	{
		if (this.caretPosition > this.currentLine.startPosition)
		{
			setCurrentPosition(this.currentLine.startPosition);
		}
	}

	/*
	 * ----------------------------------------------------------------------
	 * --
	 * --------------------------------------------------------------------
	 * ----
	 */
	public void moveToEndOfLine()
	{
		// if (this.caretPosition < this.currentLine.endPosition)
		// {
		setCurrentPosition(this.currentLine.endPosition);
		// }
	}

	// ------------------------------------------------------------------------
	// ------------------------------------------------------------------------
	public void moveLeft()
	{
		if (this.caretPosition > 0)
		{
			this.caretPosition--;
			this.positionInLine--;

			if (this.currentLineNumber > 0 && this.currentLine.startPosition > this.caretPosition)
			{
				// We went past the start of the current line.
				// Move caret to the last position of previous line.
				moveToPreviousLine();
				this.caretPosition = this.currentLine.endPosition;
			}

			this.positionInLine = this.caretPosition - this.currentLine.startPosition;

			verifyVisibleLines();
		}
	}

	// ------------------------------------------------------------------------
	// ------------------------------------------------------------------------
	public void moveRight()
	{
		if (this.caretPosition < this.text.length())
		{
			this.caretPosition++;
			this.positionInLine++;

			// Recalc paragraph/line position
			if (this.currentLineNumber < this.lineCount && this.caretPosition > this.currentLine.endPosition)
			{
				// We went past the end of the current line.
				// Move to start of next line.
				setCurrentLine(this.currentLineNumber + 1);
				this.caretPosition = this.currentLine.startPosition;
				this.positionInLine = 0;
			}
			else
			{
				this.positionInLine = this.caretPosition - this.currentLine.startPosition;
				verifyVisibleLines();
			}
		}
	}

	// ------------------------------------------------------------------------
	// ------------------------------------------------------------------------
	public void setCurrentLine(int lineNumber)
	{
		if (lineNumber >= 0 && lineNumber < lineCount)
		{
			this.currentLineNumber = lineNumber;
			this.currentLine = (Line) this.allLines.get(lineNumber);

			int lineLength = this.currentLine.length();

			if (lineLength < this.positionInLine)
				this.positionInLine = lineLength - 1;

			if (positionInLine < 0)
				positionInLine = 0;

			this.caretPosition = this.currentLine.startPosition + this.positionInLine;
		}
	}

	// ------------------------------------------------------------------------
	// ------------------------------------------------------------------------
	public void setCurrentPosition(int position)
	{
		if (position >= 0 && position <= this.text.length())
		{
			this.caretPosition = position;

			if (this.currentLine == null)
			{
				this.currentLineNumber = getLineForCharacterPosition(position);
				this.currentLine = getLine(this.currentLineNumber);
			}
			else if (position < this.currentLine.startPosition || position > this.currentLine.endPosition)
			{
				this.currentLineNumber = getLineForCharacterPosition(position);
				this.currentLine = getLine(this.currentLineNumber);
			}

			if (this.currentLine == null)
			{
				this.positionInLine = 0;
			}
			else
			{
				this.positionInLine = position - this.currentLine.startPosition;
				verifyVisibleLines();
			}
		}
	}

	// ------------------------------------------------------------------------
	// ------------------------------------------------------------------------
	public int getCurrentPosition()
	{
		return this.caretPosition;
	}

	/*
	 * ----------------------------------------------------------------------
	 * --
	 * --------------------------------------------------------------------
	 * ----
	 */
	public int getLength()
	{
		return this.text.length();
	}

	// ------------------------------------------------------------------------
	// ------------------------------------------------------------------------
	public int getLineCount()
	{
		return this.lineCount;
	}

	// ------------------------------------------------------------------------
	// ------------------------------------------------------------------------
	public int getCurrentLine()
	{
		return this.currentLineNumber;
	}

	// ------------------------------------------------------------------------
	// ------------------------------------------------------------------------
	public Line getLine(int lineNumber)
	{
		if (lineNumber >= 0 && lineNumber < lineCount)
		{
			return (Line) this.allLines.get(lineNumber);
		}
		else
		{
			return null;
		}
	}

	// ------------------------------------------------------------------------
	// ------------------------------------------------------------------------
	public int getLineForCharacterPosition(int position)
	{
		if (this.lineCount <= 1)
		{
			return 0;
		}
		else
		{
			// if (position >= this.text.length())
			// {
			// return this.lineCount - 1;
			// }
			// else
			// {
			for (int paragraphIndex = 0; paragraphIndex < this.paragraphs.size(); paragraphIndex++)
			{
				Paragraph paragraph = (Paragraph) this.paragraphs.get(paragraphIndex);

				if (position <= paragraph.endPosition)
				{
					// Found the paragraph containing the current line. Now
					// determine the line number
					for (int lineIndex = 0; lineIndex < paragraph.lines.size(); lineIndex++)
					{
						Line thisLine = (Line) paragraph.lines.get(lineIndex);

						if (position <= thisLine.endPosition)
						{
							return Document.this.allLines.indexOf(thisLine);
						}
					}
				}
			}
			// }
		}

		return -1;
	}

	// ------------------------------------------------------------------------
	// ------------------------------------------------------------------------
	public String getText()
	{
		return this.text.toString();
	}

	/*
	 * ----------------------------------------------------------------------
	 * --
	 * --------------------------------------------------------------------
	 * ----
	 */
	public void verifyVisibleLines()
	{
		if (this.currentLineNumber != this.lastKnownLineNumber || this.firstVisibleLine < 0)
		{
			int lastVisibleLine = this.firstVisibleLine + this.visibleLineCount;

			if (this.currentLineNumber > lastVisibleLine - 1 && lastVisibleLine <= this.lineCount)
			{
				// Move viewport down so that we can see the current line
				this.firstVisibleLine = this.currentLineNumber - this.visibleLineCount + 2;

				if (this.firstVisibleLine >= this.lineCount)
				{
					this.firstVisibleLine = this.lineCount - 2;
				}
			}
			else if (this.currentLineNumber < this.firstVisibleLine + 1 && this.firstVisibleLine > 0)
			{
				this.firstVisibleLine = this.currentLineNumber - 2;

				if (this.firstVisibleLine < 0)
				{
					this.firstVisibleLine = 0;
				}
			}

			this.lastKnownLineNumber = this.currentLineNumber;
		}
	}

	// ------------------------------------------------------------------------
	// -- Formatting Section
	// ------------------------------------------------------------------------

	/*
	 * ----------------------------------------------------------------------
	 * -- Attempts smart reflow by starting at the line before the current
	 * line, and reflowing only the current paragraph.
	 * ----------------------
	 * --------------------------------------------------
	 */
	// public void reflowFromLine(int fromLine)
	// {
	//
	// }

	/*
	 * ----------------------------------------------------------------------
	 * -- Reflows the entire document
	 * ----------------------------------------
	 * --------------------------------
	 */
	public void reflow()
	{
		// Break into paragraphs
		this.paragraphs = new ArrayList();
		this.allLines = new ArrayList();
		this.currentLine = null;

		if (this.text.length() > 0)
		{
			int start = 0;
			int documentEndPosition = this.text.length();

			// Scan text to break at paragraphs
			for (int position = 0; position <= documentEndPosition; position++)
			{
				char thisChar = position == documentEndPosition ? '\n' : this.text.charAt(position);

				if (thisChar == '\r' || thisChar == '\n' || position >= documentEndPosition)
				{
					// We got a paragraph
					Paragraph paragraph = new Paragraph(start, position);
					this.paragraphs.add(paragraph);
					paragraph.reflow();
					start = position;
				}
			}

			// Recalc line positions
			this.lineCount = this.allLines.size();
			this.currentLineNumber = getLineForCharacterPosition(this.caretPosition);
			this.currentLine = getLine(this.currentLineNumber);
			this.positionInLine = this.caretPosition - this.currentLine.startPosition;
			this.verifyVisibleLines();
		}
		else
		{
			this.positionInLine = 0;
			this.caretPosition = 0;
			this.currentLineNumber = 0;
			this.currentLine = null;
			this.lineCount = 0;
		}
	}

	/*
	 * ----------------------------------------------------------------------
	 * -- Reflows the specified paragraph
	 * ------------------------------------
	 * ------------------------------------
	 */
	public void reflowParagraph(int paragraphNumber)
	{
		if (paragraphNumber <= this.paragraphs.size())
		{
			((Paragraph) (this.paragraphs.get(paragraphNumber))).reflow();
		}
	}

	/*
	 * ----------------------------------------------------------------------
	 * --
	 * --------------------------------------------------------------------
	 * ----
	 */
	public void setPositionInLine(int positionInLine)
	{
		this.positionInLine = positionInLine;
	}

	/*
	 * ----------------------------------------------------------------------
	 * --
	 * --------------------------------------------------------------------
	 * ----
	 */
	public int getPositionInLine()
	{
		return positionInLine;
	}

	// --------------------------------------------------------------------------------------------
	// --------------------------------------------------------------------------------------------
	public class Paragraph
	{
		private ArrayList	lines;
		private int			startPosition, endPosition;

		/*
		 * ------------------------------------------------------------------
		 * ------
		 * ------------------------------------------------------------
		 * ------------
		 */
		public Paragraph ()
		{
		}

		/*
		 * ------------------------------------------------------------------
		 * ------
		 * ------------------------------------------------------------
		 * ------------
		 */
		public Paragraph ( int start, int end )
		{
			this.startPosition = start;
			this.endPosition = end;
		}

		/*
		 * ------------------------------------------------------------------
		 * ------
		 * ------------------------------------------------------------
		 * ------------
		 */
		public int length()
		{
			return this.endPosition - this.startPosition;
		}

		/*
		 * ------------------------------------------------------------------
		 * ------ Returns the number of lines in this paragraph, assuming it
		 * has already been reflowed.
		 * ----------------------------------------
		 * --------------------------------
		 */
		public int lineCount()
		{
			// Reflow paragragh, if necessary, and return the number of
			// lines
			if (this.lines == null)
			{
				reflow();
			}

			return this.lines.size();
		}

		/*
		 * ------------------------------------------------------------------
		 * ------ Reflows the current paragraph
		 * ------------------------------
		 * ------------------------------------------
		 */
		public void reflow()
		{
			this.lines = new ArrayList();

			int documentEndPosition = Document.this.text.length();

			if (this.endPosition < 0)
				this.endPosition = 0;

			while (this.startPosition <= this.endPosition && this.startPosition < documentEndPosition && (Document.this.text.charAt(this.startPosition) == '\n' || Document.this.text.charAt(this.startPosition) == '\r'))
				this.startPosition++;

			if (this.startPosition > this.endPosition)
				this.startPosition = this.endPosition;

			// Perform reflow only if the paragraph contains any characters
			if (this.endPosition >= this.startPosition)
			{
				int breakPosition = 0;
				int lineWidth = 0;
				int lineWidthAtBreak = 0;
				int lineStartPosition = this.startPosition;
				char currentChar = 0;
				int currentCharWidth = 0;
				
				if (Document.this.charWidths == null)
				{
					Document.this.charWidths =  App.settings.getFontWidths();
				}
				
				// Scan each char adding up pixel width until we're at the
				// line max.
				// If no break chars found, cut off at max length, else cut
				// off at last break position
				for (int charIndex = this.startPosition; charIndex <= this.endPosition && charIndex <= documentEndPosition; charIndex++)
				{
					if (charIndex < documentEndPosition)
					{
						currentChar = Document.this.text.charAt(charIndex);
						
						if (currentChar < 256 && currentChar >= 32)
							currentCharWidth = Document.this.charWidths[currentChar];
						else
							currentCharWidth = App.settings.getAverageCharWidth();

						lineWidth += currentCharWidth;

						if (Document.this.breakChars.indexOf(currentChar) >= 0)
						{
							breakPosition = charIndex;
							lineWidthAtBreak = lineWidth;
						}
					}

					if (lineWidth >= Document.this.linePixelwidth || charIndex == this.endPosition || charIndex == documentEndPosition)
					{
						// Check whether we must break here or we have a
						// break position to fall back to
						if (breakPosition == 0 || charIndex == this.endPosition)
						{
							addLineToCollection(new Line(lineStartPosition, charIndex));
							lineStartPosition = charIndex;
							lineWidth = currentCharWidth;
						}
						else
						{
							addLineToCollection(new Line(lineStartPosition, breakPosition));
							lineStartPosition = breakPosition + 1;
							lineWidth = lineWidth - lineWidthAtBreak;
							breakPosition = 0;
						}

						if (charIndex == this.endPosition)
							break;
					}
				}
			}
		}

		/*
		 * ------------------------------------------------------------------
		 * ------
		 * ------------------------------------------------------------
		 * ------------
		 */
		private void addLineToCollection(Line line)
		{
			this.lines.add(line);
			Document.this.allLines.add(line);
		}
	}

	// --------------------------------------------------------------------------------------------
	// --------------------------------------------------------------------------------------------
	public class Line
	{
		public int	startPosition;
		public int	endPosition;

		/*
		 * ------------------------------------------------------------------
		 * ------
		 * ------------------------------------------------------------
		 * ------------
		 */
		public Line ()
		{
		}

		/*
		 * ------------------------------------------------------------------
		 * ------
		 * ------------------------------------------------------------
		 * ------------
		 */
		public Line ( int start, int end )
		{
			this.startPosition = start;
			this.endPosition = end;
		}

		/*
		 * ------------------------------------------------------------------
		 * ------
		 * ------------------------------------------------------------
		 * ------------
		 */
		public int length()
		{
			return this.endPosition - this.startPosition;
		}

		/*
		 * ------------------------------------------------------------------
		 * ------
		 * ------------------------------------------------------------
		 * ------------
		 */
		public String toString()
		{
			if (this.startPosition < Document.this.text.length())
			{
				// if (this.endPosition + 1 >= Document.this.text.length())
				// return Document.this.text.substring(this.startPosition);
				// else
				return Document.this.text.substring(this.startPosition, this.endPosition);
			}
			else
				return "";
		}
	}

}

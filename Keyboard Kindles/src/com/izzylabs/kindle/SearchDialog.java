package com.izzylabs.kindle;


import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Container;
import java.util.ResourceBundle;

import com.amazon.kindle.customdialog.CustomDialog;
import com.amazon.kindle.kindlet.ui.KButton;
import com.amazon.kindle.kindlet.ui.KLabel;
import com.amazon.kindle.kindlet.ui.KLabelMultiline;
import com.amazon.kindle.kindlet.ui.KTextField;
import com.amazon.kindle.kindlet.ui.KindletUIResources;
import com.amazon.kindle.kindlet.ui.KindletUIResources.KFontFamilyName;
import com.amazon.kindle.kindlet.ui.KindletUIResources.KFontStyle;


/**
 * Sample Dialog used to show how to override the custom dialog.
 */
public class SearchDialog extends CustomDialog 
{
	protected KTextField		textField;
//	protected ButtonGroup		optionGroup;
//	protected KRadioButton		exactOption;
//	protected JRadioButton		allOption;
//	protected JRadioButton		anyOption;
	//protected final Font		font				= KindletUIResources.getInstance().getFont(KindletUIResources.KFontFamilyName.SANS_SERIF, 24, KFontStyle.PLAIN);
	protected ActionListener    okEventHandler;
	protected ActionListener    cancelEventHandler;
 
    /**
     * Private constructor for the sample dialog. Invoke using the static method. 
     * @param root the active content's root container
     */
    private SearchDialog(final Container root) {        
        super(root);                                                                    
    }

	// ----------------------------------------------------------------------------------
	private SearchDialog(final Container root, ActionListener okEvent, ActionListener cancelEvent) 
	{        
        super(root);      
        
        this.okEventHandler = okEvent;
        this.cancelEventHandler = cancelEvent;
    }
	
    /** Creates and adds all components. */
    protected void initDialog() 
    {        
        super.initDialog();
        
        this.setLayout(new GridBagLayout());
        
        final Font		font				= KindletUIResources.getInstance().getFont(KindletUIResources.KFontFamilyName.SANS_SERIF, 24, KFontStyle.PLAIN);
        
        final KLabel headerLabel = new KLabel("Find ...");
        headerLabel.setFont(KindletUIResources.getInstance().getFont(KFontFamilyName.SANS_SERIF, 26, KFontStyle.BOLD));
        
        final GridBagConstraints headerConstraints = new GridBagConstraints();
        headerConstraints.fill = GridBagConstraints.HORIZONTAL;
        headerConstraints.gridx = 0;
        headerConstraints.gridy = 0;
        headerConstraints.gridwidth =  3;
        headerConstraints.insets = new Insets(10, 20, 10, 20);
        headerConstraints.ipadx = 10;
        headerConstraints.ipady = 0;        
        headerConstraints.weightx = 0.5;        
        this.add(headerLabel, headerConstraints);
    	
		// Add search term textbox
		this.textField = new KTextField()
		{
			private static final long	serialVersionUID	= 1L;

			public Insets getInsets()
			{
				return new Insets(2, 5, 2, 5);
			}
		};
		
		this.textField.setFont(font);
		
		final GridBagConstraints textConstraints = new GridBagConstraints();
		textConstraints.fill = GridBagConstraints.HORIZONTAL;
		textConstraints.gridx = 0;
		textConstraints.gridy = 1;
		textConstraints.gridwidth = 3;        
		textConstraints.insets = new Insets(10, 20, 10, 20);
		textConstraints.weighty = 0.3;        
		add(this.textField, textConstraints);
        
        
        // Add search instructions
		
//		exactOption = new JRadioButton("Exact Match", true);
//		allOption = new JRadioButton("All Words", false);
//		anyOption = new JRadioButton("Any words", false);
//		exactOption.setActionCommand("0");
//		allOption.setActionCommand("1");
//		anyOption.setActionCommand("2");
//		exactOption.setHorizontalTextPosition(SwingConstants.RIGHT);
//		allOption.setHorizontalTextPosition(SwingConstants.RIGHT);
//		anyOption.setHorizontalTextPosition(SwingConstants.RIGHT);
//		optionGroup = new ButtonGroup();
//		optionGroup.add(exactOption);
//		optionGroup.add(allOption);
//		optionGroup.add(anyOption);
//		
//		final Box optionContainer = Box.createVerticalBox();
//		optionContainer.setAlignmentX(Component.RIGHT_ALIGNMENT);
//		optionContainer.add(exactOption);
//		optionContainer.add(allOption);
//		optionContainer.add(anyOption);
		
//		final GridBagConstraints optionConstraints = new GridBagConstraints();
//		optionConstraints.fill = GridBagConstraints.HORIZONTAL;
//		optionConstraints.gridx = 0;
//		optionConstraints.gridy = 2;
//		optionConstraints.gridwidth = 3;        
//		optionConstraints.insets = new Insets(0, 20, 0, 20);
//		optionConstraints.weighty = 0.5;        
//		this.add(optionContainer, optionConstraints);

		final SearchDialog thisDialog = this;		 
		final KButton cancelButton = new KButton("Cancel");
		cancelButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				thisDialog.hideDialog();
				thisDialog.remove();
				thisDialog.cancelEventHandler.actionPerformed(e);
			}
		});
		
		final GridBagConstraints cancelButtonConstraints = new GridBagConstraints();
		cancelButtonConstraints.fill = GridBagConstraints.HORIZONTAL;
		cancelButtonConstraints.gridx = 0;
		cancelButtonConstraints.gridy = 3;
		cancelButtonConstraints.gridwidth = 1;        
		cancelButtonConstraints.insets = new Insets(10, 20, 20, 30);
		cancelButtonConstraints.weighty = 0.4;        
		cancelButtonConstraints.weightx = 0.5;
		add(cancelButton, cancelButtonConstraints);
		
		// Add the Search button
		final KButton searchButton = new KButton("Find");
		searchButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				thisDialog.hideDialog();
				
				int selectedSearchOption = 0; //Integer.parseInt(optionGroup.getSelection().getActionCommand());
				ActionEvent resultEvent = new ActionEvent(SearchDialog.this, selectedSearchOption, textField.getText());
				
                // Since a new Dialog is created each time, we will call remove on the
                // dialog to remove it from the root container.
                thisDialog.remove();
                thisDialog.okEventHandler.actionPerformed(resultEvent);
			}
		});
		
		final GridBagConstraints searchButtonConstraints = new GridBagConstraints();
		searchButtonConstraints.fill = GridBagConstraints.HORIZONTAL;
		searchButtonConstraints.gridx = 2;
		searchButtonConstraints.gridy = 3;
		searchButtonConstraints.gridwidth = 1;        
		searchButtonConstraints.insets = new Insets(10, 30, 20, 20);
		searchButtonConstraints.weighty = 0.4;
		searchButtonConstraints.weightx = 0.5;
		add(searchButton, searchButtonConstraints);
		
		this.setFocusDefault(this.textField); 
        
        
        
        
        
        
//        
//    	// Load the resource from the resource bundle    
//    	final String header = "TEST HEADER";
//    	final String purchaseText = "Purchase text";
//    	final String cancelText = "Cancel";
//    	final String message =  "THis is my message";
//    	final Dimension dialogDimensions = new Dimension();
////    			.getObject(DialogBundle.DIALOG_DIMENSION));
////    	final int textAlignment = ((Integer) ResourceBundle.getBundle(DialogBundle.PROPERTIES)
////    			.getObject(DialogBundle.TEXT_ALIGNMENT)).intValue();
//    	
//      if (App.isBigScreen())
//      {
//    	  dialogDimensions.height = 450;
//    	  dialogDimensions.width = 550;
//      }
//      else
//      {
//    	  dialogDimensions.height = 330;
//    	  dialogDimensions.width = 500;
//      }
//    	this.setSize(dialogDimensions);    
//                               
//        final KLabel headerLabel = new KLabel(header);
//        final KLabelMultiline msgLabel = new KLabelMultiline(message);
//        final KButton confirm = new KButton(purchaseText);
//        final KButton cancel = new KButton(cancelText);
//        
//        // Reset the font for the header label
//        headerLabel.setFont(KindletUIResources.getInstance().getFont(KFontFamilyName.SANS_SERIF, 26, KFontStyle.BOLD, true));
//        this.setLayout(new GridBagLayout());
//                
//        // Create constraints and add the header label
//        final GridBagConstraints headerConstraints = new GridBagConstraints();
//        headerConstraints.fill = GridBagConstraints.HORIZONTAL;
//        headerConstraints.gridx = 0;
//        headerConstraints.gridy = 0;
//        headerConstraints.gridwidth =  2;
//        headerConstraints.insets = new Insets(15,30,10,30);
//        headerConstraints.ipadx = 10;
//        headerConstraints.ipady = 8;        
//        headerConstraints.weightx = 0.8;        
//        this.add(headerLabel, headerConstraints);
//
//        // Create constraints and add the message label
//        final GridBagConstraints msgConstraints = new GridBagConstraints();
//        msgConstraints.fill = GridBagConstraints.VERTICAL;
//        msgConstraints.gridx = 0;
//        msgConstraints.gridy = 1;
//        msgConstraints.gridwidth = 2;        
//        msgConstraints.insets = new Insets(10,30,10,30);
//        msgConstraints.weighty = 1.0;        
//        this.add(msgLabel, msgConstraints);
//                     
//        // Create constraints and add the cancel button
//        final GridBagConstraints cancelConstraints = new GridBagConstraints();
//        cancelConstraints.anchor = GridBagConstraints.SOUTHEAST;
//        cancelConstraints.gridx = 0;
//        cancelConstraints.gridy = 2;
//        cancelConstraints.insets = new Insets(0,180,8,0);
//        cancelConstraints.weighty = 0.4;        
//        this.add(cancel, cancelConstraints);
//        
//        // Create constraints and add the confirmation button
//        final GridBagConstraints confirmConstraints = new GridBagConstraints();
//        confirmConstraints.anchor = GridBagConstraints.SOUTHEAST;
//        confirmConstraints.gridx = 1;
//        confirmConstraints.gridy = 2;
//        confirmConstraints.insets = new Insets(0,0,8,18);
//        confirmConstraints.weighty = 0.4;                
//        this.add(confirm, confirmConstraints);
//        
//        final SearchDialog thisDialog = this;
//               
//        // Create the action listener for the buttons
//        final ActionListener buttonListener = new ActionListener() {
//        	public void actionPerformed(final ActionEvent event) {
//        		thisDialog.hideDialog();
//
//        		ActionEvent resultEvent =  new ActionEvent(SearchDialog.this, 1, "") ;// new ActionEvent(SearchDialog.this, null); //selectedSearchOption, textField.getText());
//        		
//        		// Since a new Dialog is created each time, we will call remove on the
//                // dialog to remove it from the root container.
//        		 thisDialog.remove();
//                 thisDialog.okEventHandler.actionPerformed(resultEvent);
//        	}
//        };
//        
//        // The button will actually do the same thing
//        confirm.addActionListener(buttonListener);        		        
//        cancel.addActionListener(buttonListener);
        
//        this.setFocusDefault(confirm);               
    }
        
    /**
     * Static method to show a sample dialog. 
     * @param root The active content's root container
     */        
    public static void show(final Container root)  {        
        final SearchDialog thisDialog = new SearchDialog(root);
        thisDialog.showDialog();
    }       
    // ----------------------------------------------------------------------------------
    public static void show(final Container root, ActionListener okEvent, ActionListener cancelEvent)  
    {        
        final SearchDialog thisDialog = new SearchDialog(root, okEvent, cancelEvent);
        thisDialog.showDialog();
        
        Dimension dialogSize = thisDialog.getPreferredSize();
        dialogSize.width = root.getWidth() - 100;  
        thisDialog.setSize(dialogSize);
    }
}
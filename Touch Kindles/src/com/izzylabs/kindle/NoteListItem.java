/*
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * Copyright (c) 2012, David Alexis. All rights reserved.
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */

package com.izzylabs.kindle;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.border.AbstractBorder;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import com.amazon.kindle.kindlet.event.GestureDispatcher;
import com.amazon.kindle.kindlet.input.Gestures;
import com.amazon.kindle.kindlet.ui.KindletUIResources;
import com.amazon.kindle.kindlet.ui.KindletUIResources.KColorName;

// -------------------------------------------------------------------------------------
final class NoteListItem extends JPanel
{
	private static final long	serialVersionUID	= 1L;
	private JLabel				nameLabel;
	private JLabel				createdLabel;
	private JLabel				modifiedLabel;
	String				noteId;
	private JPanel				displayPanel;
	private JPopupMenu			popup;
	private ActionListener		deleteHandler;
	private ActionListener		saveAsHandler;
	
	// ----------------------------------------------------------------------------------
	NoteListItem(String id, String name, String created,	String modified)
	{
		this.noteId = id;

		// Build UI of the note display
		setLayout(new BorderLayout(10, 10));
		setFocusable(false);
		
		displayPanel = new JPanel(new BorderLayout(5, 5));

		nameLabel = new JLabel(name)
		{
			private static final long	serialVersionUID	= 1L;

			public Insets getInsets()
			{
				return new Insets(5, 30, 5, 20);
			}
		};
		nameLabel.setFont(App.settings.getFont(Settings.NotebookItemTitleFont));
		createdLabel = new JLabel(created);
		modifiedLabel = new JLabel(modified);

		createdLabel.setFont(App.settings.getFont(Settings.BasicFont));
		modifiedLabel.setFont(App.settings.getFont(Settings.BasicFont));

		displayPanel.add(nameLabel, BorderLayout.NORTH);
		displayPanel.add(createdLabel, BorderLayout.WEST);
		displayPanel.add(modifiedLabel, BorderLayout.EAST);
		this.add(displayPanel, BorderLayout.NORTH);
		
		//Dimension mySize = new Dimension(Settings.ScreenWidth - 40, 90);
//		this.setSize(mySize);
//		this.setMaximumSize(mySize);
		
		final GestureDispatcher gestureDispatcher = new GestureDispatcher();
		this.addMouseListener(gestureDispatcher);
		ActionMap gestures = this.getActionMap();

		gestures.put(Gestures.ACTION_TAP, new AbstractAction()
		{
			private static final long	serialVersionUID	= 1L;

			public void actionPerformed(ActionEvent e)
			{
				if (App.controller.modalDialogOpen == false)
				{
					String noteId = NoteListItem.this.noteId;
					App.controller.getEventHandler().actionPerformed(new ActionEvent(NoteListItem.this, App.EDITORVIEW, noteId));
				}
			}
		});

		gestures.put(Gestures.ACTION_HOLD, new AbstractAction()
		{
			private static final long	serialVersionUID	= 1L;

			public void actionPerformed(ActionEvent e)
			{
				if (App.controller.modalDialogOpen == false)
				{
					popup.show(NoteListItem.this, 50, 0);
				}
			}
		});

		popup = new JPopupMenu()
		{
			private static final long	serialVersionUID	= 1L;

			public Insets getInsets()
			{
				return new Insets(40, 10, 10, 10);
			}
		};

	    TitledBorder labelBorder = BorderFactory.createTitledBorder(
	            null, "Note Actions",
	            TitledBorder.CENTER, TitledBorder.ABOVE_TOP, 
	            App.settings.getFont(Settings.NotebookItemTitleFont), 
	            KindletUIResources.getInstance().getColor(KColorName.BLACK));
	   
	    
	    Border outerBorder = new AbstractBorder()
		{
			private static final long	serialVersionUID	= 1L;
			
			public void paintBorder(Component c, Graphics g, int x, int y, int w, int h)
			{
				g.setColor(KindletUIResources.getInstance().getColor(KColorName.DARK_GRAY));
				Graphics2D g2d = (Graphics2D) g;
				g2d.setStroke(new BasicStroke(4, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
				g2d.drawRoundRect(2, 2, w - 4, h - 4, 30, 30);
			}
		};
		
		popup.setBorder(BorderFactory.createCompoundBorder(outerBorder, labelBorder));
		popup.add(new JSeparator());
		
		addMenuItem(popup, "Delete this note", "images/delete.png", new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				deleteHandler.actionPerformed(new ActionEvent(NoteListItem.this, 1, NoteListItem.this.noteId));
			}
		});

		addMenuItem(popup, "Make a copy of this note", "images/copy.png", new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				saveAsHandler.actionPerformed(new ActionEvent(NoteListItem.this, 2, NoteListItem.this.noteId));
			}
		});
	}

//	// ----------------------------------------------------------------------------------
//	private void addAllGestures(final ActionMap actionMap, final Action action) 
//	{
//        actionMap.put(Gestures.ACTION_TAP, action);
//        actionMap.put(Gestures.ACTION_DOUBLE_TAP, action);
//        actionMap.put(Gestures.ACTION_HOLD, action);
//        actionMap.put(Gestures.ACTION_FLICK_NORTH, action);
//        actionMap.put(Gestures.ACTION_FLICK_WEST, action);
//        actionMap.put(Gestures.ACTION_FLICK_SOUTH, action);
//        actionMap.put(Gestures.ACTION_FLICK_EAST, action);
//        actionMap.put(Gestures.ACTION_GROW, action);
//        actionMap.put(Gestures.ACTION_SHRINK, action);
//    }
	
	// ----------------------------------------------------------------------------------
	public Insets getInsets()
	{
		return new Insets(10, 10, 10, 10);
	}
	
	// ----------------------------------------------------------------------------------
	public void paint(final Graphics g)
	{
		super.paint(g);

		g.setColor(KindletUIResources.getInstance().getColor(KColorName.LIGHT_GRAY));

		// Draw a border at the bottom
		int width = getWidth();
		int height = getHeight();

		Graphics2D g2d = (Graphics2D) g;
		g2d.setStroke(new BasicStroke(1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
		g2d.drawLine(0, height - 2, width, height - 2);
	}
	
//	// ----------------------------------------------------------------------------------
//	public void setText(String name, String created, String modified)
//	{
//		this.nameLabel.setText(name);
//		this.createdLabel.setText(created);
//		this.modifiedLabel.setText(modified);
//		repaint();
//	}

	// ----------------------------------------------------------------------------------
	private void addMenuItem(JPopupMenu menu, String text, String iconFileName, ActionListener handler)
	{
		JMenuItem menuItem = new JMenuItem(text)
		{
			private static final long	serialVersionUID	= 1L;

			public Insets getInsets()
			{
				return new Insets(10, 10, 10, 10);
			}
		};

		ImageIcon icon = new ImageIcon(getClass().getResource(iconFileName));
		menuItem.setIcon(icon);
		menuItem.addActionListener(handler);
		menu.add(menuItem);
	}

	// ----------------------------------------------------------------------------------
	void addDeleteHandler(ActionListener noteDeleteHandler)
	{
		this.deleteHandler = noteDeleteHandler;
	}

	// ----------------------------------------------------------------------------------
	void addSaveAsHandler(ActionListener noteSaveAsHandler)
	{
		this.saveAsHandler = noteSaveAsHandler;
	}
}
/*
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * Copyright (c) 2012, David Alexis. All rights reserved.
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */

package com.izzylabs.kindle;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Comparator;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.border.AbstractBorder;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.amazon.kindle.kindlet.event.GestureDispatcher;
import com.amazon.kindle.kindlet.input.Gestures;
import com.amazon.kindle.kindlet.ui.KMenu;
import com.amazon.kindle.kindlet.ui.KMenuItem;
import com.amazon.kindle.kindlet.ui.KOptionPane;
import com.amazon.kindle.kindlet.ui.KPagedContainer;
import com.amazon.kindle.kindlet.ui.KPages;
import com.amazon.kindle.kindlet.ui.KindletUIResources;
import com.amazon.kindle.kindlet.ui.KindletUIResources.KColorName;
import com.amazon.kindle.kindlet.ui.pages.PageProviders;
import edu.emory.mathcs.backport.java.util.Collections;

final class NotebookList implements IViewable
{
	private Container view;
	
	// ----------------------------------------------------------------------------------
	public Container getView()
	{
		return this.view;
	}
	
	// ----------------------------------------------------------------------------------
	public void setView(Container view)
	{
		this.view = view;
	}
	
	// ----------------------------------------------------------------------------------
	public String getViewName()
	{
		return "notebooklist";
	}
	
	// ----------------------------------------------------------------------------------
	public int getViewId()
	{
		return App.NOTEBOOKLISTVIEW;
	}

	// ----------------------------------------------------------------------------------
	public NotebookList()
	{
		this.view = new NotebookListView();
	}
	
	// ----------------------------------------------------------------------------------
	private class NotebookListView extends JPanel
	{
		private static final long	serialVersionUID	= 1L;
		
		private 	KPages			pages;
		private 	FooterPanel		footerPanel;
		private 	KMenu			menu;
		private 	int				currentPageStart;
		private 	int				currentPage;
		private 	int				pageCount;
		private 	int				itemCount;
		private 	boolean			trackPageChanges;
		private 	int				pageSize;
		
		// ------------------------------------------------------------------------------
		public NotebookListView()
		{
			this.setLayout(new BorderLayout(0, 0));			
			this.add(createButtonBar(), BorderLayout.NORTH);
			
			this.pages = new KPages(PageProviders.createBoxLayoutProvider(BoxLayout.Y_AXIS))
			{
				private static final long	serialVersionUID	= 1L;

				public Insets getInsets()
				{
					return new Insets(10, 10, 10, 10);
				}
			};
			this.pages.setFirstPageAlignment(BorderLayout.NORTH);
			this.pages.setFocusable(false);
			this.pages.setPagingPolicy(KPagedContainer.PAGING_POLICY_LOCAL);
			this.add(this.pages, BorderLayout.CENTER);
			this.pageSize = 0;
			this.footerPanel = new FooterPanel();
			this.add(footerPanel, BorderLayout.SOUTH);

			this.addComponentListener();
			this.addPropertyListener();
			
			this.createMenu();
			
			final GestureDispatcher gestureDispatcher = new GestureDispatcher();
			this.addMouseListener(gestureDispatcher);
			final ActionMap gestures = this.pages.getActionMap();

			if (gestures.get(Gestures.ACTION_FLICK_WEST) == null)
			{
				AbstractAction nextPageAction = new AbstractAction()
				{
					private static final long	serialVersionUID	= 1L;

					public void actionPerformed(ActionEvent e)
					{
						if (App.controller.modalDialogOpen == false)
							pages.next();
					}
				};

				gestures.put(Gestures.ACTION_FLICK_WEST, nextPageAction);
			}

			if (gestures.get(Gestures.ACTION_FLICK_EAST) == null)
			{
				AbstractAction previousPageAction = new AbstractAction()
				{
					private static final long	serialVersionUID	= 1L;

					public void actionPerformed(ActionEvent e)
					{
						if (App.controller.modalDialogOpen == false)
						{
							pages.previous();
							currentPageStart = pages.getPageStartLocation();
	
							if (currentPageStart < 0)
								currentPageStart = 0;
						}
					}
				};

				gestures.put(Gestures.ACTION_FLICK_EAST, previousPageAction);
			}
		}
		
		// ------------------------------------------------------------------------------
		private Component createButtonBar()
		{
			final ButtonBar bar = new ButtonBar("Notebooks");
			
			bar.addButton("New Notebook", "images/newNB.png", App.EDITORVIEW, null, new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					String notebookTitle = KOptionPane.showInputDialog(NotebookListView.this, "What is the name for this Notebook?", "New Notebook", KOptionPane.PLAIN_MESSAGE, KOptionPane.CANCEL_SAVE_OPTIONS, null);
		
					if (notebookTitle != null && notebookTitle.trim().length() != 0)
					{
						notebookTitle = notebookTitle.trim();
						
						if (notebookTitle.length() != 0)
						{
							String newNotebookId = FileUtilities.sanitize(notebookTitle).toLowerCase();
							Notebook newNotebook = new Notebook(newNotebookId);
							newNotebook.name = notebookTitle;
							newNotebook.save();
							newNotebook = null;
							displayItems();
						}
					}
					
//					simplePromptDialog.showDialog("New Notebook", "What is the name for this Notebook?", "", 30, "", new ActionListener()
//					{
//						public void actionPerformed(ActionEvent e)
//						{
//							try
//							{
//								String notebookTitle = e.getActionCommand();
//								
//								if (notebookTitle != null && notebookTitle.trim().length() != 0)
//								{
//									notebookTitle = notebookTitle.trim();
//									
//									if (notebookTitle.length() != 0)
//									{
//										String newNotebookId = FileUtilities.sanitize(notebookTitle).toLowerCase();
//										Notebook newNotebook = new Notebook(newNotebookId);
//										newNotebook.name = notebookTitle;
//										newNotebook.save();
//										newNotebook = null;
//										displayItems();
//									}
//								}					
//							}
//							finally
//							{
//								App.controller.modalDialogOpen = false;
//							}
//						}
//					});					
				}
			});
			
			// Add close button
			bar.addButton("close", "images/close.png", -1, "close", new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					App.controller.getEventHandler().actionPerformed(new ActionEvent(NotebookListView.this, -1, "close"));
				}
			});
			
			return bar;
		}
		
		// ------------------------------------------------------------------------------
		private void addComponentListener()
		{
			addComponentListener(new ComponentListener()
			{
				public void componentShown(ComponentEvent arg0)
				{
					displayItems();
					setMenu(true);
					footerPanel.updateStatus(currentPage, pageCount, itemCount);

					validate();
					repaint();
				}

				public void componentResized(ComponentEvent arg0)
				{
					calculatePages();
					footerPanel.updateStatus(currentPage, pageCount, itemCount);
					validate();
					repaint();
				}

				public void componentMoved(ComponentEvent arg0)
				{}

				public void componentHidden(ComponentEvent arg0)
				{
					pages.setPagingPolicy(KPagedContainer.PAGING_POLICY_DISABLED);
					setMenu(false);
				}
			});
		}
		
		// ------------------------------------------------------------------------------
		private void addPropertyListener()
		{
			pages.addPropertyChangeListener(new PropertyChangeListener()
			{
				public void propertyChange(PropertyChangeEvent evt)
				{
					if (evt.getPropertyName().compareTo("pageEndLocation") == 0 && trackPageChanges == true)
					{
						try
						{
							calculatePages();
							footerPanel.updateStatus(currentPage, pageCount, itemCount);

							int firstLocation = pages.getPageStartLocation();
							currentPageStart = firstLocation;

							if (!(currentPageStart != 0 && currentPageStart < pages.getPageModel().getLastLocation()))
							{
								currentPageStart = pages.getPageStartLocation();
							}

							if (NotebookListView.this.itemCount <= NotebookListView.this.currentPageStart)
								NotebookListView.this.currentPageStart = NotebookListView.this.itemCount - 1;
						}
						catch (Exception ex)
						{
							App.logError("Property page change error: " + ex.getMessage());
						}
					}
				}
			});
		}
		
		// ----------------------------------------------------------------------------------
		private void calculatePages()
		{
			try
			{
				if (pages.getHeight() <= 0)
					return;

				// Gather known information: item count, location range of
				// current page
				int lastLocation = pages.getPageModel().getLastLocation();
				itemCount = lastLocation + 1;
				int pageEndLocation = pages.getPageEndLocation();
				int pageStartLocation = pages.getPageStartLocation();

				if (pageEndLocation == Integer.MAX_VALUE)
					pageEndLocation = lastLocation;
				else
					pageEndLocation -= 1;

				if (pageStartLocation == Integer.MIN_VALUE)
					pageStartLocation = 0;

				// Calculate page size from given information
				int pageSize = pageEndLocation - pageStartLocation + 1;

				// Compare with saved page location and use whichever is larger
				if (pageSize > this.pageSize)
				{
					this.pageSize = pageSize;
				}
				else
				{
					pageSize = this.pageSize;
				}

				// Based on this data, figure out how many pages there are and
				// what page we're on
				if (this.pageSize == 0 || itemCount == 0)
					pageCount = 1;
				else
					pageCount = ((int) (itemCount / this.pageSize)) + ((itemCount % pageSize) > 0 ? 1 : 0);

				if (lastLocation <= 0)
					currentPage = 1;
				else
					currentPage = (int) ((pageStartLocation + 1) / pageSize) + 1;
			}
			catch (Exception ex)
			{
				App.logError("Error! " + ex.getMessage() + ex.getStackTrace());
			}
		}
		
		// ------------------------------------------------------------------------------
		private void displayItems()
		{

			try
			{
				pages.setPagingPolicy(KPagedContainer.PAGING_POLICY_LOCAL);

				ArrayList notebooks = getNotebookList();
				this.pages.removeAllItems();
				trackPageChanges = false;

				for (int index = 0; index < notebooks.size(); index++)
				{
					NotebookMetadata thisNotebook = (NotebookMetadata) notebooks.get(index);				
					NotebookListItemView note = new NotebookListItemView(thisNotebook.id, thisNotebook.name, thisNotebook.noteCount);

					pages.addItem(note);
				}

				itemCount = pages.getPageModel().getLastLocation();

				if (itemCount == Integer.MAX_VALUE)
					itemCount = 0;
				else
					itemCount += 1;

				// Display the footer with page data
				this.validate();
				trackPageChanges = true;
				calculatePages();
				footerPanel.updateStatus(currentPage, pageCount, itemCount);

				this.currentPageStart = 0;

				if (this.currentPageStart != 0 && this.currentPageStart < this.pages.getPageModel().getLastLocation())
				{
					this.pages.setLocation(this.currentPageStart);
				}
				else
					this.pages.setLocation(this.pages.getPageStartLocation());
			}
			catch (Error e)
			{
				App.logError(e.getMessage() + e.getStackTrace());
			}
			finally
			{
				trackPageChanges = true;
			}
		}
		
		// ------------------------------------------------------------------------------
		private ArrayList getNotebookList()
		{
			ArrayList notebooks = new ArrayList();
			String notebooksPath = System.getProperty("kindlet.home") + "/notebooks/";
			
			// Get list of JSON notebook indexes
			File notebooksDirectory = new File(notebooksPath);
			final FilenameFilter fileFilter = new FilenameFilter() 
			{
				public boolean accept(File dir, String name) 
				{
				  	return name.endsWith(".json");
			  	}
			};
			    
			String[] notebookIndexFiles = notebooksDirectory.list(fileFilter);
			
			// Read each index file to get notebook name, id and note count
			for (int fileIndex = 0; fileIndex < notebookIndexFiles.length; fileIndex++)
			{
				String fileName = notebooksPath + "/" + notebookIndexFiles[fileIndex];
				String jsonContent = Notebook.readNotebookIndexFile(fileName);
				
				if (jsonContent != null)
				{
					JSONObject notebookData = Notebook.parseNotebookIndexJSON(jsonContent);
					JSONArray notes = (JSONArray) notebookData.get("notes");
					String name = notebookData.get("name").toString();
					String id = notebookData.get("id").toString();
					int noteCount = notes == null ? 0 : notes.size();
					
					NotebookMetadata notebookEntryData = new NotebookMetadata(name, id, noteCount);
					notebooks.add(notebookEntryData);
				}
			}

			Collections.sort(notebooks, new byName());
			return notebooks;
		}

		// ----------------------------------------------------------------------------------
		private void createMenu()
		{
			try
			{
				this.menu = new KMenu();
				final KMenuItem helpMenuItem = new KMenuItem("TakeNote Help");
				helpMenuItem.setActionCommand("");
				helpMenuItem.addActionListener(new ActionListener()
				{
					 public void actionPerformed(ActionEvent e)
					 {
						 App.controller.getEventHandler().actionPerformed(new ActionEvent(NotebookListView.this, App.HELPVIEW, null));
					 }
				});
					
				menu.add(helpMenuItem);

			}
			catch (Exception ex)
			{
				App.logError(ex.getMessage() + ex.getStackTrace());
			}
		}
		
		// ----------------------------------------------------------------------------------
		private void setMenu(boolean enable)
		{
			try
			{
				if (Thread.interrupted() == false && App.context != null && this.menu != null)
				{
					if (enable == true)
						App.context.setMenu(this.menu);
					else
						App.context.setMenu(null);
				}
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
			}
		}
		
		// ----------------------------------------------------------------------------------
		private void onRenameDialogClose(final String notebookId, final String oldNotebookName, final String newNotebookName)
		{
			try
			{
				if (newNotebookName != null && newNotebookName.equals(oldNotebookName) == false)
				{		
					//TODO:  Validate that the new name does not already exist.
					Notebook currentNotebook = App.controller.getCurrentNotebook();
					
					if (currentNotebook.id.equals(notebookId))
					{
						App.controller.setCurrentNotebook(null);
					}
					
					String notebooksPath = System.getProperty("kindlet.home") + "/notebooks/";
					String indexFilePath = notebooksPath + notebookId + ".json";
					String noteFolder = notebooksPath + notebookId + "/";
					String newNotebookId = FileUtilities.sanitize(newNotebookName).toLowerCase();
					String newIndexFilePath = notebooksPath + newNotebookId + ".json";
					
					Notebook notebook = new Notebook(notebookId);
					notebook.filePath = newIndexFilePath;
					notebook.id = newNotebookId;
					notebook.name = newNotebookName;
					notebook.save();
					notebook = null;
					
					File noteDirectory = new File(noteFolder);
					
					// Rename the notes folder
					if (noteDirectory.exists())
					{
						noteDirectory.renameTo(new File(notebooksPath + newNotebookId));
						noteDirectory = null;
					}
					
					// Rename the notebook index file
					File newNotebookFile = new File(newIndexFilePath);
					File notebookFile = new File(indexFilePath);
					
					if (newNotebookFile.exists())
					{
						// The new index file was already created.  Just delete the old one.
						notebookFile.delete();
					}
					else
					{
						// Rename the old file to the new name
						notebookFile.renameTo(new File(newIndexFilePath));
					}
	
					// Reload
					NotebookListView.this.displayItems();
				}
			}
			finally
			{
				App.controller.modalDialogOpen = false;
			}
		}
		
		// ----------------------------------------------------------------------------------
		private class NotebookListItemView extends JPanel
		{
			private static final long	serialVersionUID	= 1L;
			private String 		notebookId;
			private JPopupMenu	popup;
			private String 		notebookName;
			private JPanel		displayPanel;

			
			private NotebookListItemView(String id, String name, int noteCount)
			{
				this.notebookId = id;
				this.notebookName = name;
				
				setLayout(new BorderLayout(10, 10));
				setFocusable(false);
				
				displayPanel = new JPanel(new BorderLayout(5, 5));
				this.add(displayPanel, BorderLayout.NORTH);
				
				final JLabel nameLabel = new JLabel(name)
				{
					private static final long	serialVersionUID	= 1L;

					public Insets getInsets()
					{
						return new Insets(5, 30, 5, 20);
					}
				};
				nameLabel.setFont(App.settings.getFont(Settings.NotebookItemTitleFont));
				final String countMessage = Integer.toString(noteCount) + (noteCount == 1 ? " note" : " notes");
				final JLabel noteCountLabel = new JLabel(countMessage)
				{
					private static final long	serialVersionUID	= 1L;

					public Insets getInsets()
					{
						return new Insets(5, 60, 10, 0);
					}
				};
				noteCountLabel.setFont(App.settings.getFont(Settings.BasicFont));

				displayPanel.add(nameLabel, BorderLayout.NORTH);
				displayPanel.add(noteCountLabel, BorderLayout.CENTER);
				
				final GestureDispatcher gestureDispatcher = new GestureDispatcher();
				this.addMouseListener(gestureDispatcher);
				ActionMap gestures = this.getActionMap();

				gestures.put(Gestures.ACTION_TAP, new AbstractAction()
				{
					private static final long	serialVersionUID	= 1L;

					public void actionPerformed(ActionEvent e)
					{
						if (App.controller.modalDialogOpen == false)
						{
							String notebookId = NotebookListItemView.this.notebookId;
							App.controller.setCurrentNotebook(null);
							App.controller.getEventHandler().actionPerformed(new ActionEvent(NotebookListItemView.this, App.NOTEBOOKVIEW, notebookId));
						}
					}
				});

				gestures.put(Gestures.ACTION_HOLD, new AbstractAction()
				{
					private static final long	serialVersionUID	= 1L;

					public void actionPerformed(ActionEvent arg0)
					{
						if (App.controller.modalDialogOpen == false)
						{
							popup.show(NotebookListItemView.this, 50, 0);
						}
					}
				});

				popup = new JPopupMenu()
				{
					private static final long	serialVersionUID	= 1L;

					public Insets getInsets()
					{
						return new Insets(40, 10, 10, 10);
					}
				};
				
				TitledBorder labelBorder = BorderFactory.createTitledBorder(
			            null, "Notebook Actions",
			            TitledBorder.CENTER, TitledBorder.ABOVE_TOP, 
			            App.settings.getFont(Settings.NotebookItemTitleFont), 
			            KindletUIResources.getInstance().getColor(KColorName.BLACK));
			   
			    
			    Border outerBorder = new AbstractBorder()
				{
					private static final long	serialVersionUID	= 1L;
					
					public void paintBorder(Component c, Graphics g, int x, int y, int w, int h)
					{
						g.setColor(KindletUIResources.getInstance().getColor(KColorName.DARK_GRAY));
						Graphics2D g2d = (Graphics2D) g;
						g2d.setStroke(new BasicStroke(4, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
						g2d.drawRoundRect(2, 2, w - 4, h - 4, 30, 30);
					}
				};
				
				popup.setBorder(BorderFactory.createCompoundBorder(outerBorder, labelBorder));
				popup.add(new JSeparator());
				
				final JMenuItem deleteMenuItem = new JMenuItem("Delete this notebook")
				{
					private static final long	serialVersionUID	= 1L;

					public Insets getInsets()
					{
						return new Insets(10, 10, 10, 10);
					}
				};

				final ImageIcon deleteIcon = new ImageIcon(getClass().getResource("images/delete.png"));
				deleteMenuItem.setIcon(deleteIcon);
				deleteMenuItem.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						Notebook currentNotebook = App.controller.getCurrentNotebook();
						final int choice = KOptionPane.showConfirmDialog(NotebookListItemView.this, "The \"" + NotebookListItemView.this.notebookName + "\" notebook and all its notes will be permanently deleted.  Are you sure you want to do this?", "Delete This Notebook", KOptionPane.NO_YES_OPTIONS);

						if (choice == KOptionPane.YES_OPTION)
						{		
							if (currentNotebook.id.equals(NotebookListItemView.this.notebookId))
							{
								App.controller.setCurrentNotebook(null);
							}
							
							// Delete notebook and all notes
							Notebook.delete(NotebookListItemView.this.notebookId);

							// Reload
							NotebookListView.this.displayItems();
						}
					}
				});
				
				final JMenuItem renameMenuItem = new JMenuItem("Rename this notebook")
				{
					private static final long	serialVersionUID	= 1L;

					public Insets getInsets()
					{
						return new Insets(10, 10, 10, 10);
					}
				};

				final ImageIcon renameIcon = new ImageIcon(getClass().getResource("images/renameNB.png"));
				renameMenuItem.setIcon(renameIcon);
				renameMenuItem.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						String notebookName = KOptionPane.showInputDialog(NotebookListView.this, "What is the new name for this Notebook?", "Rename Notebook", KOptionPane.PLAIN_MESSAGE, KOptionPane.CANCEL_SAVE_OPTIONS, NotebookListItemView.this.notebookName);
						onRenameDialogClose(NotebookListItemView.this.notebookId, NotebookListItemView.this.notebookName, notebookName);
					}
				});
				
				popup.add(deleteMenuItem);
				popup.add(renameMenuItem);
			}
			
			// ----------------------------------------------------------------------------------
			public Insets getInsets()
			{
				return new Insets(10, 20, 10, 20);
			}
			
			// ----------------------------------------------------------------------------------
			public void paint(final Graphics g)
			{
				super.paint(g);

				g.setColor(KindletUIResources.getInstance().getColor(KColorName.LIGHT_GRAY));

				// Draw a border at the bottom
				int width = getWidth();
				int height = getHeight();

				Graphics2D g2d = (Graphics2D) g;
				g2d.setStroke(new BasicStroke(1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
				g2d.drawLine(0, height - 2, width, height - 2);
			}
		}
	}

	// ----------------------------------------------------------------------------------
	private class byName implements Comparator
	{		
		public byName()
		{
			super();
		}
		
		public int compare(Object entry1, Object entry2)
		{
			int result = ((NotebookMetadata)entry1).name.toLowerCase().compareTo(((NotebookMetadata) entry2).name.toLowerCase());
			
			return result;
		}

	}
		
	//----------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------
	private class FooterPanel extends JPanel
	{
		private static final long	serialVersionUID	= -4911638306174881473L;
		private JLabel				notebookNameLabel;
		private JLabel				countLabel;

		// ------------------------------------------------------------------------------
		public FooterPanel()
		{
			try
			{
				this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
				this.setFocusable(false);
				Font footerFont = App.settings.getFont(Settings.BasicFont);
				this.notebookNameLabel = new JLabel();
				this.notebookNameLabel.setFont(footerFont);
				this.countLabel = new JLabel();
				this.countLabel.setFont(footerFont);
				this.setBackground(KindletUIResources.getInstance().getColor(KColorName.GRAY_02));
				this.add(this.notebookNameLabel);
				this.add(Box.createHorizontalGlue());
				this.add(this.countLabel);
			}
			catch (Exception ex)
			{
				App.logError(ex.getMessage());
			}
		}

		// -----------------------------------------------------------------------------
		private void updateStatus(int pageNumber, int pageCount, int itemCount)
		{
			String pageStatus = "page " + pageNumber + " of " + pageCount;
			notebookNameLabel.setText(pageStatus);

			if (itemCount == 1)
				countLabel.setText("1 notebook");
			else
				countLabel.setText(itemCount + " notebooks");

			validate();
			repaint();
		}

		// ----------------------------------------------------------------------------------
		public Insets getInsets()
		{
			return new Insets(10, 10, 5, 10);
		}

		// ----------------------------------------------------------------------------------
		public void paint(final Graphics g)
		{
			super.paint(g);

			// Draw a border around the panel
			int width = getWidth();
			g.setColor(KindletUIResources.getInstance().getColor(KColorName.GRAY_05));
			g.drawLine(0, 0, width, 0);
		}

	}
}

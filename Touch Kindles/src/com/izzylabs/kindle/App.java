/*
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * Copyright (c) 2012, David Alexis. All rights reserved.
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */

package com.izzylabs.kindle;

import javax.swing.SwingUtilities;

import org.apache.log4j.*;

import com.amazon.kindle.kindlet.*;

public class App extends AbstractKindlet
{
	public static KindletContext		context;
	public static boolean				isStartingUp;
	public static boolean				isFirstRun;
	public static Settings				settings;
	public static Controller			controller;
	
	static final int					NOVIEW				= 0;
	static final int					DASHBOARDVIEW		= 1;
	static final int					NOTEBOOKLISTVIEW	= 2;
	static final int					NOTEBOOKVIEW		= 3;
	static final int					EDITORVIEW			= 4;
	static final int					SETTINGSVIEW		= 5;
	static final int					HELPVIEW			= 6;
	static final int					SEARCHVIEW			= 7;
	static final int					PREVIOUSVIEW		= -1;
	
	private static boolean 				stopped				= false;
	private static String 				appVersion			= null;
	private static Logger				logger;

	
	// ----------------------------------------------------------------------------------
	public void create(final KindletContext context)
	{
		try
		{
			App.isStartingUp = true;
			App.context = context;
			initializeLogger(context);					
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

	// ----------------------------------------------------------------------------------
	public void start()
	{
		synchronized (this)
		{
			App.setStopped(false);
			super.start();

			try
			{
				if (isStartingUp)
				{
					SwingUtilities.invokeLater( new Runnable() 
					{
					    public void run() 
					    { 
					    	App.this.doStartup();
					    }
					});				
				}
				else
				{
					App.controller.startMemoryChecker();
				}
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
			}
		}
	}

	// ----------------------------------------------------------------------------------
	public void stop()
	{
		try
		{			
			synchronized (this)
			{
				App.setStopped(true);
				App.settings.save();
				App.controller.stopMemoryChecker();
			}
		}
		catch (Exception e)
		{
		}
	}

	// ----------------------------------------------------------------------------------
	public void destroy()
	{
		try
		{
			App.settings.save();	
			App.controller.shutdown();	
			App.context.getRootContainer().setLayout(null);
			
			App.settings = null;
			App.context = null;
			App.logger = null;
		}
		catch (Exception ex)
		{
		}
		
		System.gc();
	}
	
	// ----------------------------------------------------------------------------------
	// set the global stop flag
	private static void setStopped(boolean value)
	{
		App.stopped = value;
	}
	
	// ----------------------------------------------------------------------------------
	// check the global stop flag (especially long running threads call that periodically)
	private static boolean isStopped()
	{
		return App.stopped;
	}
	

	// ----------------------------------------------------------------------------------
	private void initializeLogger(final KindletContext context)
	{
		App.logger = Logger.getLogger(App.class);
		//App.settings.getLoggingLevel()
		App.logger.setLevel(Level.DEBUG);

		if (App.logger.getLevel() != Level.OFF)
		{
			try
			{
				PatternLayout layout = new PatternLayout("%p - %m%n");
				FileAppender appender = new FileAppender(layout, context.getHomeDirectory() + "/appEvents.log");
				BasicConfigurator.configure(appender);
			}
			catch (java.io.IOException e)
			{
				e.printStackTrace();
			}
		}
	}

	// ----------------------------------------------------------------------------------
	private void doStartup() 
	{
		try
		{
			if (App.isStopped()) 
			{
				return; 
			}
			
			App.settings = new Settings();
				
			if (isStartingUp == true)
			{			
				App.settings.VerifyHomeFolder();
				App.controller = new Controller();
				App.isStartingUp = false;
			}					
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	// ----------------------------------------------------------------------------------
	static boolean isBigScreen()
	{
		checkDeviceSettings();
		
		if (App.settings.ScreenWidth < 780)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	// ----------------------------------------------------------------------------------
	static void logInfo(String message)
	{
		// TODO:  add timestamp
		logger.info(message);
	}
	
	// ----------------------------------------------------------------------------------
	static void logError(String message)
	{
		logger.error(message);
	}

	// ----------------------------------------------------------------------------------
	public static String getAppVersion() 
	{
//	    Enumeration resEnum;
	    
	    if (appVersion == null)
	    {
//		    try 
//		    {
//		        resEnum = Thread.currentThread().getContextClassLoader().getResources(JarFile.MANIFEST_NAME);
//		        
//	            try
//	            {
//	                URL url = (URL)resEnum.nextElement();
//	                InputStream is = url.openStream();
//	                
//	                if (is != null) 
//	                {
//	                    Manifest manifest = new Manifest(is);
//	                    Attributes mainAttribs = manifest.getMainAttributes();
//
//	                    while (resEnum.hasMoreElements()) 
//	    		        {
//		                    String version = mainAttribs.getValue("Implementation-Version");
//		                    
//		                    if (version != null) 
//		                    {
//		                        appVersion = version;
//		                        break;
//		                    }
//	    		        }
//	                }
//	            }
//	            catch (Exception e) 
//	            {
//	                // Silently ignore wrong manifests on classpath?
//	            }
//		    } catch (IOException e1) 
//		    {
//		        // Silently ignore wrong manifests on classpath?
//		    }
	    	appVersion = "1.7.1";
	    }

	    return appVersion;
	}

	// ----------------------------------------------------------------------------------
	public static void checkDeviceSettings()
	{
		if (App.settings.ScreenHeight == 0)
		{
			switch(App.context.getRootContainer().getWidth())
			{
				case 600:
					// Kindle Touch
					App.settings.ScreenHeight = 800;
					App.settings.ScreenWidth = 600;
					App.settings.KeyboardHeight = 275; 
					break;
					
				case 758:
					// Kindle PaperWhite
					App.settings.ScreenHeight = 899;
					App.settings.ScreenWidth = 758;
//					App.settings.KeyboardHeight = 352; 
					App.settings.KeyboardHeight = 275; 
					break;
			}
		}
	}
}
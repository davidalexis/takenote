///*
// * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// * Copyright (c) 2012, David Alexis. All rights reserved.
// * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// */
//
//package com.izzylabs.kindle;
//
//import java.awt.event.ActionEvent;
//import java.awt.event.ActionListener;
////TODO:  Create final style/skin
//import java.awt.BasicStroke;
//import java.awt.BorderLayout;
//import java.awt.Container;
//import java.awt.FlowLayout;
//import java.awt.Graphics;
//import java.awt.Graphics2D;
//import java.awt.Insets;
//
//import javax.swing.AbstractAction;
//import javax.swing.Action;
//import javax.swing.ActionMap;
//import javax.swing.JLabel;
//import javax.swing.JPanel;
//
//import com.amazon.kindle.kindlet.event.GestureDispatcher;
//import com.amazon.kindle.kindlet.event.GestureEvent;
//import com.amazon.kindle.kindlet.input.Gestures;
//import com.amazon.kindle.kindlet.ui.KindletUIResources;
//import com.amazon.kindle.kindlet.ui.KindletUIResources.KColorName;
//
//public class Dashboard implements IViewable
//{
//	private Container view;
//	
//	// ----------------------------------------------------------------------------------
//	public Dashboard()
//	{
//		// Set up the view and subscribe to events
//		this.view = new DashboardView(new ActionListener()
//		{
//			public void actionPerformed(ActionEvent e)
//			{
//				// Get the ID of the dashboard item that was tapped
//				// Tell the application context to show the selected view.
//				show(e.getID(), e.getActionCommand());
//			}
//
//		});
//	}
//
//	// ----------------------------------------------------------------------------------
//	public void show(int id, String parameter)
//	{
////		int viewId = 1;
//		
////		switch (id)
////		{
////			case App.NotebookView:
////				// parameter contains notebook ID
////				//viewName = "notebook";
////				viewId = App.NotebookView;
////				// Set the current notebook ID to
////				break;
////				
////			case App.NotebookListView:
////				// parameter contains notebook ID
////				viewName = "notebooks";
////				break;
////			
////			case App.EditorView:
////				// parameter contains <notebook id>::<note id> identifying the last edited note
////				viewName = "editor";
////				break;
////				
////			case App.SettingsView:
////				// parameter is meaningless
//////				viewName = "settings";
////				break;
////				
////			case App.HelpView:
//////				viewName = "help";
////				viewId = App.HelpView;
////				break;
////		}
//		
//		// Initialize the view if it has not been loaded yet
//		
//		// Show the view
////		App.controller.RequestViewByName(viewName);
//		App.controller.RequestViewById(id, parameter);
//	}
//
//	// ----------------------------------------------------------------------------------
//	public Container getView()
//	{
//		return this.view;
//	}
//	
//	// ----------------------------------------------------------------------------------
//	public String getViewName()
//	{
//		return "dashboard";
//	}
//	
//	// ----------------------------------------------------------------------------------
//	public int getViewId()
//	{
//		return App.DashoardView;
//	}
//	
//	// ----------------------------------------------------------------------------------
//	public void setView(Container view)
//	{
//		this.view = view;
//	}
//
//}
//
//class DashboardView extends JPanel
//{
//
//	private static final long	serialVersionUID	= 1L;
//	private ActionListener 		eventSubscriber;
//	
//	// ----------------------------------------------------------------------------------
//	public DashboardView(ActionListener subscriber)
//	{
//		this.eventSubscriber = subscriber;
//		
//		this.setLayout(new BorderLayout());
//		this.add(new TitlePanel("TakeNote", true, null), BorderLayout.NORTH);
//		Container itemContainer = new Container();
//		this.add(itemContainer, BorderLayout.CENTER);
//		
//		itemContainer.setLayout(new FlowLayout(FlowLayout.LEFT));
//		
//		itemContainer.add(new DashboardItemView(Integer.toString(App.NotebookListView), "Notebooks", "Select from a list of your Notebooks or create a new one."));
//		itemContainer.add(new DashboardItemView(Integer.toString(App.NotebookView), "Current Notebook", "Open the current (or last used) notebook."));
//		// this.add(new DashboardItemView("0", "All Notes", "Show all notes, regardless of Notebook.")); // TODO
//		//this.add(new DashboardItemView(Integer.toString(App), "Search", "Find notes matching your criteria."));
//		itemContainer.add(new DashboardItemView(Integer.toString(App.SettingsView), "Settings", "View and change application options."));
//		itemContainer.add(new DashboardItemView(Integer.toString(App.HelpView), "Help", "Get help on using TakeNote, find out about \nIzzyLabs, and contact us."));
//	}
//	
//	
//	// ----------------------------------------------------------------------------------
//	class DashboardItemView extends JPanel
//	{
//
//		private static final long	serialVersionUID	= 1L;
//		private JLabel				titleLabel;
//		private JLabel				descriptionLabel;
//		public String				itemId;
//		private JPanel				displayPanel;
//
//		// ----------------------------------------------------------------------------------
//		public DashboardItemView(String itemId, String title, String description)
//		{
//			this.itemId = itemId;
//
//			setLayout(new BorderLayout(10, 10));
//			setFocusable(false);
//
//			displayPanel = new JPanel(new BorderLayout(5, 5));
//
//			titleLabel = new JLabel(title);
//			titleLabel.setFont(App.settings.getFont(Settings.LargeBoldFont));
//			
//			descriptionLabel = new JLabel(description);
//			descriptionLabel.setFont(App.settings.getFont(Settings.BasicFont));
//
//			displayPanel.add(titleLabel, BorderLayout.NORTH);
//			displayPanel.add(descriptionLabel, BorderLayout.CENTER);
//			this.add(displayPanel, BorderLayout.CENTER);
//
//			final GestureDispatcher gestureDispatcher = new GestureDispatcher();
//	        final Action itemAction = new ItemGestureEventAction();
//
//	        this.addMouseListener(gestureDispatcher);
//	        ActionMap gestures = this.getActionMap();
//	        
//	        gestures.put(Gestures.ACTION_TAP, itemAction);
////	        gestures.put(Gestures.ACTION_DOUBLE_TAP, itemAction);
//
//		}
//
//		// ----------------------------------------------------------------------------------
//		public Insets getInsets()
//		{
//			return new Insets(10, 10, 10, 10);
//		}
//
//		// ----------------------------------------------------------------------------------
//		public void paint(final Graphics g)
//		{
//			super.paint(g);
//			
//			g.setColor(KindletUIResources.getInstance().getColor(KColorName.GRAY_10));
//
//			int width = getWidth();
//			int height = getHeight();
//
//			Graphics2D g2d = (Graphics2D) g;
//			g2d.setStroke(new BasicStroke(6, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
//			g2d.drawRoundRect(2, 2, width - 4, height - 4, height / 3, height / 3);
//		}
//
//		//----------------------------------------------------------------------------------
//		class ItemGestureEventAction extends AbstractAction 
//		{
//			private static final long	serialVersionUID	= 1L;
//
//			public void actionPerformed(final ActionEvent e)
//		    {
//		        if (e instanceof GestureEvent) 
//		        {
//		            DashboardItemView.this.setBackground(KindletUIResources.getInstance().getColor(KColorName.GRAY_10));
//		            DashboardItemView.this.repaint();
//		            
//		            // Notify the Controller of the user's selection
//		            eventSubscriber.actionPerformed(new ActionEvent(DashboardView.this, Integer.parseInt(((DashboardItemView)(e.getSource())).itemId), ""));
//		        }
//		    }
//		}
//	}
//}
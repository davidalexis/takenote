/*
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * Copyright (c) 2012, David Alexis. All rights reserved.
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */

package com.izzylabs.kindle;

import java.io.*;
import java.text.*;
import java.util.Date;

final class Note
{
	private String		id;
	private String		name;
	private String		text;
	private Date		created;
	private Date		modified;
	private int 		firstVisibleLine;
	private int 		lastCaretPosition;
	private boolean		isDirty;
	private Notebook	myNotebook;
	
// TODO Remove unused code found by UCDetector
// 	public DateFormat		sortDateFormatter	= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	// ----------------------------------------------------------------------------------
	Note(Notebook notebook)
	{
		this.myNotebook = notebook;
	}
	
	// ----------------------------------------------------------------------------------
	public String getName()
	{
		return this.name;
	}

	// ----------------------------------------------------------------------------------
	public void setName(String value)
	{
		this.name = value;
	}

	// ----------------------------------------------------------------------------------
	public String getText()
	{
		return this.text;
	}

	// ----------------------------------------------------------------------------------
	public void setText(String value)
	{
		this.text = value;
	}

	// ----------------------------------------------------------------------------------
	public Date getDateCreated()
	{
		return new Date();
	}

	// ----------------------------------------------------------------------------------
	public Date getDateModified()
	{
		return new Date();
	}

	// ----------------------------------------------------------------------------------
	public void setCreated(Date created)
	{
		this.created = created;
	}

	// ----------------------------------------------------------------------------------
	public Date getCreated()
	{
		return created;
	}

	// ----------------------------------------------------------------------------------
	public void setModified(Date modified)
	{
		this.modified = modified;
	}

	// ----------------------------------------------------------------------------------
	public Date getModified()
	{
		return modified;
	}

	// ----------------------------------------------------------------------------------
	public void setId(String id)
	{
		this.id = id;
	}

	// ----------------------------------------------------------------------------------
	public String getId()
	{
		return id;
	}

	// ----------------------------------------------------------------------------------
	public void setLastCaretPosition(int value)
	{
		this.lastCaretPosition = value;
	}
	
	// ----------------------------------------------------------------------------------
	public int getLastCaretPosition()
	{
		return this.lastCaretPosition;
	}
	
	// ----------------------------------------------------------------------------------
	public void setFirstVisibleLine(int value)
	{
		this.firstVisibleLine = value;
	}
	
	// ----------------------------------------------------------------------------------
	public int getFirstVisibleLine()
	{
		return this.firstVisibleLine;
	}
	
	// ----------------------------------------------------------------------------------
	public void setIsDirty(boolean dirtyFlag)
	{
		this.isDirty = dirtyFlag;
	}
	
	// ----------------------------------------------------------------------------------
	public boolean getIsDirty()
	{
		return this.isDirty;
	}

	// ----------------------------------------------------------------------------------
	void save()
	{
		Date now = new Date();
		// Save note file
		
		// Are we creating a new note?
		if (this.id == null)
		{		
			// Generate a new ID
			Format newIdFormatter = new SimpleDateFormat("yyyyMMddHHmmss");
			this.id  = newIdFormatter.format(now);
		}

		if (this.created == null)
		{
			this.created = now;
		}
		
		this.modified = now;
		
		// The notebook's note files should be in a sub-folder with a name
		// matching the notebook ID
		saveNoteFile();
		// Tell Notebook to update its list
		this.myNotebook.updateNoteEntry(this);
		this.isDirty = false;
	}

	// ----------------------------------------------------------------------------------
	private void saveNoteFile()
	{
		App.settings.VerifyHomeFolder();
		
		File noteFile = getNoteFile();

		if (noteFile != null)
		{
			FileOutputStream outputStream = null;

			try
			{
				// Save the file
				byte[] noteBytes = this.text.getBytes();
				outputStream = new FileOutputStream(noteFile);
				outputStream.write(noteBytes);
			}
			catch (FileNotFoundException e)
			{
				e.printStackTrace();
				App.logError(e.getMessage());
			}
			catch (IOException e)
			{
				e.printStackTrace();
				App.logError(e.getMessage());
			}
			catch (Exception e)
			{
				App.logError("Error saving note. " + e.getMessage());
			}
			finally
			{
				try
				{
					outputStream.close();
				}
				catch (IOException e)
				{
					e.printStackTrace();
					App.logError("Failed to close note file.  " + e.getMessage());
				}
			}
		}
	}

	// ----------------------------------------------------------------------------------
	void delete()
	{
		this.myNotebook.removeNote(id);

		File noteFile = getNoteFile();
		
		if (noteFile != null)
		{
			noteFile.delete();
		}
	}	

	// ----------------------------------------------------------------------------------
	private File getNoteFile()
	{
		String filePath = this.myNotebook.filePath;
		int sep = filePath.lastIndexOf("/");
		String notesDirName = filePath.substring(0, sep) + "/" + this.myNotebook.id;
		File notesDir = new File(notesDirName);

		// Check if the notes directory exists
		if (!notesDir.exists())
		{
			notesDir.mkdir();
		}
		
		// Look for a file with the name "note<noteid>"
		String noteFileName = "note" + this.id;
		File noteFile = new File(notesDirName, noteFileName);
		
		return noteFile;
	}
}

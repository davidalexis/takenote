/*
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * Copyright (c) 2012, David Alexis. All rights reserved.
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */

package com.izzylabs.kindle;

import java.io.*;

final class FileUtilities
{

	static final String	ILLEGAL_CHARACTERS	= "/\n\r\t\0\f`?*\\<>|\":";

	// ----------------------------------------------------------------------------------
	static String convertStreamToString(InputStream is)
			throws IOException
	{
		/*
		 * To convert the InputStream to String we use the Reader.read(char[]
		 * buffer) method. We iterate until the Reader return -1 which means
		 * there's no more data to read. We use the StringWriter class to
		 * produce the string.
		 */
		if (is != null)
		{
			Writer writer = new StringWriter();

			char[] buffer = new char[1024];
			try
			{
				Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
				int n;
				while ((n = reader.read(buffer)) != -1)
				{
					writer.write(buffer, 0, n);
				}
			}
			finally
			{
				is.close();
			}

			return writer.toString();
		}
		else
		{
			return "";
		}
	}

	// ----------------------------------------------------------------------------------
	static void writeFile(String fileName, String content)
	{
		FileOutputStream outputStream = null;

		// The notebook's note files should be in a sub-folder with a name
		// matching the notebook ID
		File fileToWrite = new File(fileName);

		// Save the file
		byte[] fileBytes = content.getBytes();

		try
		{
			outputStream = new FileOutputStream(fileToWrite);
			outputStream.write(fileBytes);
		}
		catch (FileNotFoundException e1)
		{
			App.logError(e1.getMessage());
		}
		catch (IOException e)
		{
			App.logError(e.getMessage());
		}
		finally
		{
			try
			{
				outputStream.close();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}

	// ----------------------------------------------------------------------------------
// TODO Remove unused code found by UCDetector
// 	public static boolean isCharValidInFileName(char thisChar)
// 	{
// 		return ILLEGAL_CHARACTERS.indexOf(thisChar) < 0;
// 	}

	// ----------------------------------------------------------------------------------
	static String readFile(String filePath)
	{
		File noteFile = new File(filePath);
		FileInputStream inputStream = null;
		String noteText = null;
		
		if (noteFile.exists())
		{
			try
			{
				byte[] fileBytes = new byte[(int) noteFile.length()];
				inputStream = new FileInputStream(noteFile);
				inputStream.read(fileBytes);

				// Convert byte array to String
				noteText = new String(fileBytes);

				fileBytes = null;
			}
			catch (FileNotFoundException e)
			{
				e.printStackTrace();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
			finally
			{
				try
				{
					inputStream.close();
				}
				catch (IOException e)
				{
				}
			}
		}
		
		return noteText;
	}
	
	// ----------------------------------------------------------------------------------
	static String sanitize(String name) 
	{
		final String invalidChars = ":\\/*?|<>$";
		
		for (int i = 0; i < invalidChars.length(); i++)
		{
			if (name.indexOf(invalidChars.charAt(i)) >= 0)
			{
				name = name.replace(invalidChars.charAt(i), '_');
			}
		}
		
	    return name;
	}
	
//	public static String readNote(String notePath)
//	{
//		StringBuffer fileContents = new StringBuffer();
//		File file = new File(notePath);
//		FileInputStream fileStream = null;
//		BufferedReader reader = null;
//
//		try
//		{
//			fileStream = new FileInputStream(file);
//
//			// Here BufferedInputStream is added for fast reading.
//			reader = new BufferedReader(new InputStreamReader(fileStream));
//			String line = null;
//
//			while ((line = reader.readLine()) != null)
//			{
//				fileContents.append(line + '\n');
//			}
//		}
//		catch (FileNotFoundException e)
//		{
//			e.printStackTrace();
//		}
//		catch (IOException e)
//		{
//			e.printStackTrace();
//		}
//		finally
//		{
//			// dispose all the resources after using them.
//			try
//			{
//				reader.close();
//				fileStream.close();
//			}
//			catch (IOException e)
//			{
//				e.printStackTrace();
//			}
//		}
//		
//		return fileContents.toString();
//	}
}
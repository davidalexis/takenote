/*
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * Copyright (c) 2012, David Alexis. All rights reserved.
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */

package com.izzylabs.kindle;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.Stack;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.undo.*;

import com.amazon.kindle.kindlet.event.GestureDispatcher;
import com.amazon.kindle.kindlet.input.Gestures;
import com.amazon.kindle.kindlet.input.keyboard.OnscreenKeyboardEvent;
import com.amazon.kindle.kindlet.input.keyboard.OnscreenKeyboardListener;
import com.amazon.kindle.kindlet.input.keyboard.OnscreenKeyboardProperties;
import com.amazon.kindle.kindlet.input.keyboard.OnscreenKeyboardUtil;
import com.amazon.kindle.kindlet.ui.KMenu;
import com.amazon.kindle.kindlet.ui.KMenuItem;
import com.amazon.kindle.kindlet.ui.KOptionPane;
import com.amazon.kindle.kindlet.ui.KindletUIResources;
import com.amazon.kindle.kindlet.ui.KindletUIResources.KColorName;

final class NoteEditor implements IViewable
{
	private NoteEditorView	view;
	private Note			note;

	// ----------------------------------------------------------------------------------
	public Container getView()
	{
		return this.view;
	}

	// ----------------------------------------------------------------------------------
	public void setView(Container view)
	{
		this.view = (NoteEditorView) view;
	}

	// ----------------------------------------------------------------------------------
	public String getViewName()
	{
		return "editor";
	}

	// ----------------------------------------------------------------------------------
	public int getViewId()
	{
		return App.EDITORVIEW;
	}

	// ----------------------------------------------------------------------------------
	NoteEditor(String noteId)
	{
		this.view = new NoteEditorView();
		this.view.isLoading = true;

		// Get note data and load it into the view
		if (noteId != null)
		{
			this.note = App.controller.getCurrentNotebook().getNote(noteId);

			String noteText = note.getText();
			this.view.titleField.setText(note.getName());
			this.view.contentField.setText(noteText == null ? "" : noteText);

			if (noteId != null && noteId.compareTo("autosave") == 0)
			{
				this.view.setIsDirty(note.getIsDirty());
				this.view.contentField.setCaretPosition(note.getLastCaretPosition());		
			}
			else
			{
				this.view.setIsDirty(false);
				this.view.titleField.setCaretPosition(0);
				this.view.contentField.setCaretPosition(0);
			}
		}
		else
		{
			this.note = new Note(App.controller.getCurrentNotebook());
			this.view.titleField.setText("");
			this.view.contentField.setText("");
			this.view.setIsDirty(false);
			this.view.titleField.setCaretPosition(0);
			this.view.contentField.setCaretPosition(0);
		}

		this.view.isLoading = false;
	}

	// ----------------------------------------------------------------------------------
	void saveState()
	{
		if (this.note == null || this.note.getId() == null)
		{
			this.note = new Note(App.controller.getCurrentNotebook());
			this.note.setId("autosave");
		}
		else
		{
			this.note.setId("autosave-" + this.note.getId());
		}

		this.note.setName(this.view.titleField.getText());
		this.note.setText(this.view.contentField.getText());
		this.note.setIsDirty(this.view.isDirty);
		this.note.setLastCaretPosition(this.view.contentField.getCaretPosition());
		this.note.save();

		App.settings.setHasSavedState(true);
	}

	// ----------------------------------------------------------------------------------
	private class NoteEditorView extends JPanel
	{
		private static final long	serialVersionUID	= 1L;

		private JTextField			titleField;
		private JTextArea			contentField;
		// protected int titleCaretPosition;
		private boolean				isLoading;
		private KMenu				menu;
		private JButton				saveButton;
		private KMenuItem			saveMenuItem;
		// saveAndCloseMenuItem;
		private JScrollPane			contentFieldPane;
		private final LineBorder	border				= new LineBorder(KindletUIResources.getInstance().getColor(KColorName.LIGHT_GRAY), 2, true);
		private Font				noteBodyFont;
		private boolean				isDirty;
		private Stack				undoStack;
		private Stack				redoStack;
		private static final int	MaxUndoLevel		= 50;
		// private SimpleDialog simplePromptDialog;
		// private boolean editableModeWasOn;
		private JButton				undoButton;
		private JButton				redoButton;

		// ------------------------------------------------------------------------------
		public NoteEditorView()
		{
			this.setLayout(new BorderLayout(0, 0));
			this.setFocusable(false);

			// simplePromptDialog = new SimpleDialog();
			// simplePromptDialog.setVisible(false);
			// simplePromptDialog.addComponentListener(new ComponentAdapter()
			// {
			// public void componentShown(ComponentEvent e)
			// {
			// simplePromptDialog.textField.requestFocus();
			// simplePromptDialog.repaint();
			// }
			//
			// public void componentResized(ComponentEvent e)
			// {
			// // simplePromptDialog.dialogFrame.repaint();
			// Dimension dim = simplePromptDialog.getSize();
			// int myWidth = 500;
			// int myHeight = 220;
			// simplePromptDialog.setSizeAndPosition(50, (dim.width - myWidth) /
			// 2, myWidth, myHeight);
			// // simplePromptDialog.validate();
			// // simplePromptDialog.dialogFrame.repaint();
			// KRepaintManager.getInstance().repaint(simplePromptDialog, true);
			// }
			// });
			//
			// this.add(simplePromptDialog, BorderLayout.CENTER);

			this.add(createButtonBar(), BorderLayout.NORTH);

			this.undoButton.setEnabled(false);
			this.redoButton.setEnabled(false);

			this.titleField = new JTextField()
			{
				private static final long	serialVersionUID	= 1L;

				public Insets getInsets()
				{
					return new Insets(2, 6, 2, 6);
				}
			};

			this.titleField.setBorder(border);
			this.titleField.setFont(App.settings.getFont(Settings.NoteTitleFont));
			this.titleField.putClientProperty(KindletUIResources.KClientProperties.ALT_LOOK, new Object());
			this.titleField.putClientProperty(KindletUIResources.KClientProperties.SELECT_ALL_TEXT_ON_FOCUS_GAINED, Boolean.FALSE);

			this.contentField = new JTextArea();
			this.contentField.setLineWrap(true);
			this.contentField.setWrapStyleWord(true);
			this.contentField.putClientProperty(KindletUIResources.KClientProperties.ALT_LOOK, new Object());
			this.contentField.putClientProperty(KindletUIResources.KClientProperties.SELECT_ALL_TEXT_ON_FOCUS_GAINED, Boolean.FALSE);
			this.noteBodyFont = App.settings.getFont(Settings.NoteBodyFont);
			this.contentField.setFont(this.noteBodyFont);

			// Set up the undo/redo
			undoStack = new Stack();
			redoStack = new Stack();

			this.contentField.getDocument().addUndoableEditListener(new UndoableEditListener()
			{
				public void undoableEditHappened(UndoableEditEvent e)
				{
					if (isLoading == false)
					{
						if (NoteEditorView.this.undoStack.size() >= MaxUndoLevel)
						{
							// Reached max undo size. Drop the tail of the
							// stack.
							NoteEditorView.this.undoStack.remove(0);
						}

						NoteEditorView.this.undoStack.push(e.getEdit());
						NoteEditorView.this.undoButton.setEnabled(true);

						// Clear the redo stack if it still has entries;
						if (NoteEditorView.this.redoStack.size() != 0)
						{
							NoteEditorView.this.redoStack.clear();
						}

						NoteEditorView.this.redoButton.setEnabled(false);
					}
				}
			});

			final GestureDispatcher gestureDispatcher = new GestureDispatcher();
			this.contentField.addMouseListener(gestureDispatcher);
//			this.contentField.addFocusListener(new FocusListener()
//			{
//				public void focusLost(FocusEvent e)
//				{					
//				}
//				
//				public void focusGained(FocusEvent e)
//				{
//					App.context.getOnscreenKeyboardManager().setVisible(true);
//				}
//			});
			
			ActionMap gestures = contentField.getActionMap();

			PinchZoomHandler fontSizeHandler = new PinchZoomHandler();

			gestures.put(Gestures.ACTION_SHRINK, fontSizeHandler);
			gestures.put(Gestures.ACTION_GROW, fontSizeHandler);

			// Wrap the text area with a JScrollPane to prevent text area from
			// auto-growing
			this.contentFieldPane = new JScrollPane(contentField);
			this.contentFieldPane.setFocusable(false);

			// Give the ViewPort some padding so that the cursor won't be
			// overlapping with the line border.
			this.contentFieldPane.setViewportBorder(new EmptyBorder(4, 4, 4, 4));
			this.contentFieldPane.setBorder(border);

			this.contentFieldPane.addComponentListener(new ComponentListener()
			{
				public void componentShown(ComponentEvent e)
				{					
					validate();
					NoteEditorView.this.repaint();
				}

				public void componentResized(ComponentEvent e)
				{
					if (App.controller.modalDialogOpen == false)
					{
						validate();
						repaint();
					}
				}

				public void componentMoved(ComponentEvent e)
				{}

				public void componentHidden(ComponentEvent e)
				{}
			});
			
			final JPanel itemContainer = new JPanel(new BorderLayout(2, 2))
			{
				private static final long	serialVersionUID	= 1L;

				public Insets getInsets()
				{
					return new Insets(5, 5, 5, 5);
				}
			};

			this.add(itemContainer, BorderLayout.CENTER);
			itemContainer.setFocusable(false);
			itemContainer.add(titleField, BorderLayout.NORTH);

			final Container contentContainer = new Container();
			contentContainer.setLayout(null);
			contentContainer.setFocusable(false);
			contentContainer.add(contentFieldPane);
			itemContainer.add(contentContainer, BorderLayout.CENTER);

			createMenu();

			App.context.getOnscreenKeyboardManager().addKeyboardListener(new MyKeyboardListener());
			final OnscreenKeyboardProperties keyboardPropsSentenceCase = new OnscreenKeyboardProperties();
			keyboardPropsSentenceCase.addProperty(OnscreenKeyboardProperties.KEYBOARD_PROPERTY_SENTENCE_CASE);
			OnscreenKeyboardUtil.configure(titleField, OnscreenKeyboardUtil.KEYBOARD_MODE_NORMAL, keyboardPropsSentenceCase);
			OnscreenKeyboardUtil.configure(contentField, OnscreenKeyboardUtil.KEYBOARD_MODE_NORMAL, keyboardPropsSentenceCase);

			final DocumentListener changeHandler = new documentChangeHandler();
			titleField.getDocument().addDocumentListener(changeHandler);
			contentField.getDocument().addDocumentListener(changeHandler);

			// Request focus for the first text component once the root
			// container becomes visible.
			addComponentListener(new ComponentAdapter()
			{
				public void componentShown(final ComponentEvent e)
				{
					App.checkDeviceSettings();
					
					final Point location = contentFieldPane.getLocationOnScreen();
					contentFieldPane.setSize(NoteEditorView.this.getWidth() - 10, App.settings.ScreenHeight - location.y);
					validate();
					NoteEditorView.this.repaint();
					setMenu(true);

//					if (App.controller.modalDialogOpen == false)
//					{
//						verifyFocus();
//					}
					
					App.controller.modalDialogOpen = false;
					verifyFocus();
//					NoteEditorView.this.repaint();
				}

				public void componentHidden(ComponentEvent e)
				{
					setMenu(false);
				}

				public void componentResized(ComponentEvent e)
				{
					App.checkDeviceSettings();
					
					final Point location = contentFieldPane.getLocationOnScreen();
					contentFieldPane.setSize(NoteEditorView.this.getWidth() - 10, App.settings.ScreenHeight - location.y);
					validate();

					if (App.controller.modalDialogOpen == false)
					{
						verifyFocus();
						NoteEditorView.this.repaint();
					}
				}
			});
		}

		// ----------------------------------------------------------------------------------
		private boolean undoContentEdit()
		{
			if (this.undoStack.size() > 0)
			{
				UndoableEdit edit = (UndoableEdit) this.undoStack.pop();
				edit.undo();
				this.redoStack.push(edit);
				this.redoButton.setEnabled(true);

				if (this.undoStack.size() == 0)
				{
					this.undoButton.setEnabled(false);
				}

				return true;
			}
			else
			{
				return false;
			}
		}

		// ----------------------------------------------------------------------------------
		private boolean redoContentEdit()
		{
			if (this.redoStack.size() > 0)
			{
				UndoableEdit edit = (UndoableEdit) this.redoStack.pop();
				edit.redo();
				this.undoStack.push(edit);
				this.undoButton.setEnabled(true);

				if (this.redoStack.size() == 0)
				{
					this.redoButton.setEnabled(false);
				}

				return true;
			}
			else
			{
				return false;
			}
		}

		// ----------------------------------------------------------------------------------
		private void setMenu(boolean enable)
		{
			try
			{
				if (Thread.interrupted() == false && App.context != null && this.menu != null)
				{
					if (enable == true)
						App.context.setMenu(this.menu);
					else
						App.context.setMenu(null);
				}
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
			}
		}

		// ------------------------------------------------------------------------------
		private Component createButtonBar()
		{
			ButtonBar bar = new ButtonBar("");

			// Add undo button
			this.undoButton = bar.addButton("undo", "images/undo.png", -1, "undo", new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					if (!undoContentEdit())
					{
						NoteEditorView.this.undoButton.setEnabled(false);
					}
				}
			});

			// Add redo button
			this.redoButton = bar.addButton("undo", "images/redo.png", -1, "redo", new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					if (!redoContentEdit())
					{
						NoteEditorView.this.redoButton.setEnabled(false);
					}
				}
			});

			// Add keyboard toggle button
			bar.addToggleButton("edit", "images/edit-on.png", "images/edit-off.png", true, "edit", new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					if (contentField.isEditable())
					{
						titleField.setEditable(false);
						contentField.setEditable(false);
					}
					else
					{
						titleField.setEditable(true);
						contentField.setEditable(true);
					}

					((ToggleButton) e.getSource()).setState(contentField.isEditable());
				}
			});

			// Add SaveAs button
			this.saveButton = bar.addButton("saveas", "images/saveas.png", -1, "saveas", new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					NoteEditorView.this.saveNoteAsNew();
				}
			});

			// Add Save button
			this.saveButton = bar.addButton("save", "images/save.png", -1, "save", new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					NoteEditorView.this.updateAndSaveNote();
					App.controller.getEventHandler().actionPerformed(new ActionEvent(NoteEditorView.this, -1, "noteeditor"));
				}
			});

			// Add close button
			bar.addButton("close", "images/close.png", -1, "close", new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					// TODO: Validate - make sure we have a title and a body.

					// Check if note is dirty, and ask whether it should be
					// saved.
					if (NoteEditorView.this.isDirty)
					{
						final int choice = KOptionPane.showConfirmDialog(NoteEditorView.this, "Would you like to save your changes before exiting?", "Closing Note Editor", KOptionPane.NO_YES_OPTIONS);

						// switch (choice)
						// {
						// case KOptionPane.YES_OPTION:
						// // Save the note then exit
						// NoteEditorView.this.updateAndSaveNote();
						// break;
						//
						// case KOptionPane.NO_OPTION:
						// break;
						// }

						if (choice == KOptionPane.YES_OPTION)
						{
							// Save the note then exit
							NoteEditorView.this.updateAndSaveNote();
						}
					}

					App.controller.getEventHandler().actionPerformed(new ActionEvent(NoteEditorView.this, -1, "close"));
				}
			});

			return bar;
		}

		// ----------------------------------------------------------------------------------
		private void setSaveState(boolean saveEnabled)
		{
			this.saveButton.setEnabled(saveEnabled);
			this.saveMenuItem.setEnabled(saveEnabled);
		}

		// ----------------------------------------------------------------------------------
		private void createMenu()
		{
			try
			{
				if (this.menu == null)
				{
					this.menu = new KMenu();

					// Create Save menu item
					this.saveMenuItem = new KMenuItem("Save and Continue Editing");
					saveMenuItem.addActionListener(new ActionListener()
					{
						public void actionPerformed(ActionEvent e)
						{
							// Save the note then exit
							NoteEditorView.this.updateAndSaveNote();
						}
					});
					this.menu.add(saveMenuItem);
					//
					// this.saveAndCloseMenuItem = new
					// KMenuItem("Save and Close");
					// saveAndCloseMenuItem.addActionListener(new
					// ActionListener()
					// {
					// public void actionPerformed(ActionEvent e)
					// {
					// // Save the note then exit
					// NoteEditorView.this.updateAndSaveNote();
					// closeView();
					// }
					// });
					// this.menu.add(saveAndCloseMenuItem);

					// Create cancel menu item
					final KMenuItem cancelMenuItem = new KMenuItem("Close Without Saving");
					cancelMenuItem.addActionListener(new ActionListener()
					{
						public void actionPerformed(ActionEvent e)
						{
							closeView();
						}
					});
					this.menu.add(cancelMenuItem);

					// Create a new note based on this one
					final KMenuItem saveAsNewMenuItem = new KMenuItem("Save As New Note");
					saveAsNewMenuItem.addActionListener(new ActionListener()
					{
						public void actionPerformed(ActionEvent e)
						{
							// KRepaintManager.getInstance().repaint(NoteEditorView.this,
							// true);
							NoteEditorView.this.repaint();
							NoteEditorView.this.saveNoteAsNew();
						}
					});
					this.menu.add(saveAsNewMenuItem);

					// Create Delete menu item
					final KMenuItem deleteMenuItem = new KMenuItem("Delete This Note");
					deleteMenuItem.addActionListener(new ActionListener()
					{
						public void actionPerformed(ActionEvent e)
						{
							final int choice = KOptionPane.showConfirmDialog(NoteEditorView.this, "This note will be permanently deleted.\nWould you like to continue?", "Delete This Note", KOptionPane.NO_YES_OPTIONS);

							if (choice == KOptionPane.YES_OPTION)
							{
								// Delete and close
								NoteEditor.this.note.delete();
								closeView();
							}
						}
					});
					this.menu.add(deleteMenuItem);

					// Create Help menu item
					final KMenuItem helpNewMenuItem = new KMenuItem("TakeNote Help");
					helpNewMenuItem.addActionListener(new ActionListener()
					{
						public void actionPerformed(ActionEvent e)
						{
							App.controller.getEventHandler().actionPerformed(new ActionEvent(NoteEditorView.this, App.HELPVIEW, null));
						}
					});
					this.menu.add(helpNewMenuItem);
				}
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
			}
		}

		// ----------------------------------------------------------------------------------
		public void setIsDirty(boolean isDirty)
		{
			this.isDirty = isDirty;
			NoteEditor.this.note.setIsDirty(isDirty);
			setSaveState(isDirty);
		}

		// //
		// ----------------------------------------------------------------------------------
		// public boolean getIsDirty()
		// {
		// return isDirty;
		// }

		// ----------------------------------------------------------------------------------
		private void updateAndSaveNote()
		{
			if (NoteEditor.this.note == null)
			{
				NoteEditor.this.note = new Note(App.controller.getCurrentNotebook());
			}

			NoteEditor.this.note.setName(NoteEditorView.this.titleField.getText());
			NoteEditor.this.note.setText(NoteEditorView.this.contentField.getText());
			NoteEditor.this.note.save();
		}

		// ----------------------------------------------------------------------------------
		private void verifyFocus()
		{
//			if (NoteEditorView.this.isShowing() && App.controller.modalDialogOpen == false)
//			{
//				App.logInfo("verifyFocus");
//				
//				if (titleField.getText().length() == 0)
//				{
//					App.logInfo("verifyFocus1.1");
//					if (titleField.hasFocus() == false)
//					{
//						App.logInfo("verifyFocus1.2");
//						SwingUtilities.invokeLater( new Runnable() 
//						{
//						    public void run() 
//						    { 
//						    	App.logInfo("verifyFocus1.3");
//						    	titleField.requestFocus();
//						    }
//						});	
//					}
//				}
//				else
//				{
//					App.logInfo("verifyFocus2.1");
//					if (contentField.hasFocus() == false)
//					{			
//						App.logInfo("verifyFocus2.2");
//						SwingUtilities.invokeLater( new Runnable() 
//						{
//						    public void run() 
//						    { 
//						    	App.logInfo("verifyFocus2.3");
//						    	contentField.requestFocus();
//						    }
//						});	
//					}
//				}
//			}
		}

		// ----------------------------------------------------------------------------------
		private void saveNoteAsNew()
		{
			NoteEditor.this.note.setName(NoteEditorView.this.titleField.getText());
			NoteEditor.this.note.setText(NoteEditorView.this.contentField.getText());

			try
			{
				App.controller.modalDialogOpen = true;
				String noteTitle = KOptionPane.showInputDialog(NoteEditorView.this, "What title would you like for the new Note?", "Save As", KOptionPane.PLAIN_MESSAGE, KOptionPane.CANCEL_SAVE_OPTIONS, NoteEditor.this.note.getName());

				if (noteTitle != null && noteTitle.trim().length() != 0)
				{
					noteTitle = noteTitle.trim();
	
					if (noteTitle.length() != 0 && NoteEditor.this.note != null && noteTitle.toLowerCase().compareTo(NoteEditor.this.note.getName().toLowerCase()) != 0)
					{
						NoteEditor.this.note = new Note(App.controller.getCurrentNotebook());
						NoteEditor.this.note.setName(noteTitle);
						NoteEditor.this.note.setText(NoteEditorView.this.contentField.getText());
						NoteEditor.this.note.save();
	
						closeView();
					}
				}
			}
			finally
			{
				App.controller.modalDialogOpen = false;
			}
		}

		// ----------------------------------------------------------------------------------
		private void closeView()
		{
			if (App.controller.modalDialogOpen)
			{
				App.controller.modalDialogOpen = false;
			}

			NoteEditor.this.note = null;
			App.controller.getEventHandler().actionPerformed(new ActionEvent(NoteEditorView.this, -1, "noteeditor"));
		}

		// ----------------------------------------------------------------------------------
		private class MyKeyboardListener implements OnscreenKeyboardListener
		{

			/** {@inheritDoc} */
			public void keyboardShown(final OnscreenKeyboardEvent event)
			{
				// The keyboard is now taking up a good chunk of the screen.
				// If we're in the content field, resize it so things don't get
				// cut off
				// Don't do this if a dialog is open.
				if (App.controller.modalDialogOpen == false)
				{
					App.checkDeviceSettings();
					
					// Get the location and size info for the content field
					final Dimension size = NoteEditorView.this.contentFieldPane.getSize();
					final Point location = contentFieldPane.getLocationOnScreen();
					size.height = App.settings.ScreenHeight - location.y - App.settings.KeyboardHeight - 5;
					contentFieldPane.setSize(size);
					contentFieldPane.setMaximumSize(size);
					contentField.setMinimumSize(size);
					contentFieldPane.validate();
					NoteEditorView.this.validate();
					contentFieldPane.repaint();
					NoteEditorView.this.repaint();
				}
			}

			/** {@inheritDoc} */
			public void keyboardHidden(final OnscreenKeyboardEvent event)
			{
				if (App.controller.modalDialogOpen == false)
				{
					App.checkDeviceSettings();
					
					final Dimension size = NoteEditorView.this.contentFieldPane.getSize();
					final Point location = contentFieldPane.getLocationOnScreen();
					size.height = App.settings.ScreenHeight - location.y - 10;
					contentFieldPane.setSize(size);
					contentFieldPane.setMaximumSize(size);
					contentField.setMinimumSize(size);
					contentFieldPane.validate();
					NoteEditorView.this.validate();
					contentFieldPane.repaint();
					NoteEditorView.this.repaint();
				}
			}

			/** {@inheritDoc} */
			public void keyboardMoved(final OnscreenKeyboardEvent event)
			{
			}

			/** {@inheritDoc} */
			public void keyboardResized(final OnscreenKeyboardEvent event)
			{}
		}

		// ----------------------------------------------------------------------------------
		// ----------------------------------------------------------------------------------
		private class PinchZoomHandler extends AbstractAction
		{
			private static final long	serialVersionUID	= 1L;

			// ------------------------------------------------------------------------------
			// Handle pinch and zoom to change font sizes
			// ------------------------------------------------------------------------------
			public void actionPerformed(ActionEvent e)
			{
				int fontSize = NoteEditorView.this.noteBodyFont.getSize();

				if (e.getActionCommand() == "shrink")
				{
					// Make font smaller, down to the minimum then no more
					if (fontSize > App.settings.getMinFontSize())
					{
						fontSize--;
						updateFontSize(fontSize);
					}
				}
				else
				{
					// Make font larger, up to the maximum then no more
					if (fontSize < App.settings.getMaxFontSize())
					{
						fontSize++;
						updateFontSize(fontSize);
					}
				}
			}

			// ------------------------------------------------------------------------------
			private void updateFontSize(int fontSize)
			{
				App.settings.setFontSize(fontSize);
				NoteEditorView.this.noteBodyFont = App.settings.getFont(Settings.NoteBodyFont);
				NoteEditorView.this.contentField.setFont(NoteEditorView.this.noteBodyFont);
				NoteEditorView.this.titleField.setFont(App.settings.getFont(Settings.NoteTitleFont));

				if (App.controller.modalDialogOpen == false)
				{
					NoteEditorView.this.validate();
					repaint();
				}
			}
		}

		// ----------------------------------------------------------------------------------
		// ----------------------------------------------------------------------------------
		private class documentChangeHandler implements DocumentListener
		{
			public void insertUpdate(DocumentEvent e)
			{
				if (NoteEditorView.this.isLoading == false)
				{
					NoteEditorView.this.setIsDirty(true);
					NoteEditorView.this.setSaveState(true);
				}
			}

			public void removeUpdate(DocumentEvent e)
			{
				if (NoteEditorView.this.isLoading == false)
				{
					NoteEditorView.this.setIsDirty(true);
					NoteEditorView.this.setSaveState(true);
				}
			}

			public void changedUpdate(DocumentEvent e)
			{
				if (NoteEditorView.this.isLoading == false)
				{
					NoteEditorView.this.setIsDirty(true);
					NoteEditorView.this.setSaveState(true);
				}
			}
		}

		// /**
		// * Create a JLabel for the passed JComponent, and put them in a JPanel
		// that uses BorderLayout. The label will be above the given
		// * text component inside the constructed panel
		// * @param labelText Text for the JLabel.
		// * @param c The JComponent to be wrapped.
		// * @return A new JPanel instance with the given text component added.
		// */
		// private JPanel wrapWithLabel(final String labelText, final JComponent
		// c)
		// {
		// final JPanel panel = new JPanel(new BorderLayout());
		// panel.add(new JLabel(labelText), BorderLayout.NORTH);
		// panel.add(c, BorderLayout.CENTER);
		// return panel;
		// }
	}
}

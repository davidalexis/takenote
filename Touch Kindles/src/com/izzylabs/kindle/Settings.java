/*
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * Copyright (c) 2012, David Alexis. All rights reserved.
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */

package com.izzylabs.kindle;

import java.awt.*;
import java.io.*;
import java.util.Properties;

import org.apache.log4j.Level;

import com.amazon.kindle.kindlet.ui.KindletUIResources;
import com.amazon.kindle.kindlet.ui.KindletUIResources.KFontStyle;

final class Settings //implements IViewable
{
	private Properties			settingsData			= null;
	private final String		settingsFileName		= System.getProperty("kindlet.home") + "/appSettings";
	private Font[]				fonts					= new Font[7];
    
	static final int		NotebookItemTitleFont 	= 0, 
			NotebookItemInfoFont 	= 1, 
			NoteTitleFont 			= 2,
			NoteBodyFont 			= 3, 
			BasicFont 				= 4,
			MediumBoldFont			= 5,
			LargeBoldFont			= 6;
	
	public int ScreenHeight,
			   ScreenWidth,
			   KeyboardHeight;
	
	boolean				isFistTimeUse			= false;
//	public int 					pageSize 				= 0;
	
//	protected SettingsView		view;
	
	
	// ----------------------------------------------------------------------------------
	public Settings()
	{
		this.load();
	}
	
	// ----------------------------------------------------------------------------------
	public String getSortField()
	{
		String sortField = settingsData.getProperty("sortField");

		if (sortField == null)
		{
			sortField = "modified";
			setSortField(sortField);
		}

		return sortField;
	}

	// ----------------------------------------------------------------------------------
	public void setSortField(String value)
	{
		settingsData.setProperty("sortField", value);
	}

	// ----------------------------------------------------------------------------------
	public String getSortDirection()
	{
		String sortDirection = settingsData.getProperty("sortDirection");

		if (sortDirection == null)
		{
			sortDirection = "desc";
			setSortDirection(sortDirection);
		}

		return sortDirection;
	}
	
	// ----------------------------------------------------------------------------------
	public void setSortDirection(String value)
	{
		settingsData.setProperty("sortDirection", value);
	}
	
	// ----------------------------------------------------------------------------------
	public int getFontSize()
	{
		int size = 21;

		if (settingsData.containsKey("fontSize"))
		{
			size = Integer.parseInt(settingsData.getProperty("fontSize"));
		}
		else
		{
			setFontSize(size);
		}

		return size;
	}

	// ----------------------------------------------------------------------------------
	public void setFontSize(int size)
	{
		settingsData.setProperty("fontSize", Integer.toString(size));
		save();
		
		// Calculate and create application fonts
		createFonts();
	}

	// ----------------------------------------------------------------------------------
	Font getFont(int fontId)
	{
		return this.fonts[fontId];
	}

	// ----------------------------------------------------------------------------------
	private void load()
	{
		settingsData = new Properties();

		try
		{
			File settingsFile = new File(settingsFileName);

			if (settingsFile.exists())
			{
				FileInputStream in = new FileInputStream(settingsFileName);
				settingsData.load(in);
				in.close();
			}
			else
			{
				setDefaults();
				isFistTimeUse = true;
			}

			// Create application fonts
			createFonts();
		}
		catch (Exception e)
		{
			App.logError("Settings failed to load");
			setDefaults();
		}
	}

	// ----------------------------------------------------------------------------------
	private void setDefaults()
	{
		settingsData.setProperty("fontSize", "22");
		settingsData.setProperty("sortField", "modified");
	}

	// ----------------------------------------------------------------------------------
	private void createFonts()
	{
		KindletUIResources uiResources = KindletUIResources.getInstance();
		int baseFontSize = getFontSize();
		
		// Set up array of application fonts
		this.fonts[NoteTitleFont] = uiResources.getFont(KindletUIResources.KFontFamilyName.SANS_SERIF, (int)(baseFontSize * 1.1), KFontStyle.BOLD);
		this.fonts[NoteBodyFont] = uiResources.getFont(KindletUIResources.KFontFamilyName.SANS_SERIF, baseFontSize, KFontStyle.PLAIN);
		this.fonts[NotebookItemTitleFont] = uiResources.getFont(KindletUIResources.KFontFamilyName.SANS_SERIF, 25, KFontStyle.BOLD);
		this.fonts[NotebookItemInfoFont] = uiResources.getFont(KindletUIResources.KFontFamilyName.SANS_SERIF, 19, KFontStyle.PLAIN);
		this.fonts[LargeBoldFont] = uiResources.getFont(KindletUIResources.KFontFamilyName.SANS_SERIF, 30, KFontStyle.BOLD);

		if (this.fonts[BasicFont] == null)
		{
			this.fonts[BasicFont] = uiResources.getFont(KindletUIResources.KFontFamilyName.SANS_SERIF, 19, KFontStyle.PLAIN);
		}
		
		if (this.fonts[MediumBoldFont] == null)
		{
			this.fonts[MediumBoldFont] = uiResources.getFont(KindletUIResources.KFontFamilyName.SANS_SERIF, 25, KFontStyle.BOLD);
		}		
	}

	// ----------------------------------------------------------------------------------
	void save()
	{
		FileOutputStream outputStream;

		try
		{
			outputStream = new FileOutputStream(settingsFileName);
			settingsData.store(outputStream, "settings");
			outputStream.close();
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	// ----------------------------------------------------------------------------------
	public void setHasSavedState(boolean value)
	{
		settingsData.setProperty("hasSavedState", Boolean.toString(value));
	}

	// ----------------------------------------------------------------------------------
	public boolean getHasSavedState()
	{
		String hasState = settingsData.getProperty("hasSavedState");
		
		if (hasState == null)
			return false;
		else
		{
			if (hasState.toLowerCase().compareTo("true") == 0)
				return true;
			else
				return false;
		}
	}

	// ----------------------------------------------------------------------------------
	public String getCurrentNotebookId() 
	{
		String notebookId = settingsData.getProperty("currentNotebook");
		return notebookId;
	}
	
	// ----------------------------------------------------------------------------------
	public void setCurrentNotebookId(String notebookId)
	{
		if (notebookId == null)
		{
			notebookId = "";
		}
		
		settingsData.setProperty("currentNotebook", notebookId);
		save();
	}

	// ----------------------------------------------------------------------------------
	public int getMinFontSize()
	{
		return 6;
	}

	// ----------------------------------------------------------------------------------
	public int getMaxFontSize()
	{
		return 96;
	}

	// ----------------------------------------------------------------------------------
	public Level getLoggingLevel()
	{
		String configuredLevel = settingsData.getProperty("log");
		Level level = Level.OFF;
		
		if (configuredLevel != null)
		{
			if (configuredLevel.compareTo("debug") == 0)
				level = Level.DEBUG;
			else
				level = Level.ERROR;
		}
			
		return level;
	}
	
	// ----------------------------------------------------------------------------------
	void VerifyHomeFolder()
	{
		// Verify our home folder
		File homeDir = App.context.getHomeDirectory();

		if (!homeDir.exists())
		{
			App.settings.isFistTimeUse = true;
			homeDir.mkdir();
		}
		
		homeDir = new File(App.context.getHomeDirectory(), "notebooks");

		if (!homeDir.exists())
		{
			App.settings.isFistTimeUse = true;
			homeDir.mkdir();
		}
	}
	
//	// ----------------------------------------------------------------------------------
//	// IViewable members
//	// ----------------------------------------------------------------------------------
//	
//	public Container getView()
//	{
//		if (this.view == null)
//		{
//			 this.view = new SettingsView();
//		}
//		
//		return this.view;
//	}
//
//	public int getViewId()
//	{
//		return App.SettingsView;
//	}
//
//	public String getViewName()
//	{
//		return "settings";
//	}
//
//	public void setView(Container view)
//	{
//		this.view = (SettingsView)view;
//	}
	
//	// ----------------------------------------------------------------------------------
//	// ----------------------------------------------------------------------------------
//	public class SettingsView extends JPanel
//	{
//		private static final long	serialVersionUID	= 1L;
//
//		public SettingsView()
//		{
//			this.setLayout(new BorderLayout(0, 0));
//			add(new TitlePanel("Settings", true, App.controller.getEventHandler()), BorderLayout.NORTH);
//			
//			String[] testData = {"one", "two", "free", "four"};
//			JList dataList = new JList(testData);
//			JScrollPane scrollPane = new JScrollPane(dataList);
//			
//			this.add(scrollPane, BorderLayout.CENTER);
//		}
//	}


}

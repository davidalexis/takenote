/*
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * Copyright (c) 2012, David Alexis. All rights reserved.
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */

package com.izzylabs.kindle;

final class SearchDefinition
{
	String 	term;
	int 	option;
	
	SearchDefinition(String term, int option)
	{
		this.term = term;
		this.option = option;
	}

	static class SearchOptions
	{
		static final int SEARCH_EXACT = 0;
		static final int SEARCH_ALL   = 1;
		static final int SEARCH_ANY   = 2;
	}
}

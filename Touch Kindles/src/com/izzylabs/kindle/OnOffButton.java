/*
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * Copyright (c) 2012, David Alexis. All rights reserved.
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */
package com.izzylabs.kindle;

import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import com.amazon.kindle.kindlet.ui.KindletUIResources;
import com.amazon.kindle.kindlet.ui.KindletUIResources.KColorName;

final class OnOffButton extends JButton
{

	private static final long	serialVersionUID	= 1L;
	
	private ImageIcon onIcon;
// TODO Remove unused code found by UCDetector
// 	protected ImageIcon offIcon;
	protected boolean   isOn;
	
	OnOffButton(final String onIconPath, final boolean initialStateIsOn, final String command, final ActionListener actionHandler)
	{
		this.onIcon  = new ImageIcon(getClass().getResource(onIconPath));
//		this.offIcon = new ImageIcon(getClass().getResource(offIconPath));
		this.isOn = initialStateIsOn;
		this.setIcon(this.onIcon);
		
		switchIcon();
		
		this.setActionCommand(command);
		this.addActionListener(actionHandler);
	}
	
	void flipState()
	{
		this.isOn = !this.isOn;
		switchIcon();
	}
	
	public void setState(boolean isOn)
	{
		this.isOn = isOn;
		switchIcon();
	}
	
	private void switchIcon()
	{
		if (this.isOn)
			this.setBackground(KindletUIResources.getInstance().getColor(KColorName.WHITE));
		else
			this.setBackground(KindletUIResources.getInstance().getColor(KColorName.GRAY_05));
//		if (this.isOn)
//			this.setIcon(this.onIcon);
//		else
//			this.setIcon(this.offIcon);
	}
}

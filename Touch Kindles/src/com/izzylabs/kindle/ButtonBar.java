/*
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * Copyright (c) 2012, David Alexis. All rights reserved.
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */

package com.izzylabs.kindle;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

import com.amazon.kindle.kindlet.ui.KindletUIResources;
import com.amazon.kindle.kindlet.ui.KindletUIResources.KColorName;
import com.amazon.kindle.kindlet.ui.KindletUIResources.KFontStyle;

class ButtonBar extends JPanel
{
	private static final long	serialVersionUID	= 1L;
	private JPanel 				buttonContainer;
	private JLabel 				headerLabel;
	private boolean				drawBorder;
	
	// ----------------------------------------------------------------------------------
	ButtonBar(String title)
	{
		this.setLayout(new BorderLayout(4, 4));
		final Font headerFont =  KindletUIResources.getInstance().getFont(KindletUIResources.KFontFamilyName.SANS_SERIF, 25, KFontStyle.BOLD);
		headerLabel = new JLabel(title)
		{
			private static final long	serialVersionUID	= 1L;

			public Insets getInsets()
			{
				return new Insets(5, 5, 5, 5);
			}
		};
		headerLabel.setFont(headerFont);
		headerLabel.setOpaque(false);
		
		this.buttonContainer = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		this.buttonContainer.setOpaque(false);
		this.add(this.buttonContainer, BorderLayout.EAST);
		this.add(headerLabel, BorderLayout.WEST);
	
		this.setBackground(KindletUIResources.getInstance().getColor(KColorName.GRAY_02));
		this.setShouldDrawBorder(true);
	}
	
	// ----------------------------------------------------------------------------------
	JButton addButton(final String title, final String iconPath, final int actionId, final String command, final ActionListener actionHandler)
	{
		final ImageIcon icon = new ImageIcon(getClass().getResource(iconPath));
		final JButton button = new JButton(icon);
		button.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				actionHandler.actionPerformed(new ActionEvent(e.getSource(), actionId, command));
			}
		});
	
		this.buttonContainer.add(button);
		
		return button;
	}
	
	// ----------------------------------------------------------------------------------
	ToggleButton addToggleButton(final String title, final String iconUpPath, final String iconDownPath, final boolean initialStateIsOn, final String command, final ActionListener actionHandler)
	{
		final ToggleButton button = new ToggleButton(iconUpPath, iconDownPath, initialStateIsOn, command, actionHandler);
		this.buttonContainer.add(button);
		
		return button;
	}
		
	// ----------------------------------------------------------------------------------
	OnOffButton addOnOffButton(final String onIconPath, final boolean initialStateIsOn, final String command, final ActionListener actionHandler)
	{
		final OnOffButton button = new OnOffButton(onIconPath, initialStateIsOn, command, actionHandler);
		this.buttonContainer.add(button);
		
		return button;
	}
	
	// ----------------------------------------------------------------------------------
	public void setTitle(String title)
	{
		this.headerLabel.setText(title);
	}
	
	// ----------------------------------------------------------------------------------
	public Insets getInsets()
	{
		return new Insets(5, 15, 5, 15);
	}

	public boolean getShouldDrawBorder()
	{
		return drawBorder;
	}

	public void setShouldDrawBorder(boolean drawBorder)
	{
		this.drawBorder = drawBorder;
	}

	// ----------------------------------------------------------------------------------
	public void paint(final Graphics g)
	{
		super.paint(g);

		if (this.drawBorder)
		{
			int width = getWidth();
			int height = getHeight() - 2;
			
			g.setColor(KindletUIResources.getInstance().getColor(KColorName.DARK_GRAY));
			g.drawLine(0, height, width, height);
		}
	}
		
}

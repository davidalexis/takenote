package com.izzylabs.kindle;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Container;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
 
import com.amazon.kindle.CustomDialog;
import com.amazon.kindle.kindlet.ui.KindletUIResources;
import com.amazon.kindle.kindlet.ui.KindletUIResources.KFontFamilyName;
import com.amazon.kindle.kindlet.ui.KindletUIResources.KFontStyle;

public class SearchDialog extends CustomDialog
{
    private static final long	serialVersionUID	= 1L;
    
	protected JTextField		textField;
	protected ButtonGroup		optionGroup;
	protected JRadioButton		exactOption;
	protected JRadioButton		allOption;
	protected JRadioButton		anyOption;
	protected final Font		font				= KindletUIResources.getInstance().getFont(KindletUIResources.KFontFamilyName.SANS_SERIF, 24, KFontStyle.PLAIN);
	protected ActionListener    okEventHandler;
	protected ActionListener    cancelEventHandler;

	// ----------------------------------------------------------------------------------
	private SearchDialog(final Container root, ActionListener okEvent, ActionListener cancelEvent) 
	{        
        super(root);      
        
        this.okEventHandler = okEvent;
        this.cancelEventHandler = cancelEvent;
    }
 
	// ----------------------------------------------------------------------------------
    protected void initDialog() 
    {        
        super.initDialog();
//        Dimension dialogSize = new Dimension();
//        
//        if (App.isBigScreen())
//        {
//        	dialogSize.height = 450;
//        	dialogSize.width = 550;
//        }
//        else
//        {
//        	dialogSize.height = 330;
//        	dialogSize.width = 500;
//        }
//        
//        this.setSize(dialogSize);
        this.setLayout(new GridBagLayout());
        
        final JLabel headerLabel = new JLabel("Find ...");
        headerLabel.setFont(KindletUIResources.getInstance().getFont(KFontFamilyName.SANS_SERIF, 26, KFontStyle.BOLD));
        
        final GridBagConstraints headerConstraints = new GridBagConstraints();
        headerConstraints.fill = GridBagConstraints.HORIZONTAL;
        headerConstraints.gridx = 0;
        headerConstraints.gridy = 0;
        headerConstraints.gridwidth =  3;
        headerConstraints.insets = new Insets(10, 20, 10, 20);
        headerConstraints.ipadx = 10;
        headerConstraints.ipady = 0;        
        headerConstraints.weightx = 0.5;        
        this.add(headerLabel, headerConstraints);
    	
		// Add search term textbox
		this.textField = new JTextField()
		{
			private static final long	serialVersionUID	= 1L;

			public Insets getInsets()
			{
				return new Insets(2, 5, 2, 5);
			}
		};
		
		this.textField.setFont(font);
		
		final GridBagConstraints textConstraints = new GridBagConstraints();
		textConstraints.fill = GridBagConstraints.HORIZONTAL;
		textConstraints.gridx = 0;
		textConstraints.gridy = 1;
		textConstraints.gridwidth = 3;        
		textConstraints.insets = new Insets(10, 20, 10, 20);
		textConstraints.weighty = 0.3;        
		add(this.textField, textConstraints);
        
        
        // Add search instructions
		
		exactOption = new JRadioButton("Exact Match", true);
		allOption = new JRadioButton("All Words", false);
		anyOption = new JRadioButton("Any words", false);
		exactOption.setActionCommand("0");
		allOption.setActionCommand("1");
		anyOption.setActionCommand("2");
		exactOption.setHorizontalTextPosition(SwingConstants.RIGHT);
		allOption.setHorizontalTextPosition(SwingConstants.RIGHT);
		anyOption.setHorizontalTextPosition(SwingConstants.RIGHT);
		optionGroup = new ButtonGroup();
		optionGroup.add(exactOption);
		optionGroup.add(allOption);
		optionGroup.add(anyOption);
		
		final Box optionContainer = Box.createVerticalBox();
		optionContainer.setAlignmentX(Component.RIGHT_ALIGNMENT);
		optionContainer.add(exactOption);
		optionContainer.add(allOption);
		optionContainer.add(anyOption);
		
		final GridBagConstraints optionConstraints = new GridBagConstraints();
		optionConstraints.fill = GridBagConstraints.HORIZONTAL;
		optionConstraints.gridx = 0;
		optionConstraints.gridy = 2;
		optionConstraints.gridwidth = 3;        
		optionConstraints.insets = new Insets(0, 20, 0, 20);
		optionConstraints.weighty = 0.5;        
		this.add(optionContainer, optionConstraints);

		final SearchDialog thisDialog = this;		 
		final JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				thisDialog.hideDialog();
				thisDialog.remove();
				thisDialog.cancelEventHandler.actionPerformed(e);
			}
		});
		
		final GridBagConstraints cancelButtonConstraints = new GridBagConstraints();
		cancelButtonConstraints.fill = GridBagConstraints.HORIZONTAL;
		cancelButtonConstraints.gridx = 0;
		cancelButtonConstraints.gridy = 3;
		cancelButtonConstraints.gridwidth = 1;        
		cancelButtonConstraints.insets = new Insets(10, 20, 20, 30);
		cancelButtonConstraints.weighty = 0.4;        
		cancelButtonConstraints.weightx = 0.5;
		add(cancelButton, cancelButtonConstraints);
		
		// Add the Search button
		final JButton searchButton = new JButton("Find");
		searchButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				thisDialog.hideDialog();
				
				int selectedSearchOption = Integer.parseInt(optionGroup.getSelection().getActionCommand());
				ActionEvent resultEvent = new ActionEvent(SearchDialog.this, selectedSearchOption, textField.getText());
				
                // Since a new Dialog is created each time, we will call remove on the
                // dialog to remove it from the root container.
                thisDialog.remove();
                thisDialog.okEventHandler.actionPerformed(resultEvent);
			}
		});
		
		final GridBagConstraints searchButtonConstraints = new GridBagConstraints();
		searchButtonConstraints.fill = GridBagConstraints.HORIZONTAL;
		searchButtonConstraints.gridx = 2;
		searchButtonConstraints.gridy = 3;
		searchButtonConstraints.gridwidth = 1;        
		searchButtonConstraints.insets = new Insets(10, 30, 20, 20);
		searchButtonConstraints.weighty = 0.4;
		searchButtonConstraints.weightx = 0.5;
		add(searchButton, searchButtonConstraints);
		
		this.setFocusDefault(this.textField); 
    }
    
    // ----------------------------------------------------------------------------------
    public static void show(final Container root, ActionListener okEvent, ActionListener cancelEvent)  
    {        
        final SearchDialog thisDialog = new SearchDialog(root, okEvent, cancelEvent);
        thisDialog.showDialog();
        
        Dimension dialogSize = thisDialog.getPreferredSize();
        dialogSize.width = root.getWidth() - 100;  
        thisDialog.setSize(dialogSize);
    }
}
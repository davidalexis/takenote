/*
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * Copyright (c) 2012, David Alexis. All rights reserved.
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */

package com.izzylabs.kindle;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.*;
import com.amazon.kindle.kindlet.event.GestureDispatcher;
import com.amazon.kindle.kindlet.event.GestureEvent;
import com.amazon.kindle.kindlet.input.Gestures;
import com.amazon.kindle.kindlet.ui.KPagedContainer;
import com.amazon.kindle.kindlet.ui.KPages;
import com.amazon.kindle.kindlet.ui.KindletUIResources;
import com.amazon.kindle.kindlet.ui.KindletUIResources.KFontStyle;
import com.amazon.kindle.kindlet.ui.KindletUIResources.KColorName;

final class Help implements IViewable
{
	private HelpView view;
	
	public Help()
	{
		this.view = new HelpView();
	}
	
	public Container getView()
	{
		return this.view;
	}

	public int getViewId()
	{
		return App.HELPVIEW;
	}

	public String getViewName()
	{
		return "help";
	}

	public void setView(Container view)
	{
		this.view = (HelpView)view;
	}

	// ----------------------------------------------------------------------------------
	private class HelpView extends JPanel
	{
		private static final long	serialVersionUID	= 1L;
		
		private Font				textFont;
		private Font				textFontBoldLarge;
		private Font				textFontBold;
		private KPages				pages;
		private FooterPanel 		footer;
		private int 				currentPageNumber;
		private int					pageCount;
		private int					contentWidth;
		
		// ----------------------------------------------------------------------------------
		public HelpView()
		{
			// TODO:  Read help content from file.
			
			setLayout(new BorderLayout(0, 0));
			setFocusable(false);
			contentWidth = (int)(App.context.getRootContainer().getWidth() * 0.9);
			
			final ButtonBar bar = new ButtonBar("TakeNote Help");
			this.add(bar, BorderLayout.NORTH);
			
			// Add close button
			bar.addButton("close", "images/close.png", -1, "close", new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					App.controller.getEventHandler().actionPerformed(new ActionEvent(HelpView.this, -1, "close"));
				}
			});

			this.pages = new KPages() //PageProviders.createFullPageProvider())
			{
				private static final long serialVersionUID = 1L;

				public Insets getInsets()
				{
					return new Insets(20, 20, 20, 20);
				}
			};

			this.pages.setFirstPageAlignment(BorderLayout.NORTH);
			this.pages.setLastPageAlignment(BorderLayout.NORTH);
			this.pages.setFocusable(false);
			this.pages.setPagingPolicy(KPagedContainer.PAGING_POLICY_LOCAL);
			
			this.currentPageNumber = 1;

			add(pages, BorderLayout.CENTER);
			
			footer = new FooterPanel();
			add(footer, BorderLayout.SOUTH);
			int fontSmall, fontMedium, fontLarge;
			
			if (App.isBigScreen())
			{
				fontSmall = 23;
				fontMedium = 25;
				fontLarge = 28;
			}
			else
			{
				fontSmall = 19;
				fontMedium = 22;
				fontLarge = 25;
			}
			
			final KindletUIResources resources = KindletUIResources.getInstance();
			textFont = resources.getFont(KindletUIResources.KFontFamilyName.SANS_SERIF, fontSmall, KFontStyle.PLAIN);
			textFontBold = resources.getFont(KindletUIResources.KFontFamilyName.SANS_SERIF, fontMedium, KFontStyle.BOLD);
			textFontBoldLarge = resources.getFont(KindletUIResources.KFontFamilyName.SANS_SERIF, fontLarge, KFontStyle.BOLD);
			
			final List contentPages = new ArrayList();
			
			JPanel page = createPage();
			page.setLayout(new BoxLayout(page, BoxLayout.Y_AXIS));
			
			page.add(createHeading1("WELCOME TO TAKENOTE – TOUCH EDITION", 10));
			page.add(createParagraph("Thank you for purchasing TakeNote, the easy note manager for Kindle devices!"));
			page.add(createParagraph("This edition is designed to take full advantage of your Kindle Touch or Kindle Paperwhite, allowing you to interact directly with objects on the screen.  " +
					"TakeNote is designed to be an easy and intuitive way to take notes on your Kindle device, and has a simple user interface that does not get in your way."));
			page.add(createParagraph("This edition adds many new features, including searching, filtered exporting, and support for multiple notebooks."));
			page.add(Box.createVerticalGlue());
			contentPages.add(page);
			
			page = createPage();			
			page.add(createHeading1("USING TAKENOTE", 10));			
			page.add(createHeading2("STANDARD KINDLE TOUCH TOOLBAR AND MENU ITEMS", 10));
			
			//JPanel paragraph = new JPanel(new FlowLayout(FlowLayout.LEFT));
			//page.add(new KImage(Toolkit.getDefaultToolkit().createImage(getClass().getResource("images/help/standardtoolbar.png"))));
			page.add(createParagraph("The Kindle Touch displays a standard toolbar that " +
				"includes the Back and the Shopping Cart buttons. " +
				"Tapping on either button will exit TakeNote."));
			page.add(createParagraph("The toolbar also contains the Menu button.  Menus " +
				"include two system items (\"Turn On/Off Wireless\" " +
				"and \"Shop in Kindle Store\") that cannot be removed. " +
				"Anything below these are TakeNote's menu options."));
			page.add(Box.createVerticalGlue());
			contentPages.add(page);
			
			page = createPage();
			page.add(createHeading2("TAKENOTE INTERFACE ELEMENTS", 10));
			//page.add(new KImage(Toolkit.getDefaultToolkit().createImage(getClass().getResource("images/help/takenotetoolbar.png"))));
			page.add(createParagraph("Directly below the standard Kindle Touch toolbar is " +
					"TakeNote's toolbar. This will typically consist of a " +
					"title on the left and icons to the right providing " +
					"context-specific functionality."));
			//page.add(new KImage(Toolkit.getDefaultToolkit().createImage(getClass().getResource("images/help/footer.png"))));
			page.add(createParagraph("A status bar at the bottom of the screen " +
					"provides information such as page numbers."));
			page.add(createParagraph("The large center area of each screen contains the " +
					"main functionality - list of notes, note content, " +
					"help content, etc."));
			page.add(Box.createVerticalGlue());
			contentPages.add(page);
			
			page = createPage();
			page.add(createHeading1("MAIN SCREEN - NOTEBOOK", 10));
			page.add(createParagraph("The title bar displays the name of the current " +
					"Notebook at the left and icons for searching and " +
					"creating new notes on the right. The center area of " +
					"the screen displays the list of notes, and the footer " +
					"bar shows the number of notes in the notebook " +
					"and the number of pages of notes."));
			page.add(createParagraph(" "));
			
			page.add(createHeading2("CREATING A NEW NOTE", 0));
			
//			final JPanel newNoteContent = createGridBagParagraph();
//			GridBagConstraints gridSetup = new GridBagConstraints();
//			gridSetup.fill = GridBagConstraints.HORIZONTAL;
//			gridSetup.anchor = GridBagConstraints.NORTHWEST;
//			gridSetup.weightx = 0.1; gridSetup.gridx = 0; gridSetup.gridy = 0;
//			newNoteContent.add(new KImage(Toolkit.getDefaultToolkit().createImage(getClass().getResource("images/newnote.png"))), gridSetup);
//			gridSetup.weightx = 100; gridSetup.gridx = 1; gridSetup.gridy = 0;
			page.add(createParagraph("Create a new Note by tapping on the New Note " +
										  "icon. This will take you to the Note Editor " +
										  "screen where you can enter the contents of " +
										  "your note."));
			//page.add(newNoteContent);
//			page.add(createParagraph(" ", false, 30));
			page.add(Box.createVerticalGlue());
			contentPages.add(page);

			page = createPage();
			page.add(createHeading1("NOTE EDITOR", 10));
			page.add(createParagraph("Tapping on a Note in the Notebook screen opens it in " +
					"the Note Editor screen.  Here you will find a toolbar of " +
					"icons at the top, a text field for the title of the Note, " +
					"and the text area for the body of the Note.  "));
			page.add(createParagraph("When a new Note is created, the cursor will be in the " +
					"note title field, and the on-screen keyboard will be " +
					"displayed. Enter a title for your Note, then tap into " +
					"the body field where you can begin entering the " +
					"content of your Note."));
			page.add(createParagraph("Tap into the text at any location to start inserting or " +
					"deleting characters from that location."));
			page.add(Box.createVerticalGlue());
			contentPages.add(page);

			page = createPage();
			page.add(createParagraph("The tool bar contains icons that represent the actions " +
					"that can be taken on a Note."));
			page.add(createParagraph(" "));
			
			page.add(createHeading2("SAVING NOTES", 10));
//			final JPanel saveContent = createGridBagParagraph();
//			gridSetup.weightx = 0.1; gridSetup.gridx = 0; gridSetup.gridy = 0;
//			saveContent.add(new KImage(Toolkit.getDefaultToolkit().createImage(getClass().getResource("images/save.png"))), gridSetup);
//			gridSetup.weightx = 100; gridSetup.gridx = 1; gridSetup.gridy = 0;
			page.add(createParagraph("The Save icon is disabled when you first go " +
											"into a Note, since no changes have been " +
											"made yet. The icon will become enabled " +
											"when a change is made to the note. " +
											"Tap on the icon, and the Note will be saved " +
											"and the editor will be closed.")); //, gridSetup);
//			page.add(saveContent);
//			page.add(createParagraph(" ", false, 10));
			
//			final JPanel saveasContent = createGridBagParagraph();
//			gridSetup.weightx = 0.1; gridSetup.gridx = 0; gridSetup.gridy = 0;
//			saveasContent.add(new KImage(Toolkit.getDefaultToolkit().createImage(getClass().getResource("images/saveas.png"))), gridSetup);
//			gridSetup.weightx = 100; gridSetup.gridx = 1; gridSetup.gridy = 0;
			page.add(createParagraph("Tap on the Save As icon to create a new note " +
											  "with the contents of the current note. " +
											  "Enter a title for the new note and tap the " +
											  "OK button. The note will be created and " +
											  "the editor will close.")); //, gridSetup);
			//page.add(saveasContent);
//			page.add(createParagraph(" ", false, 30));
			page.add(Box.createVerticalGlue());
			contentPages.add(page);

			page = createPage();
			page.add(createHeading2("EDIT MODE", 0));
//			final JPanel editModeContent = createGridBagParagraph();
//			gridSetup.weightx = 0.1; gridSetup.gridx = 0; gridSetup.gridy = 0;
//			editModeContent.add(new KImage(Toolkit.getDefaultToolkit().createImage(getClass().getResource("images/edit-on.png"))), gridSetup);
//			gridSetup.weightx = 100; gridSetup.gridx = 1; gridSetup.gridy = 0;
			page.add(createParagraph("The edit mode allows you to toggle between " +
											    "editable and read-only modes. Tap on this " +
											    "icon and the on-screen keyboard will be " +
											    "hidden. This can be handy when you want to " +
											    "just read through a Note without the on-screen " +
											    "keyboard getting in the way. "));
			
//			gridSetup.weightx = 0.1; gridSetup.gridx = 0; gridSetup.gridy = 1;
//			editModeContent.add(new KImage(Toolkit.getDefaultToolkit().createImage(getClass().getResource("images/edit-off.png"))), gridSetup);
//			gridSetup.weightx = 100; gridSetup.gridx = 1; gridSetup.gridy = 1;
			page.add(createParagraph("The icon will change to indicate that editing " +
												"is now disabled. Tap on the icon again to " +
												"switch back to edit mode. Tapping on either " +
												"the note title of body area will cause the " +
												"on-screen keyboard to re-appear."));
//			page.add(editModeContent);
//			page.add(createParagraph(" ", false, 30));
			page.add(Box.createVerticalGlue());
			contentPages.add(page);

			page = createPage();			
			page.add(createHeading2("UNDO / REDO", 0));
			page.add(createParagraph("TakeNote allows undo/redo of up to 50 changes " +
					"during and editing session. The undo and redo icons " +
					"will be disabled if no changes were made to the note."));
//			final JPanel undoRedoContent = createGridBagParagraph();
//			gridSetup.weightx = 0.1; gridSetup.gridx = 0; gridSetup.gridy = 0;
//			undoRedoContent.add(new KImage(Toolkit.getDefaultToolkit().createImage(getClass().getResource("images/undo.png"))), gridSetup);
//			gridSetup.weightx = 100; gridSetup.gridx = 1; gridSetup.gridy = 0;
			page.add(createParagraph("As you make changes to the note, the Undo " +
												"icon will become enabled. Tap on the icon " +
												"to undo a change. Repeatedly tap to undo up " +
												"to 50 changes."));			
//			gridSetup.weightx = 0.1; gridSetup.gridx = 0; gridSetup.gridy = 1;
//			undoRedoContent.add(new KImage(Toolkit.getDefaultToolkit().createImage(getClass().getResource("images/redo.png"))), gridSetup);
//			gridSetup.weightx = 100; gridSetup.gridx = 1; gridSetup.gridy = 1;
			page.add(createParagraph("Tapped the redo icon to re-apply the changes that were undone."));
//			page.add(undoRedoContent);
//			page.add(createParagraph(" ", false, 30));
			page.add(Box.createVerticalGlue());
			contentPages.add(page);

			page = createPage();
			page.add(createHeading2("LARGE NOTES", 0));
			page.add(createParagraph("Once the content of a note exceeds the available " +
					"size of the edit area, you will notice that a scrollbar " +
					"appears on the right side, and two page navigation " +
					"buttons appear at the bottom of the edit area. " +
					"These allow you to page up and down through the " +
					"Note’s content."));
			page.add(createParagraph("The edit area is also touch-enabled for navigation. " +
					"Swipe upwards to move down through the Note " +
					"content, and swipe downwards to scroll up."));
			page.add(createParagraph(" "));
			page.add(createHeading2("FONT SIZE", 0));
			page.add(createParagraph("The size of the editor fonts can be easily changed by " +
					"using “pinch” and “zoom” gestures, just like any " +
					"other touch-enabled device. Pinch your fingers " +
					"together in the edit area to make the font smaller. " +
					"Spread your fingers apart to make the font larger. " +
					"TakeNote remembers the font size change, so it will " +
					"be the same the next time you use the application.", false, 10));
			page.add(Box.createVerticalGlue());
			contentPages.add(page);


			page = createPage();
			page.add(createHeading2("NOTE EDITOR MENU", 0));
			page.add(createParagraph("As you can see, all common actions we have talked " +
					"about so far is done via touch, without using the " +
					"menu. The Note Editor, however, has useful menu " +
					"options for actions that you probably won’t be using " +
					"on a regular basis. These include saving your " +
					"changes without closing the editor, deleting the Note, " +
					"and accessing the TakeNote Help."));
//			final JPanel closeContent = createGridBagParagraph();
//			gridSetup.weightx = 0.1; gridSetup.gridx = 0; gridSetup.gridy = 0;
//			closeContent.add(new KImage(Toolkit.getDefaultToolkit().createImage(getClass().getResource("images/help/closebutton.png"))), gridSetup);
//			gridSetup.weightx = 100; gridSetup.gridx = 1; gridSetup.gridy = 0;
			page.add(createParagraph("The Close icon will close the Note Editor when " +
											 " tapped. If you have made any changes to the " +
											 "Note, a confirmation dialog will be displayed, " +
											 "asking whether you would like to save the " +
											 "changes before exiting."));
//			page.add(closeContent);
//			page.add(createParagraph(" ", false, 30));
			page.add(Box.createVerticalGlue());
			contentPages.add(page);

			page = createPage();
			page.add(createHeading2("NOTEBOOK SCREEN - NOTE LIST", 0));
			page.add(createParagraph("The note list displays each note's title, date created, " +
					"and date last modified). Tapping on a note will open " +
					"it in the note editor screen."));
			page.add(createParagraph("The number of notes in your notebook will be " +
					"displayed on the right side of the footer. When you " +
					"have multiple pages of notes, the page indicator " +
					"on the left side of the footer will indicate the number " +
					"of pages of notes and the page that is currently " +
					"being displayed. For example, it may say " +
					"“page 1 of 3”."));
			page.add(createParagraph("Flip forward to the next page by swiping your finger " +
					"across the screen from right to left. Flip backwards to " +
					"the previous page by swiping your finger across the " +
					"screen from left to right."));
			page.add(Box.createVerticalGlue());
			contentPages.add(page);

			page = createPage();
			page.add(createParagraph("Long-tap (press and hold for about a second or two) " +
					"on a note to display a pop-up menu of available " +
					"actions that can be performed on the note. These are " +
					"“Delete this note” and “Make a copy of this note”."));
			page.add(createParagraph("Tapping on the “delete” option will display a " +
					"confirmation dialog asking if you are sure you’d like " +
					"to delete the note."));
			page.add(createParagraph("Tapping on the “make a copy” option will display the " +
					"Save As dialog. Enter the new title and tap Ok, and " +
					"a copy of the note will be saved under the new title " +
					"you entered."));
			page.add(Box.createVerticalGlue());
			contentPages.add(page);

			page = createPage();
			page.add(createHeading2("NOTEBOOK SCREEN MENU", 0));
			page.add(createParagraph("The menu provides access to the following features."));
			page.add(createParagraph(" "));
			
			page.add(createParagraph("HELP", true, 0));
			page.add(createParagraph("Opens the TakeNote Help screen to display " +
					"this document."));
			page.add(createParagraph(" "));
			
			page.add(createParagraph("SORTING NOTES", true, 0));
			page.add(createParagraph("Tap the “Sort Notes By...” menu option to display the " +
					"sort options dialog. Notes can be sorted by last " +
					"modified date or title in ascending or descending " +
					"order. The default sort is by modified date, " +
					"descending order (newest to oldest). TakeNote " +
					"remembers your selection, so notes will be displayed " +
					"in the selected order whenever you use the " +
					"application."));
			page.add(Box.createVerticalGlue());
			contentPages.add(page);

			page = createPage();
			page.add(createHeading2("SEARCHING", 10));
//			final JPanel searchContent = createGridBagParagraph();
//			gridSetup.weightx = 0.1; gridSetup.gridx = 0; gridSetup.gridy = 0;
//			searchContent.add(new KImage(Toolkit.getDefaultToolkit().createImage(getClass().getResource("images/find.png"))), gridSetup);
//			gridSetup.weightx = 100; gridSetup.gridx = 1; gridSetup.gridy = 0;
			page.add(createParagraph("Tap on the search icon to display the search " +
											  "options dialog. Enter the text you would like " +
											  "to find then select one of three search methods " +
											  "– notes containing the exact word or phrase " +
											  "that was entered, notes containing all of the " +
											  "words entered (the words can appear anywhere " +
											  "in a note), or notes containing any of the " +
											  "words entered (one or more of the words)."));
//			page.add(searchContent);
			
			page.add(createParagraph("Tap on the Find button to activate the search. The " +
					"list of Notes matching the search criteria in either the " +
					"title or the body will be displayed. The search icon on " +
					"the title bar will have a darkened background, " +
					"indicating that a filter is in effect. Tap the icon again " +
					"to clear the search filter and go back to displaying " +
					"the full list of notes."));
//			page.add(createParagraph(" ", false, 150));
			page.add(Box.createVerticalGlue());
			contentPages.add(page);
			
			page = createPage();
			page.add(createHeading2("SWITCH NOTEBOOKS", 10));
			page.add(createParagraph("TakeNote allows you to organize your notes into " +
					"different Notebooks. For example, you may have one " +
					"Notebook for general quick notes, another for " +
					"meeting notes at work, yet another for your poetry, " +
					"etc, etc. Switching between Notebooks is as " +
					"easy as selecting the “Switch to Another Notebook” " +
					"menu option.", false, 5));
			page.add(createParagraph(" ", false, 10));
			
			page.add(createHeading1("NOTEBOOKS SCREEN", 10));
			page.add(createParagraph("Selecting  the “Switch to Another Notebook” menu " +
					"option will display the Notebooks screen with a list of " +
					"notebooks.  Each entry in the list shows the name of " +
					"the notebook and the number of notes it contains."));
			page.add(createParagraph("The Notebooks screen’s title bar contains two icons " +
					"on the right.  The Close icon will close the " +
					"notebook list screen and go back to the Note list " +
					"screen without changing the currently selected " +
					"Notebook."));
			page.add(Box.createVerticalGlue());
			contentPages.add(page);

			page = createPage();
			page.add(createParagraph("The second icon is the New Notebook icon.  Tap on " +
					"this and a dialog will be displayed asking for the " +
					"name of the new Notebook. Enter the name then tap " +
					"the “OK” button. The new, empty Notebook will be " +
					"created and displayed in the Notebook list. " +
					"Notebooks are sorted alphabetically. " +
					"Notebook names can be up to 30 characters."));
			page.add(createParagraph("Tap on your new Notebook and it will be opened in " +
					"the Notebook (notes list) screen, with the notebook’s " +
					"name displayed on the title bar."));
			page.add(createParagraph("Navigating a multi-page list of Notebooks is just like " +
					"navigating pages of Notes in the main screen -  " +
					"swipe left to go to the next page, and swipe right to " +
					"go to the previous page. Tapping on a Notebook in " +
					"the list opens the Notebook."));
			page.add(Box.createVerticalGlue());
			contentPages.add(page);

			page = createPage();
			page.add(createHeading2("EXPORTING TO HTML", 10));
			page.add(createParagraph("The \"Export Notebook to HTML\" menu option " +
					"(Notebook screen) will export the current notebook to " +
					"an HTML document. If there is " +
					"currently a search filter in effect, only the " +
					"filtered list of notes will be exported. The name of the file " +
					"will be the name of the Notebook plus the current " +
					"date and time. To find the file:"));
			
			GridBagConstraints gridSetup = new GridBagConstraints();
			gridSetup.fill = GridBagConstraints.HORIZONTAL;
			gridSetup.anchor = GridBagConstraints.NORTHWEST;
			
			final JPanel Content = createGridBagParagraph();
			gridSetup.weightx = 0.1; gridSetup.gridx = 0; gridSetup.gridy = 0;
			Content.add(new JLabel("1. "), gridSetup);
			gridSetup.weightx = 100; gridSetup.gridx = 1; gridSetup.gridy = 0;
			Content.add(createParagraph("Connect your Kindle to a USB port of your " +
					"computer. (If your Kindle is password-" +
					"protected, input your password and press Enter.)"), gridSetup);
			gridSetup.weightx = 0.1; gridSetup.gridx = 0; gridSetup.gridy = 1;
			Content.add(new JLabel("2. "), gridSetup);
			gridSetup.weightx = 100; gridSetup.gridx = 1; gridSetup.gridy = 1;
			Content.add(createParagraph("Navigate to the Kindle from your computer " +
					"and select the \".active-content-data\" folder."), gridSetup);
			gridSetup.weightx = 0.1; gridSetup.gridx = 0; gridSetup.gridy = 2;
			Content.add(new JLabel("3. "), gridSetup);
			gridSetup.weightx = 100; gridSetup.gridx = 1; gridSetup.gridy = 2;
			Content.add(createParagraph("Within that folder, select " +
					"\"8a10d02f2d12d4d2012d3b1ec3040004\""), gridSetup);
			gridSetup.weightx = 0.1; gridSetup.gridx = 0; gridSetup.gridy = 3;
			Content.add(new JLabel("4. "), gridSetup);
			gridSetup.weightx = 100; gridSetup.gridx = 1; gridSetup.gridy = 3;
			Content.add(createParagraph("Within this folder, go to \"work/exported\", " +
					"where you will find the HTML file."), gridSetup);

			page.add(Content);
			page.add(createParagraph(" ", false, 150));
			page.add(Box.createVerticalGlue());
			contentPages.add(page);

			page = createPage();
			page.add(createHeading1("BACKUP YOUR NOTES", 10));
			page.add(createParagraph("You should back up your TakeNote data often so you " +
					"won't lose everything if something happens to your Kindle."));
			page.add(createParagraph("To do so, connect your Kindle to your PC and follow " +
					"the 'Exporting to HTML' instructions to locate the " +
					"TakeNote folder. Copy the entire 'work' folder to " +
					"your PC."));
			page.add(createParagraph("If you ever need to restore your data, simply replace " +
					"the 'work' folder on the Kindle with the backed up " +
					"copy.  "));
			page.add(Box.createVerticalGlue());
			contentPages.add(page);

			page = createPage();
			page.add(createHeading1("ABOUT IZZYLABS", 10));
			page.add(createParagraph("TakeNote is created by IzzyLabs, a small software " +
					"company founded by David Alexis and his daughter, " +
					"Isabel (Izzy) Alexis. Our goal is to create software " +
					"that is easy, useful, and fun."));
			page.add(createParagraph("We hope you like TakeNote!"));
			page.add(createParagraph("Please visit us at http://www.izzylabs.com, and drop " +
					"us a line to tell us how you like TakeNote at " +
					"takenote.support@izzylabs.com."));
			page.add(createParagraph("Copyright © 2012, IzzyLabs, David Alexis. " +
					"All Rights Reserved.", false, 20));
			
			page.add(createParagraph("Copyright Notices", true, 0));
			page.add(createParagraph("TakeNote uses JRegex - Copyright (c) 2001, Sergey A. Samokhodkin"));
			page.add(Box.createVerticalGlue());
			contentPages.add(page);

			
			final Iterator pageIterator = contentPages.iterator();
			
			while (pageIterator.hasNext())
			{
				pages.addItem(pageIterator.next());
			}
			
			pages.setFocusTraversalKeysEnabled(false);		
			pages.validate();
			pages.setLocation(pages.getPageStartLocation());
			
			HelpView.this.footer.updateStatus();

			pages.validate();
			this.pageCount = pages.getPageModel().getLastLocation() + 1;
			footer.updateStatus();
			
			addComponentListener(new ComponentListener()
			{
				public void componentShown(ComponentEvent e)
				{
					pages.setPagingPolicy(KPagedContainer.PAGING_POLICY_LOCAL);
					pages.validate();
					//pages.setLocation(0);
					pages.first();
					pages.repaint();
					App.context.setMenu(null);
				}

				public void componentResized(ComponentEvent e)
				{
					pages.setPagingPolicy(KPagedContainer.PAGING_POLICY_LOCAL);
					pages.validate();
					//pages.setLocation(pages.getPageStartLocation());
					pages.repaint();
				}

				public void componentMoved(ComponentEvent e)
				{
				}

				public void componentHidden(ComponentEvent e)
				{
					pages.setPagingPolicy(KPagedContainer.PAGING_POLICY_DISABLED);
				}
			});
			
			pages.addPropertyChangeListener(new PropertyChangeListener()
			{

				public void propertyChange(PropertyChangeEvent evt)
				{				
					if (evt.getPropertyName().compareTo("pageEndLocation") == 0)
					{
//						pages.relayoutPage();
//						pages.repaint();
													
						currentPageNumber = pages.getPageStartLocation() + 1;
						HelpView.this.footer.updateStatus();
//						KRepaintManager.getInstance().repaint(pages, false);
//						pages.repaint();
					}
				}
			});
			
			final GestureDispatcher gestureDispatcher = new GestureDispatcher();
			pages.addMouseListener(gestureDispatcher);
			final ActionMap gestures = pages.getActionMap();

			gestures.put(Gestures.ACTION_TAP, new AbstractAction()
			{
				private static final long	serialVersionUID	= 1L;
				private final int leftTenPercentOfScreen = 120;
				
				public void actionPerformed(ActionEvent e)
				{					
					if (App.controller.modalDialogOpen == false && e instanceof GestureEvent) 
					{
		                final GestureEvent gesture = (GestureEvent) e;
		                final Point tapLocation = gesture.getLocation();
		                
		                if (tapLocation.x <= leftTenPercentOfScreen)
		                {
		                	// Go to previous page
		                	pages.previous();
		                }
		                else
		                {
		                	// Go to next page
		                	pages.next();
		                }
		                
//	                	pages.repaint();
					}
				}
			});

		}

		// ----------------------------------------------------------------------------------
		private JPanel createGridBagParagraph()
		{
			JPanel paragraph = new JPanel(new GridBagLayout())
			{
				private static final long	serialVersionUID	= 1L;

				public Insets getInsets()
			 	{
			 		return new Insets(0, 20, 0, 10);
			 	}
			};
			return paragraph;
		}
	
		// ----------------------------------------------------------------------------------
		private JPanel createPage()
		{
			final JPanel page = new JPanel();
			page.addComponentListener(new ComponentListener()
			{
				
				public void componentShown(ComponentEvent e)
				{
					repaint();
				}
				
				public void componentResized(ComponentEvent e)
				{
					validate();
					repaint();
				}
				
				public void componentMoved(ComponentEvent e)
				{
					// TODO Auto-generated method stub
					
				}
				
				public void componentHidden(ComponentEvent e)
				{
					// TODO Auto-generated method stub
					
				}
			});
			
			final BoxLayout layout = new BoxLayout(page, BoxLayout.Y_AXIS);
			page.setLayout(layout);
			page.setAlignmentX(LEFT_ALIGNMENT);
			page.setAlignmentY(TOP_ALIGNMENT);
			page.setFocusable(false);
			
			return page;
		}

		// ----------------------------------------------------------------------------------
		private JComponent createHeading1(String text, final int spacingAfter)
		{
			final JTextArea headingLabel = new JTextArea(text)
			{
				private static final long	serialVersionUID	= 1L;

				public Insets getInsets()
				{
					return new Insets(10, 20, spacingAfter, 20);
				}
			};
			
			headingLabel.setEditable(false);
			headingLabel.setFocusable(false);
			headingLabel.setLineWrap(true);
			headingLabel.setWrapStyleWord(true);
			headingLabel.setFont(this.textFontBoldLarge);
			
			final Dimension size = headingLabel.getPreferredSize();
			size.width = contentWidth;
			headingLabel.setMaximumSize(size);
			headingLabel.setAlignmentY(TOP_ALIGNMENT);
			
			return headingLabel;
		}
		
		// ----------------------------------------------------------------------------------
		private JComponent createHeading2(String text, final int spacingAfter)
		{
			final JTextArea headingLabel = new JTextArea(text)
			{
				private static final long	serialVersionUID	= 1L;

				public Insets getInsets()
				{
					return new Insets(5, 20, spacingAfter, 20);
				}
			};
			
			headingLabel.setEditable(false);
			headingLabel.setFocusable(false);
			headingLabel.setLineWrap(true);
			headingLabel.setWrapStyleWord(true);
			headingLabel.setFont(this.textFontBold);
			final Dimension size = headingLabel.getPreferredSize();
			size.width = contentWidth;
			headingLabel.setMaximumSize(size);
			headingLabel.setAlignmentY(TOP_ALIGNMENT);
			
			return headingLabel;		
		}
		
		// ----------------------------------------------------------------------------------
		private JComponent createParagraph(final String text)
		{
			return createParagraph(text, false, 10);
		}
						
		// ----------------------------------------------------------------------------------
		private JComponent createParagraph(final String text, final boolean bold, final int spacingAfter)
		{
			final JTextArea paragraph = new JTextArea(text, 0, 40)
			{
				private static final long	serialVersionUID	= 1L;

				{
					setEditable(false);
					setFocusable(false);
					setLineWrap(true);
					setWrapStyleWord(true);
					setFont(bold ? textFontBold : textFont);
					setAlignmentY(TOP_ALIGNMENT);
					setAlignmentY(TOP_ALIGNMENT);
				}
				
				public Insets getInsets()
				{
					return new Insets(5, 20, spacingAfter, 20);
				}
			};
			
			//LineBreakMeasurer foo;
			
			paragraph.setText(text);
			paragraph.validate();
//			paragraph.setEditable(false);
//			paragraph.setFocusable(false);
//			paragraph.setLineWrap(true);
//			paragraph.setWrapStyleWord(true);
//			paragraph.setFont(bold ? this.textFontBold : this.textFont);
//			paragraph.setAlignmentY(TOP_ALIGNMENT);
//			paragraph.setAlignmentY(TOP_ALIGNMENT);
			final Dimension size = paragraph.getPreferredSize();
			size.width = contentWidth;
			paragraph.setMaximumSize(size);
			
			return paragraph;
		}
		
		// ----------------------------------------------------------------------------------
		
		// ----------------------------------------------------------------------------------
		private class FooterPanel extends JPanel
		{
		 	private static final long	serialVersionUID	= -4911638306174881473L;
		 	private JLabel				morePagesLabel;

		 	public FooterPanel()
		 	{
		 		try
		 		{
		 			setLayout(new BorderLayout());
		 			setFocusable(false);

		 			Font footerFont = App.settings.getFont(Settings.BasicFont);
		 			this.morePagesLabel = new JLabel();
		 			this.morePagesLabel.setFont(footerFont);
		 			JLabel buildLabel = new JLabel("TakeNote Version " + App.getAppVersion());
		 			buildLabel.setFont(footerFont);
		 			
		 			add(buildLabel, BorderLayout.EAST);
		 			add(this.morePagesLabel, BorderLayout.WEST);
		 		}
		 		catch (Exception ex)
		 		{
		 			App.logError(ex.getMessage());
		 		}
		 	}

		 	public void updateStatus()
		 	{
		 		morePagesLabel.setText("page " + HelpView.this.currentPageNumber + " of " + HelpView.this.pageCount);
		 		validate();
		 		pages.requestFocus();
		 		repaint();
		 	}

		 	public Insets getInsets()
		 	{
		 		return new Insets(10, 10, 5, 10);
		 	}

		 	public void paint(Graphics g)
		 	{
		 		super.paint(g);

		 		// Draw a border around the panel
		 		int width = getWidth();
		 		g.setColor(KindletUIResources.getInstance().getColor(KColorName.BLACK));
		 		g.drawLine(0, 5, width, 5);
		 	}

		}
	}
}

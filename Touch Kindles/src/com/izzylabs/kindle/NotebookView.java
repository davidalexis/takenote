/*
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * Copyright (c) 2012, David Alexis. All rights reserved.
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */

package com.izzylabs.kindle;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.border.AbstractBorder;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import com.amazon.kindle.kindlet.event.GestureDispatcher;
import com.amazon.kindle.kindlet.input.Gestures;
import com.amazon.kindle.kindlet.ui.KMenu;
import com.amazon.kindle.kindlet.ui.KMenuItem;
import com.amazon.kindle.kindlet.ui.KOptionPane;
import com.amazon.kindle.kindlet.ui.KPagedContainer;
import com.amazon.kindle.kindlet.ui.KPages;
import com.amazon.kindle.kindlet.ui.KindletUIResources;
import com.amazon.kindle.kindlet.ui.KindletUIResources.KColorName;
import com.amazon.kindle.kindlet.ui.pages.PageProviders;

//----------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------
final class NotebookView extends JPanel
{
	private static final long	serialVersionUID		= 1L;
	private KPages				pages;
	private NotebookFooterPanel	footerPanel;
	private KMenu				menu;
	private final DateFormat	displayDateFormatter	= new SimpleDateFormat("MMM d, yyyy");
	private boolean				trackPageChanges		= false;
	private int					pageCount				= 0;
	private int					currentPage				= 0;
	private int					currentPageStart		= 0;
	private String				noteId;
	private int					itemCount;
	private Notebook			myNotebook;
	private ArrayList			currentFilteredNoteList;
	private OnOffButton			searchButton;
	private int					pageSize				= 0;
	private SearchDefinition	currentSearchTerm;
	private NoteDeleteHandler	noteDeleteHandler;
	private NoteSaveAsHandler	noteSaveAsHandler;
	private JPopupMenu			sortOptionMenu;

	// ------------------------------------------------------------------------------
	NotebookView(Notebook notebook)
	{
		this.myNotebook = notebook;
		this.setLayout(new BorderLayout(0, 0));
		this.currentSearchTerm = null;
		this.sortOptionMenu = createSortOptionPopup();
		this.add(createButtonBar(), BorderLayout.NORTH);
		
		this.pages = new KPages(PageProviders.createBoxLayoutProvider(BoxLayout.Y_AXIS))
		{
			private static final long	serialVersionUID	= 1L;

			public Insets getInsets()
			{
				return new Insets(10, 10, 10, 10);
			}
		};
		this.pages.setFirstPageAlignment(BorderLayout.NORTH);
		this.pages.setFocusable(false);
		this.pages.setPagingPolicy(KPagedContainer.PAGING_POLICY_LOCAL);
		this.add(this.pages, BorderLayout.CENTER);

		this.footerPanel = new NotebookFooterPanel();
		this.add(footerPanel, BorderLayout.SOUTH);

		this.addComponentListener();
		this.addPropertyListener();

		createMenu();

		final GestureDispatcher gestureDispatcher = new GestureDispatcher();
		this.addMouseListener(gestureDispatcher);
		ActionMap gestures = this.pages.getActionMap();

		AbstractAction nextPageAction = new AbstractAction()
		{
			private static final long	serialVersionUID	= 1L;

			public void actionPerformed(ActionEvent e)
			{
				if (App.controller.modalDialogOpen == false)
					pages.next();
			}
		};

		gestures.put(Gestures.ACTION_FLICK_WEST, nextPageAction);

		AbstractAction previousPageAction = new AbstractAction()
		{
			private static final long	serialVersionUID	= 1L;

			public void actionPerformed(ActionEvent e)
			{
				if (App.controller.modalDialogOpen == false)
				{
					pages.previous();
					currentPageStart = pages.getPageStartLocation();

					if (currentPageStart < 0)
						currentPageStart = 0;
				}
			}
		};

		gestures.put(Gestures.ACTION_FLICK_EAST, previousPageAction);

		this.noteDeleteHandler = new NoteDeleteHandler();
		this.noteSaveAsHandler = new NoteSaveAsHandler();
	}

	// ------------------------------------------------------------------------------
	private JPopupMenu createSortOptionPopup()
	{
		JPopupMenu popup = new JPopupMenu()
		{
			private static final long	serialVersionUID	= 1L;

			public Insets getInsets()
			{
				return new Insets(40, 10, 10, 10);
			}
		};
		
		Color blackColor = KindletUIResources.getInstance().getColor(KColorName.BLACK);
		
		TitledBorder labelBorder = BorderFactory.createTitledBorder(
	            null, "Sort Notes by ...",
	            TitledBorder.CENTER, TitledBorder.ABOVE_TOP, 
	            App.settings.getFont(Settings.NotebookItemTitleFont), 
	            blackColor);
	   
	    
	    Border outerBorder = new AbstractBorder()
		{
			private static final long	serialVersionUID	= 1L;
			
			public void paintBorder(Component c, Graphics g, int x, int y, int w, int h)
			{
				g.setColor(KindletUIResources.getInstance().getColor(KColorName.DARK_GRAY));
				Graphics2D g2d = (Graphics2D) g;
				g2d.setStroke(new BasicStroke(4, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
				g2d.drawRoundRect(2, 2, w - 4, h - 4, 30, 30);
			}
		};
		
		popup.setBorder(BorderFactory.createCompoundBorder(outerBorder, labelBorder));
		popup.add(new JSeparator());
		
		// Add menu items
		
		final JMenuItem sortByDateAscMenu =   createPopupMenuItem("Modified Date Ascending");
		final JMenuItem sortByDateDescMenu =  createPopupMenuItem("Modified Date Descending");
		final JMenuItem sortByTitleAscMenu =  createPopupMenuItem("Title Ascending");
		final JMenuItem sortByTitleDescMenu = createPopupMenuItem("Title Descending");
		
		final ImageIcon sortByDateAscIcon = new ImageIcon(getClass().getResource("images/SortDateAsc.png"));
		final ImageIcon sortByDateDescIcon = new ImageIcon(getClass().getResource("images/SortDateDesc.png"));
		final ImageIcon sortByTitleAscIcon = new ImageIcon(getClass().getResource("images/SortAlphaAsc.png"));
		final ImageIcon sortByTitleDescIcon = new ImageIcon(getClass().getResource("images/SortAlphaDesc.png"));
		
		sortByDateAscMenu.setIcon(sortByDateAscIcon);
		sortByDateDescMenu.setIcon(sortByDateDescIcon);
		sortByTitleAscMenu.setIcon(sortByTitleAscIcon);
		sortByTitleDescMenu.setIcon(sortByTitleDescIcon);
		
		sortByDateAscMenu.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				App.settings.setSortField("modified");
				App.settings.setSortDirection("asc");

				NotebookView.this.displayNotes(true);
			}
		});
		
		sortByDateDescMenu.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				App.settings.setSortField("modified");
				App.settings.setSortDirection("desc");

				NotebookView.this.displayNotes(true);

			}
		});
		
		sortByTitleAscMenu.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				App.settings.setSortField("name");
				App.settings.setSortDirection("asc");

				NotebookView.this.displayNotes(true);
			}
		});
		
		sortByTitleDescMenu.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				App.settings.setSortField("name");
				App.settings.setSortDirection("desc");

				NotebookView.this.displayNotes(true);
			}
		});
		
		popup.add(sortByDateAscMenu);
		popup.add(sortByDateDescMenu);
		popup.add(sortByTitleAscMenu);
		popup.add(sortByTitleDescMenu);
		
		return popup;
	}
	
	// ------------------------------------------------------------------------------
	private JMenuItem createPopupMenuItem(String caption)
	{
		final JMenuItem menuItem = new JMenuItem(caption)
		{
			private static final long	serialVersionUID	= 1L;

			public Insets getInsets()
			{
				return new Insets(10, 10, 10, 10);
			}
		};
		
		return menuItem;
	}
	
	// ------------------------------------------------------------------------------
	private Component createButtonBar()
	{
		final ButtonBar bar = new ButtonBar(this.myNotebook.name);

		// Add Search button
		this.searchButton = bar.addOnOffButton("images/find.png", true, "edit", new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				OnOffButton button = ((OnOffButton) e.getSource());

				if (button.isOn)
				{
					App.controller.modalDialogOpen = true;
					NotebookView.this.searchButton.setEnabled(false);
					
					SearchDialog.show (
							App.context.getRootContainer(), 
							new ActionListener()
							{
								public void actionPerformed(ActionEvent e)
								{
									App.controller.modalDialogOpen = false;
									NotebookView.this.currentSearchTerm = new SearchDefinition(e.getActionCommand(), e.getID());
									NotebookView.this.setSearchControlsMode(true);
									NotebookView.this.searchButton.setEnabled(true);
									NotebookView.this.doSearch(NotebookView.this.currentSearchTerm);
								}
							}, 
							new ActionListener()
							{
								public void actionPerformed(ActionEvent e)
								{
									App.controller.modalDialogOpen = false;
									NotebookView.this.searchButton.setEnabled(true);
									NotebookView.this.setSearchControlsMode(true);
								}
							});
				}
				else
				{
					NotebookView.this.currentFilteredNoteList = null;
					NotebookView.this.currentSearchTerm = null;
					NotebookView.this.searchButton.setEnabled(true);
					NotebookView.this.setSearchControlsMode(true);
					NotebookView.this.displayNotes(false);
				}
			}
		});

		bar.addButton("New Note", "images/newnote.png", App.EDITORVIEW, null, App.controller.getEventHandler());

		return bar;
	}

	// ------------------------------------------------------------------------------
	private void addPropertyListener()
	{
		pages.addPropertyChangeListener(new PropertyChangeListener()
		{
			public void propertyChange(PropertyChangeEvent evt)
			{
				if (evt.getPropertyName().compareTo("pageEndLocation") == 0 && trackPageChanges == true)
				{
					try
					{
						calculatePages();
						footerPanel.updateStatus(currentPage, pageCount, itemCount);

						int firstLocation = pages.getPageStartLocation();
						currentPageStart = firstLocation;

						if (!(currentPageStart != 0 && currentPageStart < pages.getPageModel().getLastLocation()))
						{
							currentPageStart = pages.getPageStartLocation();
						}

						if (NotebookView.this.itemCount <= NotebookView.this.currentPageStart)
							NotebookView.this.currentPageStart = NotebookView.this.itemCount - 1;
					}
					catch (Exception ex)
					{
						App.logError("Property page change error: " + ex.getMessage());
					}
				}
			}
		});
	}

	// ------------------------------------------------------------------------------
	private void addComponentListener()
	{
		addComponentListener(new ComponentListener()
		{
			public void componentShown(ComponentEvent arg0)
			{
				displayNotes(true);
				setMenu(true);
				footerPanel.updateStatus(currentPage, pageCount, itemCount);

				validate();
				repaint();
			}

			public void componentResized(ComponentEvent arg0)
			{
				validate();
				repaint();
				calculatePages();
				footerPanel.updateStatus(currentPage, pageCount, itemCount);
			}

			public void componentMoved(ComponentEvent arg0)
			{}

			public void componentHidden(ComponentEvent arg0)
			{
				pages.setPagingPolicy(KPagedContainer.PAGING_POLICY_DISABLED);
				setMenu(false);
			}
		});
	}

	// ------------------------------------------------------------------------------
	private void displayNotes(boolean restorePosition)
	{
		try
		{
			pages.setPagingPolicy(KPagedContainer.PAGING_POLICY_LOCAL);

			if (this.currentFilteredNoteList != null)
			{
				this.currentFilteredNoteList = this.myNotebook.getFilteredNotes(this.currentSearchTerm);
				renderNotes(restorePosition, this.currentFilteredNoteList);
			}
			else
			{
				ArrayList notes = this.myNotebook.getNotes();
				renderNotes(restorePosition, notes);
			}
		}
		catch (Error e)
		{
			App.logError(e.getMessage());
		}
		finally
		{
			trackPageChanges = true;
		}
	}

	// ------------------------------------------------------------------------------
	private void renderNotes(boolean restorePosition, ArrayList notes)
	{
		int savedCurrentPageStart = this.currentPageStart;
		this.pages.removeAllItems();
		this.pages.invalidate();

		trackPageChanges = false;

		for (int index = 0; index < notes.size(); index++)
		{
			NoteIndexEntry thisNote = (NoteIndexEntry) notes.get(index);
			NoteListItem note = new NoteListItem(thisNote.id, thisNote.name, "created on " + displayDateFormatter.format(thisNote.created), "changed on " + displayDateFormatter.format(thisNote.modified));
			note.addDeleteHandler(this.noteDeleteHandler);
			note.addSaveAsHandler(this.noteSaveAsHandler);

			if (restorePosition == true && this.noteId != null && note.noteId.equals(this.noteId))
			{
				this.pages.setLocation(savedCurrentPageStart);
			}

			this.pages.addItem(note);
		}

		trackPageChanges = true;
		itemCount = this.pages.getPageModel().getLastLocation();

		if (itemCount == Integer.MAX_VALUE)
			itemCount = 0;
		else
			itemCount += 1;

		// Display the footer with page data
		this.validate();
		calculatePages();
		footerPanel.updateStatus(currentPage, pageCount, itemCount);

		if (restorePosition == false)
		{
			// this.currentItem = null;
			this.currentPageStart = 0;
		}
		else
		{
			this.currentPageStart = savedCurrentPageStart;
		}

		if (this.currentPageStart != 0 && this.currentPageStart < this.pages.getPageModel().getLastLocation())
		{
			this.pages.setLocation(this.currentPageStart);
		}
		else
			this.pages.setLocation(this.pages.getPageStartLocation());

		this.pages.repaint();
	}

	// ----------------------------------------------------------------------------------
	private void calculatePages()
	{
		try
		{
			if (this.pages.getHeight() <= 0)
				return;

			// Gather known information: item count, location range of
			// current page
			int lastLocation = this.pages.getPageModel().getLastLocation();
			itemCount = lastLocation + 1;
			int pageEndLocation = this.pages.getPageEndLocation();
			int pageStartLocation = this.pages.getPageStartLocation();

			if (pageEndLocation == Integer.MAX_VALUE)
				pageEndLocation = lastLocation;
			else
				pageEndLocation -= 1;

			if (pageStartLocation == Integer.MIN_VALUE)
				pageStartLocation = 0;

			// Calculate page size from given information
			int pageSize = pageEndLocation - pageStartLocation + 1;

			// Compare with saved page location and use whichever is larger
			if (pageSize > this.pageSize)
			{
				this.pageSize = pageSize;
			}
			else
			{
				pageSize = this.pageSize;
			}

			// Based on this data, figure out how many pages there are and
			// what page we're on
			if (this.pageSize == 0 || itemCount == 0)
				pageCount = 1;
			else
				pageCount = ((int) (itemCount / this.pageSize)) + ((itemCount % pageSize) > 0 ? 1 : 0);

			if (lastLocation <= 0)
				currentPage = 1;
			else
				currentPage = (int) ((pageStartLocation + 1) / pageSize) + 1;
		}
		catch (Exception ex)
		{
			App.logError("Error! " + ex.getMessage() + ex.getStackTrace());
		}
	}

	// ----------------------------------------------------------------------------------
	private void createMenu()
	{
		try
		{
			this.menu = new KMenu();

			menu.add(createMenuItem("Export Notebook to HTML", "export", new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					String message = null;
					String exportFileName = NotebookView.this.myNotebook.exportNotes();

					if (exportFileName != null)
					{
						message = "The notes have been exported to " + exportFileName + ". Please refer to the TakeNote Help content for instructions on how to retrieve this file.";
					}
					else
					{
						message = "Failed to export your notebook.";
					}

					KOptionPane.showMessageDialog(NotebookView.this, message, "Notebook Export");
				}
			}));

			menu.add(createMenuItem("Sort Notes By ...", "", new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					//NotebookView.this.sortSelectionPopup.setVisible(true);
//					Container root = App.context.getRootContainer();
					int xPosition = 100; //(int)((root.getWidth() - sortOptionMenu.getWidth()) / 2);
					int yPosition = 100; //(int)((root.getHeight() - sortOptionMenu.getHeight()) / 2);
					sortOptionMenu.show(NotebookView.this, xPosition, yPosition);
					
					//App.controller.modalDialogOpen = true;
				}
			}));

			menu.add(createMenuItem("Switch to Another Notebook", "", new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					App.controller.getEventHandler().actionPerformed(new ActionEvent(NotebookView.this, App.NOTEBOOKLISTVIEW, null));
				}
			}));

			menu.add(createMenuItem("TakeNote Help", "", new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					App.controller.getEventHandler().actionPerformed(new ActionEvent(NotebookView.this, App.HELPVIEW, null));
				}
			}));
		}
		catch (Exception ex)
		{
			App.logError(ex.getMessage() + ex.getStackTrace());
		}
	}

	// ----------------------------------------------------------------------------------
	private KMenuItem createMenuItem(String text, String command,
			ActionListener handler)
	{
		final KMenuItem menuItem = new KMenuItem(text);

		// Set the action command for the menu item
		menuItem.setActionCommand(command);
		menuItem.addActionListener(handler);

		return menuItem;
	}

	// ----------------------------------------------------------------------------------
	private void setMenu(boolean enable)
	{
		try
		{
			if (Thread.interrupted() == false && App.context != null && this.menu != null)
			{
				if (enable == true)
					App.context.setMenu(this.menu);
				else
					App.context.setMenu(null);
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

	// ----------------------------------------------------------------------------------
	private void captureCurrentStartPage()
	{
		this.currentPageStart = this.pages.getPageStartLocation();
	}

	// ----------------------------------------------------------------------------------
	private void doSearch(SearchDefinition searchTerm)
	{

		if (searchTerm != null && searchTerm.term.length() != 0)
		{
			currentFilteredNoteList = this.myNotebook.getFilteredNotes(searchTerm);
			renderNotes(false, currentFilteredNoteList);
			setSearchControlsMode(false);
		}
		else
		{
			setSearchControlsMode(true);
		}
	}

	// ----------------------------------------------------------------------------------
	public Insets getInsets()
	{
		return new Insets(0, 0, 0, 0);
	}

	// ----------------------------------------------------------------------------------
	private void setSearchControlsMode(boolean isEnabled)
	{
		// searchMenuItem.setLabel(isEnabled ? "Search/Filter Notes" :
		// "Cancel Current Search Filter");
		searchButton.setState(isEnabled);
	}

	// ----------------------------------------------------------------------------------
	private class NoteDeleteHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			try
			{
				String noteId = e.getActionCommand();
				NotebookView.this.captureCurrentStartPage();

				final int choice = KOptionPane.showConfirmDialog(NotebookView.this, "This note will be permanently deleted.  Would you like to continue?", "Delete This Note", KOptionPane.NO_YES_OPTIONS);

				if (choice == KOptionPane.YES_OPTION)
				{
					NotebookView.this.myNotebook.deleteNote(noteId);
					NotebookView.this.displayNotes(true);
				}
			}
			finally
			{
				App.controller.modalDialogOpen = false;
			}
		}
	}

	// ----------------------------------------------------------------------------------
	private class NoteSaveAsHandler implements ActionListener
	{
		private Note	sourceNote;

		public void actionPerformed(ActionEvent e)
		{
			try
			{
				sourceNote = NotebookView.this.myNotebook.getNote(e.getActionCommand());

				String noteTitle = KOptionPane.showInputDialog(NotebookView.this, "What title would you like for the new Note?", "Save As", KOptionPane.PLAIN_MESSAGE, KOptionPane.CANCEL_SAVE_OPTIONS, sourceNote.getName());

				if (noteTitle != null && noteTitle.trim().length() != 0)
				{
					noteTitle = noteTitle.trim();

					if (noteTitle.length() != 0 && sourceNote != null && noteTitle.toLowerCase().compareTo(sourceNote.getName().toLowerCase()) != 0)
					{
						Note newNote = new Note(NotebookView.this.myNotebook);
						newNote.setText(sourceNote.getText());
						newNote.setName(noteTitle);
						newNote.save();

						NotebookView.this.displayNotes(false);
					}

				}
			}
			finally
			{
				App.controller.modalDialogOpen = false;
			}
		}
	}
}
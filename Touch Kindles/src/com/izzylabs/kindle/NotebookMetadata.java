/*
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * Copyright (c) 2012, David Alexis. All rights reserved.
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */

package com.izzylabs.kindle;

final class NotebookMetadata
{
	String name;
	String id;
	int noteCount;

	NotebookMetadata(String name, String id, int noteCount)
	{
		this.name = name;
		this.id = id;
		this.noteCount = noteCount;
	}
}

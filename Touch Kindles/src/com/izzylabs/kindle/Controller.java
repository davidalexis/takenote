/*
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * Copyright (c) 2012, David Alexis. All rights reserved.
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */

package com.izzylabs.kindle;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Container;
import java.awt.event.*;
import java.io.Serializable;
import java.util.Stack;
import javax.swing.*;

final class Controller implements Serializable
{
	private static final long	serialVersionUID	= -5587509040724238782L;
	private JPanel				uiContainer;
	private CardLayout			cardPanelLayout;
	private JPanel				cardPanel;
	private Stack				viewStack;
	private Notebook			currentNotebook;
	boolean						modalDialogOpen;
	private Timer 				memoryCheckTimer;
	private static final String	defaultViewName = "notebook";
	
	// ----------------------------------------------------------------------------------
	public Controller()
	{
		// Startup code goes here
		// Set up the UI
		this.uiContainer = new JPanel();
		final Container root = App.context.getRootContainer();
		root.setLayout(new BorderLayout(0, 0));
		root.add(this.uiContainer, BorderLayout.CENTER);
		root.setFocusable(false);
		
		this.uiContainer.setLayout(new BorderLayout(0, 0));
		this.uiContainer.setFocusable(false);
		this.viewStack = new Stack();
		
		
		// Create the CardLayout panel containing the UI screen cards
		this.cardPanelLayout = new CardLayout();
		this.cardPanel = new JPanel(this.cardPanelLayout);
		this.cardPanel.setFocusable(false);
		this.uiContainer.add(this.cardPanel);

		App.controller = this;
		String lastNotebook = App.settings.getCurrentNotebookId();
		this.setCurrentNotebook(new Notebook(lastNotebook));
		this.cardPanel.add(this.currentNotebook.getView(), "notebook");

		if (App.settings.getHasSavedState() == true)
		{
			this.RequestViewById(App.EDITORVIEW, "autosave");
			App.settings.setHasSavedState(false);
		}
		else
		{
			this.RequestViewById(App.NOTEBOOKVIEW, null);
		}
		
		startMemoryChecker();
	}

	// ----------------------------------------------------------------------------------
	private void RequestViewById(int viewId, String viewData)
	{
		String viewName = defaultViewName;
		App.checkDeviceSettings();

		if (viewId == App.PREVIOUSVIEW)
		{
			if (this.viewStack.empty())
			{
				// No views on the stack.  Go to the Notebook view
				viewId = App.NOTEBOOKVIEW;
				viewName = "notebook";
			}
			else
			{
				// Pop the stack and remove that item from the cardPanel
				IViewable currentInterface = (IViewable)this.viewStack.pop();
				this.cardPanel.remove(currentInterface.getView());
				
				// Get the view that needs to be displayed
				if (this.viewStack.empty())
				{
					viewId = App.NOTEBOOKVIEW;
					viewName = "notebook";
				}
				else
				{
					IViewable interfaceToShow = (IViewable)this.viewStack.peek(); 
					viewId = interfaceToShow.getViewId();
					viewName = interfaceToShow.getViewName();
				}				
				
				if (viewName == "notebook" && this.currentNotebook == null)
				{
					this.setCurrentNotebook(new Notebook(Notebook.DefaultNotebokId));
					this.viewStack.pop();
					this.cardPanel.remove(currentInterface.getView());
					buildViewableObject(this.currentNotebook);
				}
			}
		}
		else
		{
			switch (viewId)
			{
				case App.NOTEBOOKVIEW:
					if (this.currentNotebook == null)
					{
						String currentNotebookId = viewData;
						
						if (currentNotebookId == null || currentNotebookId == "")
						{
							currentNotebookId = "notebook";
						}
						
						this.setCurrentNotebook(new Notebook(currentNotebookId));
					}
					
					viewName = this.currentNotebook.getViewName();
					buildViewableObject(this.currentNotebook);
					break;
					
				case App.NOTEBOOKLISTVIEW:
					IViewable notebooks = new NotebookList();
					viewName = notebooks.getViewName();
					buildViewableObject(notebooks);
					break;
				
				case App.EDITORVIEW:
					// The id of the note we want to edit is in the viewData variable.
					IViewable editor = new NoteEditor(viewData);
					viewName = editor.getViewName();
					buildViewableObject(editor);
					break;
					
				case App.HELPVIEW:
					IViewable help = new Help();
					viewName = help.getViewName();
					buildViewableObject(help);
					break;
			}
		}

		this.cardPanelLayout.show(this.cardPanel, viewName);
	}
	
	// ----------------------------------------------------------------------------------
	void shutdown()
	{
		stopMemoryChecker();
		memoryCheckTimer = null;
		
		// Save editor state
		IViewable currentInterface = (IViewable)this.viewStack.peek(); 
		
		if (currentInterface.getViewId() == App.EDITORVIEW)
		{
			((NoteEditor)currentInterface).saveState();
			App.settings.save();
		}
		
		// Clean up view/controller objects
//		this.dashboard.setView(null);
//		this.dashboard = null;
		this.currentNotebook.setView(null);
		this.currentNotebook = null;
		
		//TODO:  Clean up any other views/controllers
		
		// Clean up the rest
		this.cardPanel = null;
		this.cardPanelLayout = null;
		this.uiContainer = null;
	}
	
	// ----------------------------------------------------------------------------------
	public ActionListener getEventHandler()
	{
		return new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				int actionId = e.getID();
				String actionData = e.getActionCommand();
				RequestViewById(actionId, actionData);
			}
		};
	}

	// ----------------------------------------------------------------------------------
	private void buildViewableObject(IViewable viewableObject)
	{
		this.viewStack.push(viewableObject);
		this.cardPanel.add(viewableObject.getView(), viewableObject.getViewName());
	}

	// ----------------------------------------------------------------------------------
	public Notebook getCurrentNotebook() 
	{
		return currentNotebook;
	}

	// ----------------------------------------------------------------------------------
	public void setCurrentNotebook(Notebook currentNotebook) 
	{
		this.currentNotebook = currentNotebook;
		
		if (currentNotebook != null)
		{
			App.settings.setCurrentNotebookId(currentNotebook.id);
		}
	}

	// ----------------------------------------------------------------------------------
	void startMemoryChecker()
	{
		if (memoryCheckTimer == null)
		{
			memoryCheckTimer = new Timer(3000, new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
			    	if (!Thread.interrupted())
					{
			    		long memory = Runtime.getRuntime().freeMemory();
			    		
			    		if (memory <= 1024 * 1024 * 4)
						{
							System.gc();
							System.gc();
						}
				    }
				}
			});

			memoryCheckTimer.setInitialDelay(4000);
			memoryCheckTimer.start();
		}
		else
			memoryCheckTimer.restart();
	}
	
	// ----------------------------------------------------------------------------------
	void stopMemoryChecker()
	{
		memoryCheckTimer.stop();
	}
}

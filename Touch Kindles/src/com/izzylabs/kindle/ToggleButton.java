/*
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * Copyright (c) 2012, David Alexis. All rights reserved.
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */

package com.izzylabs.kindle;

import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;

final class ToggleButton extends JButton
{

	private static final long	serialVersionUID	= 1L;
	
	private ImageIcon onIcon;
	private ImageIcon offIcon;
	private boolean   isOn;
	
	ToggleButton(final String onIconPath, final String offIconPath, final boolean initialStateIsOn, final String command, final ActionListener actionHandler)
	{
		this.onIcon  = new ImageIcon(getClass().getResource(onIconPath));
		this.offIcon = new ImageIcon(getClass().getResource(offIconPath));
		this.isOn = initialStateIsOn;
		this.setIcon(this.onIcon);
		
		switchIcon();
		
		this.setActionCommand(command);
		this.addActionListener(actionHandler);
		this.setState(initialStateIsOn);
	}
	
// TODO Remove unused code found by UCDetector
// 	public void flipState()
// 	{
// 		this.isOn = !this.isOn;
// 		switchIcon();
// 	}
	
	public void setState(boolean isOn)
	{
		this.isOn = isOn;
		switchIcon();
	}
	
	private void switchIcon()
	{
		if (this.isOn)
			this.setIcon(this.onIcon);
		else
			this.setIcon(this.offIcon);
	}
}
